﻿using System;
using UnityEngine;
using System.Collections;
using Object = UnityEngine.Object;

namespace JW.CharacterCustomization
{
    [System.Serializable]
    public class CharacterPart
    {
        public bool Visible = true;
        public bool UnlockedByDefault = false;
        public string Label = "noName";
        public GameObject gameObject;

        /// <summary> 
        /// If specified, this texture replaces the main object texture on instantiation. 
        /// Note: This is different than a custom texture.
        /// </summary>
        public Texture2D DefaultTextureOverride;

        /// <summary> The UV map for this character part. </summary>
        //public Texture2D UVImage;
        //public Funds Price;

        /// <summary>
        /// Instantiates the character part, and assigns a replacement texture if nessesary.
        /// </summary>
        /// <returns>The new Gameobject in the scene.</returns>
        public GameObject InstantiatePart()
        {
            GameObject newPart = Object.Instantiate(gameObject);

            //Now assign the replacement texture, if it exists.
            if (DefaultTextureOverride != null)
            {
                Texture2D replacementTexture = DefaultTextureOverride;

                //Check if the renderer exists...
                Renderer renderer = newPart.GetComponent<Renderer>();
                if (renderer == null)
                    renderer = newPart.GetComponentInChildren<Renderer>();

                if (renderer == null || renderer.material == null)
                {
                    Debug.LogError(typeof(CharacterPart).Name + ": Cannot find renderer/material on " + Label + " to replace texture!");
                    return newPart;
                }

                //Replace the texture
                renderer.material.mainTexture = replacementTexture;
            }

            return newPart;
        }

        /// <summary>
        /// Returns the texture that should be intially visible on the instantiated model.
        /// </summary>
        public Texture2D GetDefaultTexture()
        {
            if (DefaultTextureOverride != null)
                return DefaultTextureOverride;

            //Otherwise, get the gameobject texture
            if (gameObject == null)
                return null;
            Renderer renderer = gameObject.GetComponent<Renderer>();
            if (renderer == null || renderer.sharedMaterial == null)
                return null;
            return renderer.sharedMaterial.mainTexture as Texture2D;
        }

        void replaceMainTexture(GameObject partGameObject)
        {

        }
    }
}