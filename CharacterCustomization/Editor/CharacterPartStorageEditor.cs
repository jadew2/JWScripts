﻿using UnityEditor;
using UnityEditorInternal; //Reorderable list
using UnityEngine;

using System;
using System.Collections.Generic;

using JW.Utils;

namespace JW.CharacterCustomization
{
    [CustomEditor(typeof(CharacterPartStorage))]
    public class CharacterPartStorageEditor : Editor
    {
        private SerializedObject mainObject;
        private CharacterPartStorage storageClass;

        //Editor GUI Vars
        static List<bool> typeFoldouts;
        static bool displayDetailedProperties;
        List<ReorderableList> reorderableLists = new List<ReorderableList>();

        ListHelper listHelper = new ListHelper();

        public void OnEnable()
        {
            mainObject = new SerializedObject(target);
            storageClass = (CharacterPartStorage)target;

            setUpLists();
            setUpReorderableLists();
        }

        //Resize AllCharacterParts and the TypeFoldouts lists to match the number of types.
        void setUpLists()
        {
            //---Create the parts list
            int partTypesCount = Enum.GetValues(typeof(CharacterPartType)).Length;
            listHelper.ResizeTo(partTypesCount, //Length
                storageClass.AllCharacterParts, //List
                () => new CharacterPartStorage.CharacterPartTypeList()); //New element method

            //---Instantiate the EditorGUIData list for it
            typeFoldouts = new List<bool>();
            for (int i = 0; i < partTypesCount; i++)
                typeFoldouts.Add(false);
        }

        void setUpReorderableLists()
        {
            var allCharacterParts = storageClass.AllCharacterParts;

            for (int i = 0; i < allCharacterParts.Count; i++)
            {
                var partList = allCharacterParts[i].Parts;

                ReorderableList reorderableList = new ReorderableList(partList, typeof(CharacterPartStorage.CharacterPartTypeList), true, false, true, true);
                reorderableLists.Add(reorderableList);

                //---Header
                reorderableList.drawHeaderCallback = rect =>
                {
                    EditorGUI.LabelField(rect, "Parts List: " + partList.Count);
                };

                //---Elements
                reorderableList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
                {
                    var element = partList[index];
                    if (element == null)
                        return;

                    {
                        float xOffset = rect.x;
                        float yOffset = rect.y;
                        float width = 25f;
                        EditorGUI.LabelField(new Rect(xOffset, yOffset, width, EditorGUIUtility.singleLineHeight), index + ".");
                        xOffset += width;

                        width = 150f;
                        element.Label = EditorGUI.TextField(new Rect(xOffset, yOffset, width, EditorGUIUtility.singleLineHeight), element.Label);
                        xOffset += width;

                        width = 200f;
                        element.gameObject = (GameObject)EditorGUI.ObjectField(new Rect(xOffset, yOffset, width, EditorGUIUtility.singleLineHeight), element.gameObject, typeof(GameObject), false);
                        xOffset += width;
                    }

                    //If we are selected, continue
                    if (!isActive)
                        return;

                    //Draw the following in the area following the reorderable list
                    GUILayout.Label("Selected Properties for: " + element.Label);
                    GUILayout.BeginHorizontal();
                    element.Visible = GUILayout.Toggle(element.Visible, " Visible In UI");
                    element.UnlockedByDefault = GUILayout.Toggle(element.UnlockedByDefault, " Unlocked By Default");
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Cost:");
                    GUILayout.Label("TargetTok");
                    // element.Price.TargetTokens = EditorGUILayout.IntField(element.Price.TargetTokens);

                    GUILayout.Label("PremiumTok");
                    // element.Price.PremiumTokens = EditorGUILayout.IntField(element.Price.PremiumTokens);
                    GUILayout.EndHorizontal();

                    element.DefaultTextureOverride = (Texture2D)EditorGUILayout.ObjectField("Replacement Texture", element.DefaultTextureOverride, typeof(Texture2D), false);
                    //element.UVImage = (Texture2D)EditorGUILayout.ObjectField("UV Texture",element.UVImage,typeof(Texture2D),false);
                };

                //OnAdd (Popup menu)
                reorderableList.onAddCallback = (ReorderableList list) =>
                {
                    CharacterPart newPart = new CharacterPart();
                    listHelper.AddBelowAt<CharacterPart>(list.index, (IList<CharacterPart>)list.list, newPart);
                    list.index++;
                };

                //On Selected
                /*reorderableList.onSelectCallback = (ReorderableList list) =>
                {};*/

                //OnRemove
                reorderableList.onRemoveCallback = (ReorderableList list) =>
                {
                    ReorderableList.defaultBehaviours.DoRemoveButton(list);
                };
            }
        }


        public override void OnInspectorGUI()
        {
            mainObject.Update(); //Load Changes from the Gameobject into the GUI			

            InspectorUtils.ShowScriptField(mainObject);

            var allParts = storageClass.AllCharacterParts;

            //---If the database is null, request the ability to create a new one.
            if (allParts == null)
            {
                GUILayout.Label("Database not initialized.");
                return;
            }

            //---Display the database
            string[] enumNames = Enum.GetNames(typeof(CharacterPartType));
            for (int i = 0; i < allParts.Count; i++)
            {
                typeFoldouts[i] = EditorGUILayout.Foldout(typeFoldouts[i], (i < enumNames.Length) ? enumNames[i] : "???");

                if (!typeFoldouts[i])
                    continue;

                reorderableLists[i].DoLayoutList();
            }

            mainObject.ApplyModifiedProperties(); //Apply any changes
        }

    }
}
