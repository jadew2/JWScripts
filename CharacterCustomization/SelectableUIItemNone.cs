﻿using UnityEngine;
using System.Collections;

namespace JW.UI
{
/// <summary>
/// A UI Item that represents a "None" row. 
/// It's automatically selected when no other items are selected.
/// </summary>
public class SelectableUIItemNone : SelectableUIItem 
{
}
}