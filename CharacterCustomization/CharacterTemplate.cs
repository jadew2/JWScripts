﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System;
using System.Linq;

namespace JW.CharacterCustomization
{
/// <summary> A key for a character part. Use it to access character parts, or design a character. </summary>
[System.Serializable]
public struct CharacterPartKey
{
	public CharacterPartType PartType;
	public int Index;

	public CharacterPartKey(CharacterPartType partType, int index)
	{
		this.PartType = partType;
		this.Index = index;
	}

	public override int GetHashCode()
	{
		return base.GetHashCode();
	}

	public override bool Equals(object obj)
	{
		if (base.Equals(obj))
			return true;

		if (!(obj is CharacterPartKey))
			return false;

		CharacterPartKey other = (CharacterPartKey)obj;
		return other.PartType == PartType &&
			other.Index == Index;
	}

	public static bool operator ==(CharacterPartKey a, CharacterPartKey b)
	{
		// If one is null, but not both, return false.
		if (((object)a == null) && ((object)b == null))
			return true;
		if (((object)a == null) || ((object)b == null))
			return false;
		return a.Equals(b);
	}

	public static bool operator !=(CharacterPartKey a, CharacterPartKey b)
	{
		return !(a == b);
	}

	public override string ToString()
	{
		return typeof(CharacterPartKey).Name + ": ( " + PartType.ToString() + ", " + Index + " )";
	}
}

[System.Serializable]
public class CharacterPartTextureData
{
	public CharacterPartKey PartKey;
	public byte[] TextureData;

	[NonSerialized]
	Texture2D texture = null;
	public Texture2D Texture { 
		get
		{ 
			//Try loading the texture
			if (texture != null)
				return texture;

			Texture2D newTexture = new Texture2D(512,512);
			if (!newTexture.LoadImage(TextureData))
				return null;
			texture = newTexture;
			return texture;
		}
	}
}


/// <summary> A blueprint for a character, composed of a list of part keys. </summary>
[System.Serializable]
public class CharacterTemplate
{
	public List<CharacterPartKey> PartKeys = new List<CharacterPartKey>();
	public List<CharacterPartTextureData> TextureData = new List<CharacterPartTextureData>();

	public CharacterTemplate(){}
	public CharacterTemplate(List<CharacterPartKey> keys) : this(keys.ToArray()){}
	public CharacterTemplate(params CharacterPartKey[] keys)
	{
		for (int i = 0; i < keys.Length; i++) 
			PartKeys.Add(keys[i]);
	}
	public CharacterTemplate(CharacterTemplate cloneThis)
	{
		cloneThis.CopyTo(this);
	}

	public CharacterPartTextureData GetTextureDataForPartKey(CharacterPartKey partKey)
	{
		return TextureData.FirstOrDefault(texData => texData.PartKey == partKey);
	}

	public override string ToString()
	{
		string output = "CharacterTemplate: \n";

		foreach (var partKey in PartKeys)
		{
			output += partKey.ToString() + "\n";
		}

		return output;
	}

	public void CopyTo(CharacterTemplate other)
	{
		other.PartKeys = new List<CharacterPartKey>(this.PartKeys);
		other.TextureData = new List<CharacterPartTextureData>(this.TextureData);
	}
}
}