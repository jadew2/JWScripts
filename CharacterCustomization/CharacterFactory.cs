﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace JW.CharacterCustomization
{
/// <summary>
/// A monobehaviour singleton which has methods for generating a character provided a template.
/// See GenerateCharacter()
/// </summary>
/// <remarks>
/// By: Jade White 2014
/// For: Project Target Party
/// 
/// To be attached to a  GO.
/// 
/// By Generate, I mean
///  	1. Instantiate a character skeleton
///  	2. Add on passed parts
///  	3. Optimize if possible. If not, leave those parts alone.
///  	4. Return the new Gameobject.
/// </remarks>
[RequireComponent (typeof(CharacterPartStorage))]
public class CharacterFactory : MonoBehaviour 
{
	public class GeneratedCharacterInfo
	{
		public GeneratedCharacterInfo(GameObject go, SkinnedMeshRenderer combinedSkinnedMeshRenderer)
		{
			this.GameObject = go;
			this.CombinedSkinnedMeshRenderer = combinedSkinnedMeshRenderer;
		}
		
		readonly public GameObject GameObject;
		readonly public SkinnedMeshRenderer CombinedSkinnedMeshRenderer;
	}
	
	[SerializeField] GameObject baseMeshPrefab = null;

	CharacterPartStorage characterPartStorage;
	
	void Awake()
	{
		characterPartStorage = GetComponent<CharacterPartStorage>();
	}

	///<summary> Generates a default character </summary>
	public virtual GameObject GenerateCharacter()
	{
		return Instantiate(baseMeshPrefab);
	}

	///<summary> Generates a new character using the character template </summary>
	public virtual GeneratedCharacterInfo GenerateCharacter(CharacterTemplate template)
	{
		GameObject newCharacter = Instantiate(baseMeshPrefab);
		return GenerateCharacter(template, newCharacter);
	}

	///<summary> Generates the character mesh using the character template and gameobject. The gameobject is the one that typically has a animator.</summary>
	public virtual GeneratedCharacterInfo GenerateCharacter(CharacterTemplate template, GameObject characterGO)
	{
		if (characterGO.GetComponent<Animator>() == null && characterGO.GetComponentInAncestors<Animator>() == null)
			Debug.LogWarning(typeof(CharacterFactory).Name + ": Passed character gameobject doesn't have a Animator. The Animator is not nessesary, but it ensures this is the right GO. It is recommended to have one.");

		stripMeshesOn(characterGO);
		Transform root = findRootBoneByName(characterGO.transform); //Because we have a bare skeleton, use this opportunity to get at the root fast.
		if (root == null)
			Debug.LogError(typeof(CharacterFactory).Name + ": Unable to find Root bone! :(");
		
		//Now create each part, keeping track of any Skinned Mesh Renderers created
		var smrList = new List<SkinnedMeshRenderer>();
		foreach (var key in template.PartKeys)
		{
			CharacterPart part = characterPartStorage.GetPart(key);

			GameObject createdPart = part.InstantiatePart();
			createdPart.transform.parent = characterGO.transform;

			SkinnedMeshRenderer smr = createdPart.GetComponent<SkinnedMeshRenderer>();
			if (smr != null)
			{
				smr.rootBone = root; //Do this for all SMR's if they're enabled or not.
				
				//Apply a custom texture (e.g. one painted by the player) if it's specified in the template.
				CharacterPartTextureData texData = template.GetTextureDataForPartKey(key);
				if (texData != null && texData.Texture != null && smr.material != null)
					smr.material.mainTexture = texData.Texture;

				smrList.Add(smr);
			}
		}
		//Combine as many skinned meshes as possible. 
		SkinnedMeshRenderer newSmr = combineSkinnedMeshes(smrList,root,characterGO);

		return new GeneratedCharacterInfo(characterGO, newSmr);
	}

	#region Threaded Optimization

	/// <summary>
	/// Class to hold and run character generation processes.
	/// </summary>
	class GenerateCharacterTask
	{
		CharacterFactory owner;
		public CharacterTemplate template;
		public GameObject characterGO;
		public Action<GeneratedCharacterInfo> onDone;
		
		public GenerateCharacterTask() {}

		public GenerateCharacterTask(CharacterFactory owner, CharacterTemplate template, GameObject characterGO, Action<GeneratedCharacterInfo> onDone)
		{
			this.owner = owner;
			this.template = template;
			this.characterGO = characterGO;
			this.onDone = onDone;
		}

		/// <summary>
		/// Executes the generate character.
		/// </summary>
		public void Run()
		{
			if (!ExtensionMethods.IsNullOrDestroyed(owner))
				onDone(owner.GenerateCharacter(template, characterGO));
		}
	}
	
	Queue<GenerateCharacterTask> generateCharacterTasks = new Queue<GenerateCharacterTask>();
	
	///<summary> Optimization: Spaces out character generation so only one generation happens per frame. </summary>
	public virtual void GenerateCharacterThreaded(CharacterTemplate template, GameObject characterGO, Action<GeneratedCharacterInfo> onDone)
	{
		GenerateCharacterTask generateCharacterTask = new GenerateCharacterTask(this,template,characterGO,onDone);
		generateCharacterTasks.Enqueue(generateCharacterTask);
	}

	void Update()
	{
		if (generateCharacterTasks.Count > 0)
		{
			GenerateCharacterTask currTask = generateCharacterTasks.Dequeue();
			currTask.Run();
		}
	}

	#endregion
	
	#region Helper Methods

	///<summary> Destroys anything with a mesh filter/skinned mesh renderer on the passed gameobject's direct children. </summary>
	 void stripMeshesOn(GameObject character)
	{
		Transform[] directChildren = character.transform.GetDirectChildren();

		//Search for MeshFilters/ Skinned Mesh Renderers in them. If so, destroy the gameobject. 
		for (int i = 0; i < directChildren.Length; i++) 
		{
			GameObject childGO = directChildren[i].gameObject;
			if ((childGO.GetComponent<SkinnedMeshRenderer>() != null) || (childGO.GetComponent<MeshRenderer>() != null))
				DestroyImmediate(childGO);
		}

		return;
	}

	/// <summary> Finds the root bone using the name "root".  </summary>
	/// <returns> The root transform. Otherwise null. </returns>
	 Transform findRootBoneByName(Transform trans)
	{
		const string rootBoneName = "root";

		//TODO: Optimize this while leaving the ability to attach anything to a player.
		Transform[] transforms = trans.GetAllChildren();
		for (int i = 0; i < transforms.Length; i++) 
		{
			if (transforms[i].name == rootBoneName)
				return transforms[i];
		}
		return null;
	}

	/// <summary>
	/// This will merge as many skinned mesh renderers (SMR)s as possible.
	/// 
	/// A SMR won't be combined if the bone skeleton of the SMR is different from our character.
	/// If the SMR is combined, it disables the old copy.
	/// </summary>
	SkinnedMeshRenderer combineSkinnedMeshes(List<SkinnedMeshRenderer> skinnedMeshRenderers, Transform root, GameObject characterGO, bool rebind = false)
	{
		//Code modified from Unity's Character Customization Example Scene.
		List<CombineInstance> meshCIs = new List<CombineInstance>();
		List<Material> combinedMaterials = new List<Material>();
		List<Transform> combinedBones = new List<Transform>();

		Transform[] characterBones = root.GetAllChildren();

		foreach (var smr in skinnedMeshRenderers) 
		{
			//yield return null; //Rest a frame for each skinned mesh renderer. The stuff in this body is heavy.
	
			//First try to add the bones in order. We try this first so we can compare the skeltons.
	
			/* Add bones in order.
			 * In order to combine all our SMRs (SkinnedMeshRenderers) into a single one, we need add references to bones in a ordered list.
			 * These must be appended in the order they appear in each SMR we're combining.
			 * We must add the instantiated character's bones to the list because the SMRs reference their original model.
			 *
			 * The method below is heavily optimized now. But this was what it was before:
			 * for (int i = 0; i < smrBones.Length; i++)
			 * {
			 *		smrBone = smrBones[i];
			 *
			 * 		foreach (characterBone in CharacterBones)
			 *		{
			 * 			if (smrBone.name == characterBone.name)
			 *			{
			 *				resultOrderedBones[i] == characterBone;
			 *				break;
			 *			}
			 *		}
			 *	}
			 * 
			 * The problem with this is it takes too long. Especially with 86 character bones.
			 * ~86bones * ~86bones * ~8smrs * 11aiPlayers * 2(.name method calls) = 1,301,696 calls to UnityEngine.Object.get_name().
			 */
			 
			JW.Generic.LinkedList<Transform> characterBonesTmpList = new JW.Generic.LinkedList<Transform>(characterBones); //Optimization
			Transform[] smrBones = smr.bones;
			Transform[] resultOrderedBones = new Transform[smrBones.Length];
			for (int i = 0; i < smrBones.Length; i++)
			{
				string smrBoneName = smrBones[i].name; //Optimization
				
				//foreach (characterBone in CharacterBones)
				JW.Generic.LinkedListNode<Transform> prev = null;
				JW.Generic.LinkedListNode<Transform> curr = characterBonesTmpList.First;
				while (curr != null)
				{
					string characterBoneName = curr.Value.name;
					
					if (smrBoneName == characterBoneName)
					{
						resultOrderedBones[i] = curr.Value;
						
						//Now we remove this bone from the tmpCharacterBones list.
						//Because it's been assigned, we won't need to iterate over this character bone again. 
						//Also this removal takes O(1) time instead of O(n).
						if (prev == null)
						{
							characterBonesTmpList.First = curr.Next; //Replace the head
						}
						else
						{
							prev.Next = curr.Next; //Modify the list to skip this element.
						}

						break;
					}
					
					//Move to the next element.
					prev = curr;
					curr = curr.Next;
				}
			}
			
			//It's okay for the SMR to not use all the bones from the character:
			//E.g. A SMR for a character's head won't use the foot bone, so it could be missing in smr.bones.
			//
			//However, when the SMR can't find a suitable bone from the character, it's catastrophic:
			//E.g. A SMR for the character's head can't find the bone that controls the head.
			//In this case, some resultOrderedBones[i] would = null. So we check for that.
			
			//Check if the skeletons match. If they don't, skip this SMR.
			bool boneSkeletonsMatch = true;
			for (int i = 0; i < smrBones.Length; i++) 
			{
				if (resultOrderedBones[i] == null) //If we didn't find a bone to add, This is a problem.
				{
					Debug.LogWarning(typeof(CharacterFactory).Name + ": In the Skinned Mesh Renderer for " + smr.gameObject.name + " we're trying to combine, bone " + smrBones[i].name + " does not exist in our base mesh. Skipping.");
					boneSkeletonsMatch = false;
					break;
				}
			}
			
			if (!boneSkeletonsMatch)
				continue; //Skip.
	
			//Otherwise, it's fine to combine this
			combinedBones.AddRange(resultOrderedBones); //Add the ordered bones
			combinedMaterials.AddRange(smr.materials); //Add materials
			for (int i = 0; i < smr.sharedMesh.subMeshCount; i++) //Add meshes to be combined
			{
				CombineInstance ci = new CombineInstance();
				ci.mesh = smr.sharedMesh;
				ci.subMeshIndex = i;
				meshCIs.Add(ci);
			}
	
			//smr.enabled = false; //We got all the information we need. Disable the old skinned mesh renderer (it's already disabled)
		}

		//Create a new GO and SkinnedMeshRenderer
		GameObject newSmrGO = new GameObject("combinedSkinnedMesh");
		newSmrGO.transform.parent = characterGO.transform;

		//Set the new SMR.
		Mesh newMesh = new Mesh();
		newMesh.CombineMeshes(meshCIs.ToArray(),false,false);

		SkinnedMeshRenderer r = newSmrGO.AddComponent<SkinnedMeshRenderer>();
		r.sharedMesh = newMesh;
		r.bones = combinedBones.ToArray();
		r.materials = combinedMaterials.ToArray();
		r.rootBone = root;
		
		//Cleanup
		//You only need to rebind the animator if you change the Avatar field. (This also avoids animation and rotation-resetting bugs!)
		if (rebind)
		{	
			
			//If there's a animator component, make sure it's refreshed for our new skinned mesh(s).
			Animator ani = characterGO.GetComponent<Animator>();
			if (ani == null)
				ani = characterGO.GetComponentInAncestors<Animator>();
			if (ani != null)
			{
				//Attempt to preserve animator state (Right now does this super simple, but you can code it to support parameters later)
				var stateInfo = ani.GetCurrentAnimatorStateInfo(0);
				float time = stateInfo.normalizedTime;
				int nameHash = stateInfo.fullPathHash;

				ani.Rebind();
				ani.Play(nameHash, 0, time);
				
				Debug.Log("Rebinding");
			}
		}

		//Sync the position of the skinned mesh renderers to their parents. (Just neater).
		r.transform.position = r.bounds.center;
		r.transform.localRotation = Quaternion.identity;

		foreach (var smr in skinnedMeshRenderers)
		{
			Transform smrTransform = smr.transform;
			smrTransform.position = smr.bounds.center;
			smrTransform.localRotation = Quaternion.identity;

			smr.enabled = false;
		}

		//Done!
		return r;
	}

	#endregion
		

}
}
