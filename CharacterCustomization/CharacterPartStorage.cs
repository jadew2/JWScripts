﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace JW.CharacterCustomization
{
public enum CharacterPartType
{
	Head = 0,
	Body = 1,
	
	Hair = 2,
	Shirt = 3,
	Pants = 4,
	
	Accessories = 5, //Shoes have been moved to accessories
	FlightGear = 6, //TP Specific
	Shield = 7, //TP Specific
}

/// <summary>
/// Holds all character parts in the game. To be attached to a GO and filled using the editor. Consider making this a prefab.
/// </summary>
public class CharacterPartStorage : MonoBehaviour 
{
	//Unity can't serialize 2D lists yet.
	//This serializable class is nessesary work around so unity can serialize the contents of a 2D list.
	[System.Serializable]
	public class CharacterPartTypeList
	{
		public List<CharacterPart> Parts = new List<CharacterPart>();
	}

	/// <summary>
	/// A list of all chooseable parts classified by type.
	/// </summary>
	[SerializeField]
	public List<CharacterPartTypeList> AllCharacterParts = new List<CharacterPartTypeList>();
	
	//Accessor Methods
	public CharacterPart GetPart (CharacterPartKey key)
	{
		return AllCharacterParts[(int)key.PartType].Parts[key.Index];
	}

	public List<CharacterPart> GetPartsOfType (CharacterPartType type)
	{
		return AllCharacterParts[(int)type].Parts;
	}

	public List<CharacterPartKey> GetPartKeysOfType (CharacterPartType type)
	{
		int partCount = GetPartsOfType(type).Count;
		List<CharacterPartKey> keys = new List<CharacterPartKey>(partCount);
		for (int i = 0; i < partCount; i++)
			keys.Add(new CharacterPartKey(type,i));

		return keys;
	}

	public List<CharacterPartKey> GetAllPartKeys()
	{
		List<CharacterPartKey> partkeys = new List<CharacterPartKey>();

		int length = Enum.GetNames(typeof(CharacterPartType)).Length;
		for (int i = 0; i < length; i++)
			partkeys.AddRange(GetPartKeysOfType((CharacterPartType)i));

		return partkeys;
	}


}
}