﻿using UnityEngine;

namespace JW.Modifiers
{
/// <summary>  Base class for Modifiers </summary>
public class ModifierBase : MonoBehaviour
{
	/// <summary> Applies the modifier on the mesh, overwriting it's contents. </summary>
	public virtual void ApplyOn(Mesh mesh /*Mesh to be modified*/, Transform at /*Origin of the operation*/)
	{
		throw new System.NotImplementedException();
	}
}
}


