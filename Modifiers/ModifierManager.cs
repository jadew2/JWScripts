﻿using UnityEngine;
using System.Collections.Generic;
using JW.Utils;

namespace JW.Modifiers
{
/// <summary>
/// A class for applying list of modifiers in order and pushing the result to a meshfilter. 
/// Does not modify the "Original Mesh" set.
/// 
/// Extends ModifierCollection.
/// </summary>
[AddComponentMenu("JWModifiers/Modifier Manager")]
[RequireComponent (typeof(MeshFilter))]
public class ModifierManager : ModifierCollection 
{
	/// <summary> The original mesh. Is not modified. </summary>
	public Mesh OriginalMesh; 
	 Mesh deformedMesh = null; //Pooled Mesh

	MeshFilter filter = null;
	MeshFilter Filter {get {return (filter == null) ? (filter = GetComponent<MeshFilter>()) : filter;}}
	const string DEFORMED_MESH_NAME = "Modifier Deformed Mesh";

	/// <summary>  Method to apply the Manager's modifiers and push the result to the MeshFilter. </summary>
	public void Evaluate()
	{
		if (deformedMesh == null)
			deformedMesh = new Mesh{ name = DEFORMED_MESH_NAME };
		if (OriginalMesh == null)
		{
			Debug.LogError(typeof(ModifierManager).Name + ": Original mesh not set!");
			return;
		}

		//First, get ourselves a fresh mesh we can modify
		MeshUtils.Copy(OriginalMesh,deformedMesh,true);

		//Apply the modifiers (method from the base class)
		ApplyOn(deformedMesh,transform);

		//Send the mesh to the MeshFilter if it's a different object.
		//If a old one exists, destroy it only if the name matches
		Mesh sharedMesh = Filter.sharedMesh;
		if (sharedMesh != deformedMesh)
		{
			if (sharedMesh != null && sharedMesh.name == DEFORMED_MESH_NAME)
				DestroyImmediate(sharedMesh);
			
			Filter.sharedMesh = deformedMesh;
		}
	}

}
}
