﻿using UnityEngine;
using System.Collections.Generic;

namespace JW.Modifiers
{
/// <summary>
/// A Modifier which represents a collection of modifiers. Very powerful if you think about it.
/// </summary>
[AddComponentMenu("JWModifiers/Modifier Collection")]
public class ModifierCollection : ModifierBase
{
	/// <summary> A ordered list of Modifiers to be Applied. </summary>
	[SerializeField] public List<ModifierBase> Modifiers = new List<ModifierBase>();

	/// <summary> Applies modifiers on the mesh in order. </summary>
	public override void ApplyOn(Mesh mesh, Transform at)
	{
		for (int i = 0; i < Modifiers.Count; i++)
		{
			//Check for this modifier inside. If so, this would cause a infinite loop.
			if (Modifiers[i] == this)
			{
				Debug.LogError(typeof(ModifierCollection).Name + ": ApplyOn: Detected same collection within itself. This would cause a infinite loop. Removing."); 
				Modifiers.RemoveAt(i);
				i--;
				continue;
			}

			Modifiers[i].ApplyOn(mesh, at);
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();
		}
	}
}
}
