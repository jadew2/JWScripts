﻿using System;
using UnityEngine;
using System.Collections;

using JW.Spline;
using JW.Utils;

namespace JW.Modifiers
{
/// <summary>
/// Modifier class which duplicates the passed mesh at optional offsets x number of times.
/// Allows you to attach a cap and end for convenience.
/// </summary>
[AddComponentMenu("JWModifiers/Array Modifier")]
public class ArrayModifier : ModifierBase
{
	public enum ArrayFitType{Count,Length,Spline}
	public ArrayFitType FitType = ArrayFitType.Count;

	public SplineManager Spline;

	public float LengthVar; //Used as Count or Length depending on the fit type

	public Vector3 ConstantOffset = new Vector3(0f,0f,0f);
	public Vector3 RelativeOffset = new Vector3(0f,0f,1f);
	public Transform ObjectOffset = null;

	public Mesh Start = null;
	public Mesh End = null;

	MeshUtils.MeshCombiner meshCombiner = new MeshUtils.MeshCombiner();

	public override void ApplyOn(Mesh mesh, Transform at)
	{	
		Mesh newMesh = new Mesh();
		MeshUtils.Copy(mesh,newMesh,true);

		int duplicateAmount = getDuplicateAmount(mesh);
		if (duplicateAmount > 0)
		{
			meshCombiner.AddMeshData(newMesh);
			for (int i = 1; i < duplicateAmount; i++) 
			{
				transformMeshOver(newMesh,true);
				meshCombiner.AddMeshData(newMesh);
			}
		}

		if (Start != null)
		{
			MeshUtils.Copy(Start,newMesh,true);
			transformMeshOver(newMesh,false);
			meshCombiner.AddMeshData(newMesh);
		}

		if (End != null)
		{
			MeshUtils.Copy(End,newMesh,true);
			for (int i = 1; i < (duplicateAmount+1); i++)
				transformMeshOver(newMesh,true);
			meshCombiner.AddMeshData(newMesh);
		}

		meshCombiner.CombineTo(mesh);
		meshCombiner.Clear();

		DestroyImmediate(newMesh);
	}


	void transformMeshOver(Mesh mesh, bool forwardChange)
	{	//Move mesh over
		int direction = 1;
		if (! forwardChange)
			direction = -1;

		Vector3[] vertices = mesh.vertices;

		if (ConstantOffset != Vector3.zero)
		{	//Apply the constant offset
			for (int i = 0; i < vertices.Length; i++) 
				vertices[i] = vertices[i] + direction * ConstantOffset; //Take into account parent transforms...
		}
		if (RelativeOffset != Vector3.zero)
		{	//Apply the relative offset
			for (int i = 0; i < vertices.Length; i++) 
				vertices[i] = vertices[i] + direction * Vector3.Scale(mesh.bounds.size,RelativeOffset);
		}
		if (ObjectOffset != null)
		{
			Vector3 toEmpty = transform.InverseTransformDirection(ObjectOffset.position - transform.position);
			if (forwardChange)
			{
				for (int i = 0; i < vertices.Length; i++)
				{//For less operations, rotate, scale then translate
					vertices[i] = ObjectOffset.rotation * vertices[i]; //Rotate
					vertices[i] = Vector3.Scale(vertices[i],ObjectOffset.localScale);//Scale
					vertices[i] = vertices[i] + toEmpty; //Translate
				}
			}
			else
			{
				Vector3 localScale = ObjectOffset.localScale;
				Vector3 objectOffsetScale = new Vector3(1f/localScale.x,1f/localScale.y,1f/localScale.z);
				for (int i = 0; i < vertices.Length; i++)
				{//For less operations, rotate, scale then translate
					vertices[i] = Quaternion.Inverse(ObjectOffset.rotation) * vertices[i]; //Rotate
					vertices[i] = Vector3.Scale(vertices[i],objectOffsetScale);//Scale
					vertices[i] = vertices[i] - toEmpty; //Translate
				}
			}
		}

		mesh.vertices = vertices;
	}

	int getDuplicateAmount(Mesh mesh)
	{
		if (FitType == ArrayFitType.Count)
			return Mathf.RoundToInt(LengthVar);

		/*
		Vector3 oneOverScale = new Vector3(
			Math.Abs((transform.localScale.x)) > 0.001 ? 1.0f/transform.localScale.x : 1.0f,
			Math.Abs((transform.localScale.y)) > 0.001 ? 1.0f/transform.localScale.y : 1.0f,
			Math.Abs((transform.localScale.z)) > 0.001 ? 1.0f/transform.localScale.z : 1.0f);
		*/
		
		float LengthOfMesh = Vector3.Dot(RelativeOffset.normalized,Vector3.Scale(mesh.bounds.size, transform.localScale));

		if (FitType == ArrayFitType.Length)
			return Mathf.RoundToInt(LengthVar / LengthOfMesh);
		if (FitType == ArrayFitType.Spline && (Spline != null))
			return Mathf.RoundToInt(Spline.GetArcLength() / LengthOfMesh);
		
		throw new System.NotImplementedException();
	}
}
}
