﻿using UnityEditor;
using UnityEngine;

using JW.Spline;

namespace JW.Modifiers
{
[CustomEditor(typeof(ArrayModifier))] 
public class ArrayModifierEditor : Editor
{
	private SerializedObject mainObject;
	private ArrayModifier arrayModifier;

	public void OnEnable()
	{
		if (target != null)
		{
			mainObject = new SerializedObject(target);
			arrayModifier = (ArrayModifier)target;
		}
	}

	public override void OnInspectorGUI()
	{
		mainObject.Update(); //Load Changes from the Gameobject into the GUI

		arrayModifier.FitType = (ArrayModifier.ArrayFitType)EditorGUILayout.EnumPopup("Fit Type:", arrayModifier.FitType);
		switch (arrayModifier.FitType)
		{
			case ArrayModifier.ArrayFitType.Count:
				arrayModifier.LengthVar = (float)EditorGUILayout.IntField("Count:", (int)arrayModifier.LengthVar);
				break;
			case ArrayModifier.ArrayFitType.Length:
				arrayModifier.LengthVar = EditorGUILayout.FloatField("Length:", arrayModifier.LengthVar);
				break;
			case ArrayModifier.ArrayFitType.Spline:
				arrayModifier.Spline = (SplineManager)EditorGUILayout.ObjectField("Spline:", arrayModifier.Spline, typeof(SplineManager), true); 
				break;
			default:
				throw new System.NotImplementedException();		
		}

		EditorGUILayout.Space();

		arrayModifier.ConstantOffset = EditorGUILayout.Vector3Field("Constant Offset", arrayModifier.ConstantOffset, GUILayout.ExpandWidth(true));
		arrayModifier.RelativeOffset = EditorGUILayout.Vector3Field("Relative Offset", arrayModifier.RelativeOffset, GUILayout.ExpandWidth(true));
		arrayModifier.ObjectOffset = (Transform)EditorGUILayout.ObjectField("Object Offset",arrayModifier.ObjectOffset, typeof(Transform), true, GUILayout.ExpandWidth(true));

		EditorGUILayout.Space();

		arrayModifier.Start = (Mesh)EditorGUILayout.ObjectField("Start:", arrayModifier.Start, typeof(Mesh), true, GUILayout.ExpandWidth(true));
		arrayModifier.End = (Mesh)EditorGUILayout.ObjectField("End:", arrayModifier.End, typeof(Mesh), true, GUILayout.ExpandWidth(true));

		mainObject.ApplyModifiedProperties(); //Apply any changes
	}
	

}
}
