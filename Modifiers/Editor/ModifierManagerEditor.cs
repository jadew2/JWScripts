﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Collections.Generic;

namespace JW.Modifiers
{
    [CustomEditor(typeof(ModifierManager), true)]
    public class ModifierManagerEditor : ModifierCollectionEditor
    {
        private SerializedObject mainObject;
        static ModifierManager modifierClass;

        static readonly int FramesForSceneDeform = 10;
        static int frameCount = 0;

        new public void OnEnable()
        {
            mainObject = new SerializedObject(target);
            modifierClass = (ModifierManager)target;

            base.OnEnable();
        }

        public override void OnInspectorGUI()
        {
            mainObject.Update(); //Load Changes from the Gameobject into the GUI
            EditorGUILayout.LabelField("ModifierManager Editor V1.0 - What a rewrite!");

            base.OnInspectorGUI();

            //Deform button
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Scene Deform:", GUILayout.MaxWidth(95f));
                DeformInScene = GUILayout.Toggle(DeformInScene, GUIContent.none, GUILayout.MaxWidth(15f));
                if (!DeformInScene)
                {
                    if (GUILayout.Button("Deform"))
                        modifierClass.Evaluate();
                }
                GUILayout.EndHorizontal();
            }

            mainObject.ApplyModifiedProperties(); //Apply any changes
        }


        #region OnScene Stuff
        static bool deformInScene = false;
        static bool DeformInScene
        {
            get { return deformInScene; }
            set
            {
                if (value == deformInScene)
                    return;

                deformInScene = value;

                if (value)
                {
                    SceneView.duringSceneGui -= OnScene;
                    SceneView.duringSceneGui += OnScene;
                }
                else
                    SceneView.duringSceneGui -= OnScene;
            }
        }

        static void OnScene(SceneView view)
        {
            if (modifierClass == null)
            {
                DeformInScene = false;
                return;
            }

            frameCount++;

            if (frameCount >= FramesForSceneDeform)
            {
                frameCount = 0;
                modifierClass.Evaluate();
            }
        }

        #endregion

    }
}
