﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections.Generic;

namespace JW.Modifiers
{
[CustomEditor(typeof(ModifierCollection), true)]
public class ModifierCollectionEditor : Editor
{
	SerializedObject MainObject { get; set; }

	ModifierCollection ModifierClass { get; set; }

	ReorderableList reorderableList;

	static GameObject selectedGameobject;

	public void OnEnable()
	{
		MainObject = new SerializedObject(target);
		ModifierClass = (ModifierCollection)target;

		setUpReorderableList();
	}

	void setUpReorderableList()
	{
		List<ModifierBase> list = ModifierClass.Modifiers;

		reorderableList = new ReorderableList(list, typeof(ModifierBase), true, true, true, true);

		reorderableList.drawHeaderCallback = 
			(Rect rect) => EditorGUI.LabelField(rect, "ModifierBase List: " + list.Count);

		reorderableList.drawElementCallback =
			(Rect rect, int index, bool isActive, bool isFocused) =>
		{
			ModifierBase element = list[index];

			Rect elementRect = new Rect(rect.x, rect.y, 0f, EditorGUIUtility.singleLineHeight); //Rect used for the current element. Helps with offsets.

			//Starting "1. ModifierBase: " Label
			elementRect.width = 15f;
			EditorGUI.LabelField(elementRect, index + "."); 
			elementRect.x += elementRect.width; //Give the X a offset for the next element

			//Object field
			elementRect.width = rect.width - elementRect.x + 35f; //+35 to get the the right edge

				string label = (element != null) ? string.Format("{0} : {1}",  element.GetType().Name, element.gameObject.name) : "null";

				list[index] = (ModifierBase)EditorGUI.ObjectField(elementRect, label, element, typeof(ModifierBase), true); 
			
			//Make sure we aren't adding a collection within itself.
			if (list[index] == ModifierClass)
			{
				Debug.LogError(typeof(ModifierManagerEditor).Name + ": Cannot add collection within itself. This would cause a infinite loop. Aborting.");
				list[index] = null;
			}

			elementRect.x += elementRect.width;
		};

		reorderableList.onAddCallback = 
			(ReorderableList roList) =>
		{
			list.Insert(roList.index + 1, null);
			roList.index++; //Changes the selected index
		};

	}

	public override void OnInspectorGUI()
	{
		MainObject.Update(); //Load Changes from the Gameobject into the GUI
		DrawPropertiesExcluding(MainObject, "Modifiers");

		//Show the list
		if (ModifierClass.Modifiers != null)
			reorderableList.DoLayoutList();

		//Add modifiers from...
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Add Modifiers From:"))
			loadModifiersFrom(selectedGameobject);
		selectedGameobject = (GameObject)EditorGUILayout.ObjectField(selectedGameobject, typeof(GameObject), true, GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth * 0.57f));
		GUILayout.EndHorizontal();

		MainObject.ApplyModifiedProperties(); //Apply any changes
	}

	/// <summary>Loads the modifiers from the selected Gameobject. Throws a LogError if it's null. </summary>
	void loadModifiersFrom(GameObject gameobject)
	{
		if (gameobject == null)
		{
			Debug.LogError(typeof(ModifierManagerEditor).Name + ": No Selected Gameobject...");
			return;
		}

		ModifierBase[] bases = gameobject.GetComponents<ModifierBase>();
		for (int i = 0; i < bases.Length; i++)
		{
			if (!ModifierClass.Modifiers.Contains(bases[i]) && (bases[i] != ModifierClass)) /*This class isn't our current collection*/
				ModifierClass.Modifiers.Add(bases[i]);
		}
	}


}
}
