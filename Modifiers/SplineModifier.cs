﻿using UnityEngine;
using System.Collections.Generic;
using JW.Spline;
namespace JW.Modifiers
{
[AddComponentMenu("JWModifiers/Spline Modifier")]
public class SplineModifier : ModifierBase
{

	public SplineManager Spline;
	public float Influence = 1f;

	readonly List<float> pooledArcLengthList = new List<float>();

	//Set up mesh and deform vertices. Calls DeformVeritces
	public override void ApplyOn(Mesh mesh, Transform at)
	{
		if (Spline == null)
			throw new UnassignedReferenceException(typeof(SplineModifier).Name + ": Spline is not set.");
		if (Spline.Nodes == null || Spline.Nodes.Count == 0)
		{
			Debug.LogError(typeof(SplineModifier).Name + ": No nodes in spline!");
			return;
		}
		
		Vector3[] vertices = mesh.vertices;

		vertices = DeformVertices(vertices, at);

		mesh.vertices = vertices; //Bounds and normal calcs are done by the manager
	}

	//Iterate vertices[] and deform accordingly. Calls DeformVertex.
	Vector3[] DeformVertices(Vector3[] vertices, Transform at)
	{
		for (int i = 0; i < vertices.Length; i++)
		{
			//Get World point of the vertex, then make it to local to the spline. This is like Blender.
			Vector3 splineLocalVertex = Spline.transform.InverseTransformPoint(at.TransformPoint(vertices[i]));

			//Find the t of the point on the spline
			float distance = splineLocalVertex.z;
			float t = Spline.GetTForArcLength(distance, pooledArcLengthList);

			//Calculate world position
			Vector3 curveToVertex = Spline.RotationAt(t) * new Vector3(splineLocalVertex.x, splineLocalVertex.y, 0); //Flatten Z
			Vector3 worldVertex = at.InverseTransformPoint(Spline.PointAt(t) + curveToVertex); //Add the world point, transform back to local space

			//Set with influence parameter
			vertices[i] = vertices[i] + (worldVertex - vertices[i]) * Influence;
		}

		return vertices;
	}
}
}