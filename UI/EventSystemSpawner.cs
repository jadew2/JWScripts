﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace JW.UI
{
/// <summary>
/// Class that simply spawns a event system prefab if there's not UI event system in the scene.
/// </summary>
public class EventSystemSpawner : MonoBehaviour {

	[SerializeField] GameObject eventSystem = null;

	void Start()
	{
		EventSystem sceneEventSystem = FindObjectOfType<EventSystem>();
		if (sceneEventSystem == null)
		{
			Instantiate(eventSystem); //If there's no event system in the scene, create a new one
		}
	}
}
}