﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

namespace JW.Utils
{
/// <summary>
/// Has some helper methods for UI work.
/// </summary>
public static class UIUtils 
{
	/*
	/// <summary>
	/// Calls GetPixelAdjustedRect() on the passed graphic
	/// </summary>
	/// <returns>The non scaled pixel rect.</returns>
	/// <param name="graphic">Graphic.</param>
	public static Rect GetNonScaledPixelRect(Graphic graphic)
	{
		Rect rect = graphic.GetPixelAdjustedRect();

		CanvasScaler canvasScaler = graphic.gameObject.GetComponentInAncestors<CanvasScaler>();
		if (canvasScaler == null)
			return rect;

		//This canvas is being scaled, correct it
		Canvas canvas = canvasScaler.GetComponent<Canvas>();
		Rect canvasRect = canvas.pixelRect;

		if (canvasScaler.uiScaleMode == CanvasScaler.ScaleMode.ScaleWithScreenSize)
		{
			Vector2 referenceResolution = canvasScaler.referenceResolution;
			Vector2 actualResolution = canvasRect.size;

			Vector2 scaleFactor = new Vector2(actualResolution.x / referenceResolution.x,actualResolution.y / referenceResolution.y);
			return new Rect(rect.position * scaleFactor,rect.size * scaleFactor);
		}
		else
			throw new NotImplementedException()
	}
	*/

	/// <summary> Creates the UI prefab and parents it correctly. </summary>
	/// <returns>The new GameObject.</returns>
	public static GameObject UIInstantiate(GameObject prefab, Transform UIParent)
	{
		GameObject newRow = UnityEngine.Object.Instantiate<GameObject>(prefab);
		newRow.transform.SetParent(UIParent,false);
		return newRow;
	}

	/// <summary> Creates the UI prefab and parents it correctly. </summary>
	/// <returns>The new GameObject.</returns>
	public static T UIInstantiate<T>(GameObject prefab, Transform UIParent)
	{
		return UIInstantiate(prefab,UIParent).GetComponent<T>();
	}
}
}