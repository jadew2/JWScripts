﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace JW.UI.Notifications
{
/// <summary> A Yes/No notification prompt. </summary>
public class NotificationPromptYesNo : Notification
{
	protected Action<bool> onDone;
	protected bool result;

	/// <summary>  Initializes the prompt notification.  </summary>
	public void Init(string title, string body, Action<bool> onDone)
	{
		base.Init(title, body);
		this.onDone = onDone;
	}

	/// <summary> Raises the click "Yes" event. Usually called by the notification GUI. </summary>
	public virtual void ClickYes()
	{
		result = true;
		Close();
	}
		
	/// <summary> Raises the click "No" event. Usually called by the notification GUI. </summary>
	public virtual void ClickNo()
	{
		result = false;
		Close();
	}
		
	protected override void OnCloseOverride()
	{
		if (onDone != null)
			onDone (result); //Pass the result of yes/no
	}
}

}