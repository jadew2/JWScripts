﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace JW.UI.Notifications
{

/*A prompt with a string feild + Enter + Cancel*/
public class NotificationPromptString : Notification 
{
	[SerializeField] InputField inputTextField = null;

	string result = null;
	Action<string> onDone;

	/// <summary> Initializes a string prompt notification with a result callback. </summary>
	public void Init(string title, string body, Action<string> onDone)
	{
		base.Init(title,body);
		this.onDone = onDone;
	}

	/// <summary> Raises the click "Enter" event. Usually called by the notification GUI. </summary>
	public void ClickEnter()
	{
		result = inputTextField.textComponent.text;
		Close();
	}

	/// <summary> Raises the click "Exit" event. Usually called by the notification GUI. </summary>
	public void ClickExit()
	{
		result = null;
		Close();
	}

	protected override void OnOpenOverride()
	{
		base.OnOpenOverride();
		
		if (!Application.isMobilePlatform) //If we're on mobile, this causes the keyboard to suddenly appear.
			inputTextField.Select();
	}

	protected override void OnCloseOverride()
	{
		if (onDone != null)
			onDone(result);
	}
}

}