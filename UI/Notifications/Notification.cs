﻿using System;

using UnityEngine;
using UnityEngine.UI;

using JW.Tween;

namespace JW.UI.Notifications
{

/// <summary>
/// Handles the Title/Body text of a notification.
/// </summary>
/// <example>
/// notification.Init("This is a title","This is the body");
/// notification.Open();
/// notification.Close();
/// </example>
public class Notification : OpenClose
{
	[SerializeField] protected Text TitleText = null;
	[SerializeField] protected Text BodyText = null;

	/// <summary> Initializes the notification's text and animations. The title and body can be left null. </summary>
	public void Init(string title, string body)
	{
		//First Initialize the UI Textboxes.
		if (title != null)
			TitleText.text = title;

		if (body != null)
			BodyText.text = body;

		base.Init();
	}


}
}
