﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

using JW.Tween;

namespace JW.UI.Notifications
{
/// <summary> Class that helps easily display notifications. </summary>
/// <remarks>
/// 
/// Each type of Notification has 3 overloads:
/// 1. Create and display an alert.
/// 2. Create an alert with a default timeout
/// 3. Create an alert that exits (if not already) when whileCondition turns false. (Evaluated on Update)
///
/// Implementation Note: While it would be simpler, Public methods don't return the notification. This is to hide the Notification classes (less work when you change their flow).
/// This class mainly handles all the messy stuff of creating notifications.
/// 
/// Important: The CanvasPrefab needs a lot of components in order to work with all TPNotifications.
/// We need
/// 1. Canvas
/// 2. GraphicRaycaster (For buttons)
/// 3. EventSystem (For firing events from buttons)
/// 3. Some Inputmodule (For EventSystem, Default ones are fine)
/// </remarks>
public class NotificationFactory : MonoBehaviour 
{
	[SerializeField] GameObject NotificationCanvasPrefab = null;

	[SerializeField] GameObject AlertOneLinePrefab = null;
	[SerializeField] GameObject AlertPrefab = null;
	[SerializeField] GameObject PromptOkPrefab = null;
	[SerializeField] GameObject PromptYesNoPrefab = null;
	[SerializeField] GameObject PromptStringPrefab = null;

	GameObject canvasGo = null;

	/// <summary> Sets up the canvas prefab for the notifications. If components are missing, handle finding/creating them. </summary>
	void createCanvasGO()
	{
		canvasGo = Instantiate(NotificationCanvasPrefab);
		//canvasGo.transform.SetParent(transform, false); //Avoids UI scaling issues.

		//Check for components that without - bad things happen.
		if (canvasGo.GetComponent<Canvas>() == null)
			Debug.LogError(typeof(NotificationFactory).Name + ": Needs a Canvas on " + canvasGo.name);
		else if (canvasGo.GetComponent<GraphicRaycaster>() == null)
			Debug.LogError(typeof(NotificationFactory).Name + ": Needs a GraphicRaycaster on " + canvasGo.name + " for buttons.");

		//Find/create a EventSystem.
		/* Let the notification Canvas handle this...
		if (FindObjectOfType<EventSystem>() == null)
		{
			Debug.Log(typeof(NotificationFactory).Name + ": Could not find EventSystem in scene. Creating one on " + canvasGo.name);

			//Create the EventSystem on the canvas GO.
			canvasGo.AddComponent<StandaloneInputModule>();
			if (canvasGo.GetComponent<EventSystem>() == null)
				canvasGo.AddComponent<EventSystem>();
		}
		*/
	}

	#region Alert Notifications

	/// <summary> Creates an Alert displaying one line of text. To close, call Close() on the returned class. </summary>
	public NotificationAlert Alert(string text, Action onDone) 
	{
		NotificationAlert alertClass = instantiateAndReturn<NotificationAlert>(AlertOneLinePrefab);
		alertClass.Init(text,null,onDone);
		alertClass.Open(); //Display it
		return alertClass;
	}

	/// <summary> Creates an Alert displaying one line of text. Closes after seconds. </summary>
	public NotificationAlert Alert(string title, float forSeconds, Action onDone)
	{
		var alertClass = Alert(title, onDone);
		this.closeAfterSeconds(alertClass, forSeconds);
		return alertClass;
	}

	/// <summary> Creates an Alert displaying one line of text. Closes when the delegate evaluates to true (Checked every update). </summary>
	public NotificationAlert Alert(string title, Func<bool> closeWhen, Action onDone)
	{
		var alertClass = Alert(title, onDone);
		this.closeWhen(alertClass,closeWhen);
		return alertClass;
	}
		

	/// <summary> Creates an Alert with a title and body. To close, call Close() on the returned class. </summary>
	public NotificationAlert Alert(string title, string body, Action onDone)
	{
		NotificationAlert alertClass = instantiateAndReturn<NotificationAlert>(AlertPrefab);
		alertClass.Init(title,body, onDone);
		alertClass.Open(); //Display it
		return alertClass;
	}

	/// <summary> Creates an Alert with a title and body. Closes after seconds. </summary>
	public NotificationAlert Alert(string title, string body, float forSeconds, Action onDone)
	{
		var alertClass = Alert(title,body,onDone);
		closeAfterSeconds(alertClass, forSeconds);
		return alertClass;
	}

	/// <summary> Creates an Alert with a title and body. Closes when the delegate evaluates to true (Checked every update). </summary>
	public NotificationAlert Alert(string title, string body, Func<bool> closeWhen, Action onDone)
	{
		var alertClass = Alert(title,body,onDone);
		this.closeWhen(alertClass, closeWhen);
		return alertClass;
	}

	#endregion

	#region Prompt Notifications

	/// <summary> Creates a Notification with a title, body and "OK" button. onDone() is called on clicking OK. </summary>
	public NotificationPromptOk PromptOk(string title, string body, Action onDone)
	{
		NotificationPromptOk promptClass = instantiateAndReturn<NotificationPromptOk>(PromptOkPrefab);
		promptClass.Init(title,body,onDone);
		promptClass.Open();
		return promptClass;
	}

	/// <summary> Creates a Notification with a title, body, "Yes" and "No" button. onDone is called with the result. </summary>
	public NotificationPromptYesNo PromptYesNo(string title, string body, Action<bool> onDone)
	{
		NotificationPromptYesNo promptClass = instantiateAndReturn<NotificationPromptYesNo>(PromptYesNoPrefab);
		promptClass.Init(title,body,onDone);
		promptClass.Open();
		return promptClass;
	}

	/// <summary> Creates a Notification with a title, body, and a input string field. onDone is called with the result. The result can be null. </summary>
	public NotificationPromptString PromptString(string title, string body, Action<string> onDone)
	{
		NotificationPromptString promptClass = instantiateAndReturn<NotificationPromptString>(PromptStringPrefab);
		promptClass.Init(title,body,onDone);
		promptClass.Open();
		return promptClass;
	}	
		


	#endregion

	#region Helper Methods

	/// <summary> Instatiates the prefab as a child of the main canvas. Then returns the notification component. </summary>
	protected T instantiateAndReturn<T>(GameObject prefab)
	{
		GameObject newGO = GameObject.Instantiate(prefab);

		if (canvasGo == null)
			createCanvasGO();

		newGO.transform.SetParent(canvasGo.transform,false);
		return newGO.GetComponentInChildren<T>();
	}

	/// <summary> Closes the notification after so many seconds. </summary>
	protected void closeAfterSeconds(Notification notification, float seconds)
	{
		SJTween.Invoke(seconds, ()=>
			{
				if(notification != null && (notification.Status == Notification.OpenCloseStatus.Open))
					notification.Close();
			});
	}

	/// <summary> Closes the notification when "when()" evaluates to true. </summary>
	protected void closeWhen(Notification notification, Func<bool> when)
	{
		SJTween.InvokeWhen(when, () =>
			{
				if(notification != null && (notification.Status == Notification.OpenCloseStatus.Open))
					notification.Close();
			});
	}
		
	#endregion

}
}
