﻿using UnityEngine;
using System.Collections;
using System;

namespace JW.UI.Notifications
{

/// <summary>
/// A simple altert notification. Has settings to be dismissable, and has a callback.
/// </summary>
public class NotificationAlert : Notification 
{
	Action onDone;

	///<summary> Initializes a Alert Notification. </summary> 
	public void Init(string title, string body, Action onDone)
	{
		base.Init(title, body);
		this.onDone = onDone;
	}

	protected override void OnCloseOverride()
	{
		if (onDone != null)
			onDone();
	}

	public void SetTitleText(string text)
	{
		TitleText.text = text;
	}
}

}