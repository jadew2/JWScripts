﻿using UnityEngine;
using System.Collections;
using System;

using JW.UI.Notifications;

public class TestNotification : MonoBehaviour 
{
	public NotificationFactory notificationClass;

	void Start () 
	{
		showOneLine();
	}

	void showOneLine()
	{
		notificationClass.Alert("Hey there. Welcome", 2f, showPlayerNamePrompt);
	}

	void showPlayerNamePrompt()
	{
		notificationClass.PromptString("Player Name", "Welcome :) Please enter a player name!",showConfirmationPrompt);
	}

	void showConfirmationPrompt(string playerName)
	{
		notificationClass.Alert("Player Name", "Your name is \"" + playerName + "\"...  That's a pretty lame name.", 3f,showPrompt);
	}

	void showPrompt()
	{
		notificationClass.PromptYesNo("Change Name?", "Are you really sure this name is fine?", result => showLoadingLevel() );
	}

	void showLoadingLevel()
	{
		notificationClass.PromptOk("Ok then.", "Don't say I didn't warn you.", showProof);
	}

	void showProof()
	{
		notificationClass.PromptOk("Ok then.", "This is proof it works", null);
	}
}
