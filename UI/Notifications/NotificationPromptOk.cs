﻿using UnityEngine;
using System.Collections;

using System;

namespace JW.UI.Notifications
{
/// <summary> A Yes/No notification prompt. </summary>
public class NotificationPromptOk : Notification
{
	Action onDone;

	/// <summary>  Initializes the prompt notification.  </summary>
	public void Init(string title, string body, Action onDone)
	{
		base.Init(title, body);
		this.onDone = onDone;
	}

	/// <summary> Raises the click "Ok" event. Usually called by the notification GUI. </summary>
	public void ClickOk()
	{
		Close();
	}

	protected override void OnCloseOverride()
	{
		if (onDone != null)
			onDone(); //Pass the result of yes/no
	}
}

}
