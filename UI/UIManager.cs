﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace JW.UI
{
/// <summary> A manager class for UI prefabs. It's main purpose is to gather screen UI systems in one place. </summary>
public class UIManager : MonoBehaviour 
{
	[SerializeField] GameObject eventSystemPrefab = null;
	[SerializeField] GameObject defaultCanvasPrefab = null;

	List<GameObject> uICanvases = new List<GameObject>();

	void Start()
	{
		//If there's no event system in the scene, create a new one
		EventSystem sceneEventSystem = FindObjectOfType<EventSystem>();
		if (sceneEventSystem == null)
			Instantiate(eventSystemPrefab); 
	}

	public GameObject GenerateScreenCanvas()
	{
		GameObject newCanvas = Instantiate(defaultCanvasPrefab);
		uICanvases.Add(newCanvas);
		return newCanvas;

	}

	public GameObject InstantiateUIElement(GameObject canvas, GameObject prefab)
	{
		return null;
	}


}
}