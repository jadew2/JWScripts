﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

namespace JW.UI
{
/// <summary>
/// Integrates Unity Events with the OpenClose system.
/// </summary>
public class OpenCloseEvents : OpenCloseListener 
{
	public UnityEvent onOpen;
	public UnityEvent onClose;

	protected override void OnOpen()
	{
		onOpen.Invoke();
	}

	protected override void OnClose()
	{
		onClose.Invoke();
	}
}
}
