﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JW.UI;
using JW.UI.Effects;

namespace JW.UI
{
[RequireComponent(typeof(CanvasGroupAlphaFader))]
public class OpenCloseAlphaFader : OpenCloseListener 
{
	CanvasGroupAlphaFader alphaFader;

	public float FadeInDuration = 1f;
	public float FadeOutDuration = 1f;

	protected override void Awake()
	{
		base.Awake();
		alphaFader = GetComponent<CanvasGroupAlphaFader>();
	}

	protected override void OnOpen()
	{
		base.OnOpen();
		alphaFader.FadeIn(FadeInDuration);
	}

	protected override void OnClose()
	{
		base.OnClose();
		alphaFader.FadeOut(FadeOutDuration);
	}
}
}
