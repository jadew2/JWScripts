﻿using UnityEngine;
using System;
using System.Collections;

using JW.Tween;

namespace JW.UI
{
/// <summary> A base class for things that Open and Close. Handles not calling events twice from IOpenClose </summary>
/// <example>
/// notification.Init();
/// notification.Open();
/// notification.Close();
/// </example>
public class OpenClose : MonoBehaviour , IOpenCloseEvents
{
	public event Action OnOpen = () => {};
	public event Action OnClose = () => {};

	public enum OpenCloseStatus 
	{
		Uninitialized, //Init() not called.
		Initialized, //Init() called.
		Open, //Open() at least called
		Closed // Close() called.
	}
	public OpenCloseStatus Status { get; private set;}

	public enum OpenCloseMode
	{
		Default, //Do nothing extra when opening and closing.
		DestroyOnClose, //Destroy the gameobject ~2 seconds after closing, unless opened again in that time.
		EnableDisable, //Enable when opening, disable ~2 seconds after closing, unless opened again in that time.
	}
	public OpenCloseMode openCloseMode = OpenCloseMode.DestroyOnClose; //What common actions to do when opening/closing.
	public bool AutoInit = false; //Call Init() in start. Set to false if you want to ensure the content is set before it opens.
	public float delayTime = 0.5f; //Time to wait to disable/destroy after closing.
	public bool UseRealtime = false; //Use realtime to count down the delay time.

	#region Public Methods

	protected virtual void Start()
	{
		if (AutoInit && Status == OpenCloseStatus.Uninitialized)
			Init();
	}

	/// <summary> Initializes the notification's text and animations. The title and body can be left null. </summary>
	public void Init()
	{
		if (Status != OpenCloseStatus.Uninitialized && Status != OpenCloseStatus.Closed)
		{
			Debug.LogWarning(typeof(OpenClose).Name + ": Init called more than once. Doing nothing.");
			return;
		}

		Status = OpenCloseStatus.Initialized;
		OnInitOverride();
	}

	/// <summary> Displays the notification. </summary>
	public void Open()
	{
		if (AutoInit && Status == OpenCloseStatus.Uninitialized)
			Init();

		if (openCloseMode == OpenCloseMode.EnableDisable)
			this.gameObject.SetActive(true);

		if (Status == OpenCloseStatus.Uninitialized)
		{
			Debug.LogWarning(typeof(OpenClose).Name + ": Open: Notification has not been initialized. Doing nothing.");
			return;
		}
		if (Status == OpenCloseStatus.Open)
		{
			Debug.LogWarning(typeof(OpenClose).Name + ": Open: Notification was already opened. Doing nothing.");
			return;
		}
		if (Status == OpenCloseStatus.Closed)
			stopCloseTween();

		Status = OpenCloseStatus.Open;
		OnOpenOverride();
		OnOpen();
	}



	public void Close()
	{
		this.Close(openCloseMode);
	}

	/// <summary> Closes the notification. By it destroys the GO after a couple seconds. So get what you want done in OnCloseOverride and OnClose. </summary>
	public void Close(OpenCloseMode closeModeOverride)
	{
		if (Status != OpenCloseStatus.Open)
		{
			Debug.LogWarning(typeof(OpenClose).Name + ": Close: Notification was not opened. Doing nothing.");
			return;
		}

		Status = OpenCloseStatus.Closed;
		OnCloseOverride();
		OnClose();

		if (closeModeOverride != OpenCloseMode.Default)
			startCloseTween(closeModeOverride);
	}

	#endregion

	#region Methods to be Overriden
	/// <summary> Called near the end of Init(). For child classes. </summary>
	protected virtual void OnInitOverride(){}
	/// <summary> Called near the end of Open(). For child classes. </summary>
	protected virtual void OnOpenOverride(){}
	/// <summary> Called near the end of Close(). For child classes. </summary>
	protected virtual void OnCloseOverride(){}

	#endregion

	/// <summary> Use realtime for destroying the openclose. Useful for things that happen in the pause menu.  </summary>
	ITween closeTween;
	/// <summary> Destroys the gameobject after x seconds. </summary>
	void startCloseTween(OpenCloseMode mode)
	{
		closeTween = SJTween.Invoke(delayTime, ()=>
		{
			if(this != null && this.gameObject != null)
			{
				if (mode == OpenCloseMode.DestroyOnClose)
					Destroy(this.gameObject);
				else if (mode == OpenCloseMode.EnableDisable)
					this.gameObject.SetActive(false);
				else
					Debug.LogError("Unhandled CloseMode: " + mode);
			}
		});
		if (UseRealtime)
			closeTween.SetUpdateType(TweenUpdate.RealTime);
	}

	/// <summary> Stops the destroy if called after destroyAfterSeconds </summary>
	void stopCloseTween()
	{
		if (closeTween != null)
			closeTween.Stop();
	}
}
}