﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.UI
{
/// <summary>
/// Simple listener monobehaviour. 
/// Finds a IOpenCloseEvents class on Awake, and calls OnOpen/OnClose from those events.
/// </summary>
/// <remarks>
/// It's this way because I wrote a couple classes that shared this similarity.
/// It's nice to keep the events in case the user wants to listen to just one.
/// </remarks>
public class OpenCloseListener : MonoBehaviour
{
	protected IOpenCloseEvents eventsClass;

	protected virtual void Awake()
	{
		//Listen to events from the event class
		eventsClass = GetComponent<IOpenCloseEvents>();
		eventsClass.OnOpen += OnOpen;
		eventsClass.OnClose += OnClose;
	}

	protected virtual void OnDestroy()
	{
		eventsClass.OnOpen -= OnOpen;
		eventsClass.OnClose -= OnClose;
	}

	protected virtual void OnOpen()
	{
		//To be overridden
	}

	protected virtual void OnClose()
	{
		//To be overridden
	}
}
}
