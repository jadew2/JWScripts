﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace JW.UI
{
[RequireComponent(typeof(OpenClose))]
public class OpenCloseTester : MonoBehaviour 
{
	public bool Init = true;
	public bool Open = true;
	public bool Close = true;

	OpenClose openClose = null;

	void Awake()
	{
		openClose = GetComponent<OpenClose>();
	}

	// Use this for initialization
	void Start () 
	{
		if (Init)
			openClose.Init();
		if (Open)
			openClose.Open();
		if (Close)
			openClose.Close();
	}

}
}