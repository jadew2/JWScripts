﻿using UnityEngine;
using System.Collections;
using System;

namespace JW.UI
{
/// <summary>
/// Simple interface for UI window like objects in Unity.
/// </summary>
public interface IOpenCloseEvents
{
	event Action OnOpen;
	event Action OnClose;
}
}