﻿using UnityEngine;
using System.Collections;

namespace JW.UI
{
/// <summary>
/// Class that adds sound effect to the open/closing of things which implement the IOpenCloseEvent interface
/// </summary>
public class OpenCloseSound : MonoBehaviour 
{
	public AudioSource OpenSound = null;
	public AudioSource CloseSound = null;

	IOpenCloseEvents eventsClass;

	void Awake()
	{
		//First, listen to events from the event class
		eventsClass = GetComponent<IOpenCloseEvents>();
		eventsClass.OnOpen += onOpen;
		eventsClass.OnClose += onClose;
	}
		
	void onOpen()
	{
		if (OpenSound != null)
			OpenSound.Play();
	}

	void onClose()
	{
		if (CloseSound != null)
			CloseSound.Play();
	}
}
}