﻿using UnityEngine;
using System.Collections;
using JW.UI;
using JW.Tween;

public class OpenCloseUICanvasSpawner : MonoBehaviour 
{

	[SerializeField] GameObject CanvasPrefab = null;
	[SerializeField] GameObject OpenClosePrefab = null;

	public bool DestroyCanvasOnClose = true;
	public float DestroyWaitTime = 1f;

	public virtual void SpawnPrefab()
	{
		GameObject newCanvas = Instantiate(CanvasPrefab);

		GameObject newOpenCloseGo = Instantiate(OpenClosePrefab);
		newOpenCloseGo.transform.SetParent(newCanvas.transform,false);

		OpenClose openClose = newOpenCloseGo.GetComponent<OpenClose>();

		if (DestroyCanvasOnClose)
			setUpCloseEvent(newCanvas,openClose);

		openClose.Init();
		openClose.Open();
	}

	/// <summary> Sets up events to destroy the canvas when it closes </summary>
	void setUpCloseEvent(GameObject canvasGo, OpenClose openClose)
	{
		openClose.OnClose += () => {
			SJTween.Invoke(DestroyWaitTime,() => 
			{	
				if (!ExtensionMethods.IsNullOrDestroyed(canvasGo))
					Destroy(canvasGo);
			});
		};
	}
}
