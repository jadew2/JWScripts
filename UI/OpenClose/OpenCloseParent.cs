﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JW.Tween;
using System;

namespace JW.UI
{
public class OpenCloseParent : OpenCloseListener 
{
	public List<OpenClose> Children = new List<OpenClose>();

	public float OpenDelay = 0;
	public float CloseDelay = 0;

	List<ITween> tweens = new List<ITween>();

	protected override void OnOpen()
	{
		finishAllTweens();
		foreach (var child in Children)
			invoke(OpenDelay,child.Open);
	}

	protected override void OnClose()
	{
		finishAllTweens();
		foreach (var child in Children)
			invoke(CloseDelay,child.Close);
		
	}

	void invoke(float delay, Action action)
	{
		tweens.Add(SJTween.Invoke(delay,action));
	}

	void finishAllTweens()
	{
		//Forces all tweens to trigger their "OnEdge()" event.
		//For .Invoke, this fires the action and stops the tween.
		foreach (var tween in tweens)
		{
			if (tween.State != TweenState.Stopped) //Check doesn't need to be here, but let's be safe.
			{
				tween.ElapsedTime = tween.Duration + 1f;
				tween.Evaluate(1f);
			}
		}
		tweens.Clear();
	}
}
}
