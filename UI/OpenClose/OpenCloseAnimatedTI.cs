﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JW.CameraControl;
using JW.UI;

namespace JW.UI
{
    public class OpenCloseAnimatedTI : OpenCloseListener
    {
        [SerializeField] AnimatedTransformInfluence TransformInfluence = null;

        [SerializeField] Transform OpenTarget = null;
        [SerializeField] float OpenTransitionDuration = 2f;
        [SerializeField] Transform CloseTarget = null;
        [SerializeField] float CloseTransitionDuration = 2f;

        protected override void OnOpen()
        {
            if (OpenTarget != null)
                TransformInfluence.TransitionTo(OpenTarget, OpenTransitionDuration);
        }

        protected override void OnClose()
        {
            if (CloseTarget != null)
                TransformInfluence.TransitionTo(CloseTarget, CloseTransitionDuration);
        }

    }
}


