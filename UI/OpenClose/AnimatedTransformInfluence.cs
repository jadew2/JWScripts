﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using JW.Constraints;
using JW.Tween;
using JW.Tween.Internal;

namespace JW.CameraControl
{
///<summary>  
/// Class that makes it easy to control the camera using other GO's. Use TransitionTo to transition the attached GO to a transform.
/// </summary>
/// <remarks>
/// Manages the MultipleTransformInfluence constraint entirley, and doesn't share it.
/// </remarks>
public class AnimatedTransformInfluence : MultipleTransformInfluence 
{
	//---Multiple transform influence constraint: Attached to our camera.
	CollectionTween tweenCollection;
	
	protected override void Awake()
	{
		base.Awake();
		tweenCollection = new CollectionTween();
	}

	public void TransitionTo(Transform transform, float duration)
	{
		TransitionTo(transform,duration,TweenEase.inOutQuad);
	}

	public void TransitionTo(Transform transform, float duration, Func<float,float> easeType)
	{
		if (transform == null)
			throw new ArgumentNullException(typeof(AnimatedTransformInfluence).Name + ": You cannot pass a null transform.");
		
		//Starts lerping all transforms currently in list to influence = 0. Lerps requested transform to influence = 1.
		//The list is cleaned up on the fly using tween callbacks.

		//Stop all transitions
		tweenCollection.Stop();

		//Check if we already have that transform in the list.
		var foundTI = TransformInfluences.Find(x => x.Transform == transform);
		if (foundTI != null) //If so, remove it.
			TransformInfluences.Remove(foundTI); 

		//Start tweening other influences to 0.
		for (int i = 0; i < TransformInfluences.Count; i++) 
			createTransition(TransformInfluences[i],0f,duration,easeType);
		
		//Re-add our transform
		if (foundTI != null)
			TransformInfluences.Add(foundTI); //Re-add it if we removed it
		else
			TransformInfluences.Add(new MultipleTransformInfluence.TransformInfluence(transform));

		//Tween it to 1.
		createTransition(TransformInfluences[TransformInfluences.Count-1],1f,duration,easeType);
	}

	void createTransition(MultipleTransformInfluence.TransformInfluence ti, float to, float duration, Func<float,float> easeType)
	{
		if (duration < 0)
			duration = 0.0f;
		
		ITween newTween = SJTween.Value(ti.Influence,to,duration,Mathf.Lerp, t => ti.Influence = t); 
		newTween.SetEaseType(easeType);//Transition Ease type
		newTween.OnStop += () => 
		{
			//Remove ourself from the TransformInfluence List if we are done and at 0f.
			if (this == null || TransformInfluences == null || ti == null) //Assume NOTHING.
				return;
			if (cameraBeingDestroyed || Math.Abs(ti.Influence) < float.Epsilon)
				TransformInfluences.Remove(ti);
		};
		
		tweenCollection.Tweens.Add(newTween);
	}
	
	
	//--- Runtime Housekeeping
	protected override void OnEnable()
	{
		base.OnEnable();
		if (tweenCollection != null)
			tweenCollection.Play();
	}
	
	protected override void OnDisable()
	{
		base.OnDisable();
		if (tweenCollection != null)
			tweenCollection.Pause();
	}
	
	bool cameraBeingDestroyed = false;
	void OnDestroy()
	{
		TransformInfluences.Clear();
		cameraBeingDestroyed = true;
		if (tweenCollection != null)
			tweenCollection.Stop();
	}
	
	public void ClearInfluencesAndTransitions()
	{
		base.SetAllInfluences(0f);
		TransformInfluences.Clear();
		
		tweenCollection.Stop();
		tweenCollection.Tweens.Clear();
	}
}
}
