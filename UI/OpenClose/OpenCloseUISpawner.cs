﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using JW.UI;

public class OpenCloseUISpawner : MonoBehaviour 
{
	[SerializeField] GameObject UIParent = null;
	[SerializeField] GameObject OpenClosePrefab = null;

	public virtual void SpawnPrefab()
	{
		OpenClose openClose = SpawnPrefabDontOpen();
		openClose.Init();
		openClose.Open();
	}

	public virtual T SpawnPrefabDontOpen<T>() where T : OpenClose
	{
		return (T) SpawnPrefabDontOpen();
	}
	
	public virtual OpenClose SpawnPrefabDontOpen()
	{
		//If the UIParent is null, find the nearest canvas
		if (UIParent == null)
		{
			Canvas targetCanvas = gameObject.GetComponent<Canvas>();
			if (targetCanvas == null)
				targetCanvas = gameObject.GetComponentInAncestors<Canvas>();

			if (targetCanvas == null)
			{
				Debug.LogError(typeof(OpenCloseUISpawner).Name + ": Cannot find parent Canvas to spawn prefab off of!");
				return null;
			}

			UIParent = targetCanvas.gameObject;
		}

		GameObject newGo = Instantiate(OpenClosePrefab);
		newGo.transform.SetParent(UIParent.transform,false);

		return newGo.GetComponent<OpenClose>();
	}

}
