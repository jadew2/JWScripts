﻿using UnityEngine;
using System.Collections;

namespace JW.UI
{
/// <summary> Class that animates the open/closing of things which implement the IOpenCloseEvent interface </summary>
[RequireComponent (typeof(Animator))]
public class OpenCloseAnimation : OpenCloseListener 
{
	Animator animator;
	public string OpenBoolParameter = "Open";

	protected override void Awake()
	{
		base.Awake();
		//Initialize the animator.
		animator = GetComponent<Animator>();
	}
		
	protected override void OnOpen()
	{
		base.OnOpen();
		animator.SetBool(OpenBoolParameter, true);
	}

	protected override void OnClose()
	{
		base.OnClose();
		animator.SetBool(OpenBoolParameter, false);
	}
}
}