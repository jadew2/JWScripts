﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace JW.UI
{

/// <summary> 
/// Helps create/remove row/grid items on UI content views. 
/// Makes it so only one GO can be selected. 
/// Works with SelectableUIItem to make this happen. 
/// <summary>
/// <description>
/// Allows Zero to be selected initially if you have the option set.
/// Also, if you have "Use none row" set, you can register a none row. 
/// The none row is automatically selected when no others are. 
/// </description>
public class SelectableUIContentManager : UIContentManager 
{
	public event Action<GameObject> OnSelectionChanged = selectedRow => {};

	/// <summary>
	/// Allow none to be selected initially. 
	/// </summary>
	public bool AllowNoneInitially = false;
	public bool UseNoneRow = false;
	/// <summary> 
	/// The none row, which is automatically selected when no others are. 
	/// If you wish to use one, you must set this bool to true.
	/// </summary>
	SelectableUIItemNone noneRow = null;

	GameObject selectedItem = null;
	public GameObject SelectedItem
	{
		get { return selectedItem; }
		set
		{
			if (selectedItem != value)
			{
				selectedItem = value;
				OnSelectionChanged(selectedItem);
			}
		}
	}

	#region Selectable Row Events

	public void Register(SelectableUIItem item) 
	{ 
		item.OnPostSelectedUICallback += onRowSelected;
		item.OnPostDeselectedUICallback += onRowDeselected;

		if (item is SelectableUIItemNone)
		{
			if (noneRow != null)
				Debug.LogError(typeof(SelectableUIContentManager).Name + ": You cannot add a second " + typeof(SelectableUIItemNone).Name + " option to the list.");
			else
				noneRow = item as SelectableUIItemNone;
		}
	}

	public void DeRegister(SelectableUIItem item)
	{
		item.OnPostSelectedUICallback -= onRowSelected;
		item.OnPostDeselectedUICallback -= onRowDeselected;

		if (item is SelectableUIItemNone)
		{
			if (((SelectableUIItem)noneRow) == item)
				noneRow = null;
		}
	}

	/// <summary> Selects the passed row and deselects all other rows. </summary>
	void onRowSelected(SelectableUIItem item)
	{
		var otherRows = getSelectableRows().Where(x => x != item);
		//SelectableUIItem[] items = otherRows.ToArray();

		//Deselect all other rows
		foreach (var selectableRow in otherRows)
			selectableRow.Selected = false;

		SelectedItem = item.gameObject;
	}

	/// <summary> If there are no other rows, and the row wants to be deselected, select it instantly again. </summary>
	void onRowDeselected(SelectableUIItem item)
	{
		var otherSelectedRows = getSelectableRows().Where(x => x != item).Where(x => x.Selected);

		if (UseNoneRow && noneRow != null)
		{
			//First, search through the rows to see if we're currently selecting another one.
			//If so, we don't want to set the SelectedItem to null. 
			//(It would fire that we selected nothing, then that object. That's not what you would expect!)
			if (!otherSelectedRows.Any())
			{
				//Select none
				noneRow.Selected = true;
			}
			return; //We don't care about selecting a different one.
		}

		//If we are here, we do.
		if (!otherSelectedRows.Any()) 
		{
			item.Selected = true;
			SelectedItem = item.gameObject;
		}
	}

	#endregion

	#region RowManager events

	/// <summary> Creates a new UI content item. Handles selecting it if needed. </summary>
	public override GameObject CreateItem (GameObject itemGameobject)
	{
		GameObject newGO = base.CreateItem (itemGameobject);

		SelectableUIItem selectableItem = newGO.GetComponent<SelectableUIItem>();
		if (selectableItem != null)
			selectableItem.SetContentManager(this);

		//If we have one row, select that automatically
		if (ContentItems.Count() == 1)
		{
			if (UseNoneRow && selectableItem == noneRow) //If we are adding a none row when we want to use one, select that first.
				selectItem(newGO);
			else if (!(UseNoneRow || AllowNoneInitially)) //If we don't want to use a none row, select the first row that's added.
				selectItem(newGO);
		}

		return newGO;
	}

	/// <summary> Creates a new UI content item. Handles selecting it if needed. </summary>
	public override T CreateItem<T> (GameObject itemGameobject)
	{
		return this.CreateItem(itemGameobject).GetComponent<T>();
	}

	/// <summary> Destroys the passed gameobject and selects the closest row. </summary>
	public override bool DeleteItem (GameObject itemGameobject)
	{
		int index = ContentItems.ToList().IndexOf(itemGameobject);
		bool result = base.DeleteItem(itemGameobject);

		IEnumerable<GameObject> newContentList = ContentItems; //Optimization
		int newContentListCount = newContentList.Count(); //Optimization

		//If there are no rows, select nothing
		if (newContentListCount == 0)
		{
			selectItem(null);
			return result;
		}

		//If it was the selected row
		if (index != -1 && itemGameobject == SelectedItem)
		{
			//Select the closest row still in the list.
			int wantedIndex = index;
			if (wantedIndex >= newContentListCount)
				wantedIndex = newContentListCount - 1; //Select the last element
			
			//We're sure there's at lest one row left because of our check for no elements earlier
			selectItem(newContentList.ElementAtOrDefault(wantedIndex)); //Select it
		}

		return result;
	}

	/// <summary>
	/// Does what the method says. What, are you stupid?
	/// Also sets the selected row to null.
	/// </summary>
	public override void DeleteAllItems()
	{
		base.DeleteAllItems();
		selectItem(null);
	}

	#endregion

	/// <summary> Selects a rowGO. </summary>
	void selectItem(GameObject itemGO)
	{
		if (itemGO == null) 
		{
			SelectedItem = null;
			return;
		}

		SelectableUIItem selectableClass = itemGO.GetComponent<SelectableUIItem>();
		if (selectableClass != null)
		{
			selectableClass.Selected = true;
			SelectedItem = selectableClass.gameObject;
		}
	}

	/// <summary> Returns all SelectableRows in UIContentManager's ContentItems </summary>
	IEnumerable<SelectableUIItem> getSelectableRows()
	{
		return ContentItems
			.Select(go => go.GetComponent<SelectableUIItem>())
			.Where(selectableRowClass => selectableRowClass != null);
	}

}
}
