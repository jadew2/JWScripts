﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using UnityEngine.EventSystems;

namespace JW.UI
{
/// <summary>
/// Rotates a Gameobject from the user tapping and dragging their finger over a UI Panel.
/// </summary>
public class PanelMaskedTurntableSpin : MonoBehaviour , IDragHandler
{
	public Vector3 Axis = Vector3.up;
	public float RotationPerScreenDrag = 360f;

	public RectTransform RotationUIPanel;
	public Transform Target;

	// Update is called once per frame
	void Update ()
	{
		/*
		if (Application.isMobilePlatform && !Application.isEditor)
			touchMethod();
		else
			mouseMethod();
		*/
	}
	
	public void OnDrag(PointerEventData eventData)
	{
		Vector2 screenDelta = toNormalizedCoords(eventData.delta);
		
		float rotation = screenDelta.x * RotationPerScreenDrag;
		Target.Rotate(Axis,rotation);
	}
	
	/*
	Vector2 previousMousePosition;
	void mouseMethod()
	{
		handleRotation(
			Input.GetMouseButton(0), //Is the mouse down currently?
			Input.GetMouseButtonDown(0), //Is this the first frame the mouse was down?
			true, //Has the mouse moved? (We just say true in this case)
			toVector2(Input.mousePosition), //ScreenPosition
			toVector2(Input.mousePosition) - previousMousePosition); //DeltaScreenPosition
		
		previousMousePosition = Input.mousePosition;
	}

	void touchMethod()
	{
		if (Input.touchCount != 1)
			return;
		Touch touch = Input.GetTouch(0);

		handleRotation(
			true,	//Is the touch down? (Always if we reached this point)
			touch.phase == TouchPhase.Began, //Has the touch begin?
			touch.phase == TouchPhase.Moved, //Has the touch moved?
			touch.position, //ScreenPosition
			touch.deltaPosition); //DeltaScreenPosition
	}

	void handleRotation(bool isMousePressed, bool isMouseBeginPhase, bool hasMouseMoved, Vector2 screenPosition, Vector2 deltaScreenPosition)
	{
		if (!isMousePressed)
		{
			doDrag = false;
			return;
		}

		if (isMouseBeginPhase)
		{
			if (RectTransformUtility.RectangleContainsScreenPoint(RotationUIPanel,screenPosition))
				doDrag = true;
		}

		if (doDrag && hasMouseMoved)
		{
			Vector2 deltaTouchPosition = toNormalizedCoords(deltaScreenPosition);
			float xPosDifference = deltaTouchPosition.x;

			float rotation = xPosDifference * RotationPerScreenDrag;
			Target.Rotate(Axis,rotation);
		}
	}

	*/
	Vector2 toNormalizedCoords (Vector2 pixelCoords)
	{
		Vector2 pixelScreenSize = new Vector2(Screen.width,Screen.height);
		return new Vector2(pixelCoords.x / pixelScreenSize.x,pixelCoords.y / pixelScreenSize.y);
	}
	

	
}
}
