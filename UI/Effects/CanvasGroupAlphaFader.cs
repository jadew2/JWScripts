﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using JW.Tween;

namespace JW.UI.Effects
{
[RequireComponent(typeof(CanvasGroup))]
public class CanvasGroupAlphaFader : MonoBehaviour , IAlphaFader
{
	[SerializeField] protected CanvasGroup CanvasGroup = null;
	ITween tween;
	public bool StartTransparent = true;
	public bool UseRealTime = false;

	float originalAlpha = 1.0f;

	//Flag set if FadeIn() or FadeOut() was called before start.
	bool fadeAlreadyTriggered = false;

	protected virtual void Awake()
	{
		if (CanvasGroup == null)
			CanvasGroup = GetComponent<CanvasGroup>();
		originalAlpha = CanvasGroup.alpha;
	}

	protected virtual void Start()
	{
		if (StartTransparent && !fadeAlreadyTriggered)
			SetAlpha(0);
	}

	public void SetAlpha(float value)
	{
		CanvasGroup.alpha = value;
		
		if (tween != null)
			tween.Stop();
	}

	public void FadeIn(float duration)
	{
		FadeTo(originalAlpha,duration);
	}

	public void FadeOut(float duration)
	{
		FadeTo(0f,duration);
	}

	public void FadeTo(float value, float duration)
	{
		if (tween != null)
			tween.Stop();

		tween = SJTween.Value(CanvasGroup.alpha,value,duration, alph =>
		{
			if (ExtensionMethods.IsNullOrDestroyed(CanvasGroup))
			{
				if (tween != null && tween.State == TweenState.Playing)
					tween.Stop();
				CanvasGroup = null;
				return;
			}
			
			CanvasGroup.alpha = alph;
		});
		
		if (UseRealTime)
			tween.SetUpdateType(TweenUpdate.RealTime);
		tween.EaseType = TweenEase.outCubic;

		fadeAlreadyTriggered = true;
	}
}
}
