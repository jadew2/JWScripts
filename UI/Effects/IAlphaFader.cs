﻿using System.Collections;
using System.Collections.Generic;

namespace JW.UI.Effects
{
public interface IAlphaFader
{
	void SetAlpha(float value);
	void FadeIn(float duration);
	void FadeOut(float duration);
	void FadeTo(float value, float duration);
}
}