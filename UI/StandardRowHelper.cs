﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JW.UI
{

/// <summary> A facade to help outside classes add/remove rows to a UI list.</summary>
public class StandardRowHelper : UIContentManager 
{
	/// <summary> The content parent. Warning, you cannot change this after awake. </summary>
	public GameObject UIRowPrefab;

	public GameObject CreateRow()
	{
		return CreateItem(UIRowPrefab);
	}

	public T CreateRow<T>()
	{
		return CreateItem<T>(UIRowPrefab);
	}

}
}