﻿using UnityEngine;
using System;
using System.Collections;

namespace JW.UI
{

public class SelectableUIItem : MonoBehaviour
{
	public event Action<SelectableUIItem> OnSelected = SelectableRow => {};
	public event Action<SelectableUIItem> OnDeselected = SelectableRow => {};

	#region Post Events
	//These callbacks are for a execution order bug emerging from how SelectableUIContentManager works.
	//0. First, SelectableUIItem.Selected = false 
	//1. If the manager's OnDeselect listener runs first, it can set SelectableUIItem.Selected = true (e.g. If it needs one selected, and the one being deselected is the only one selected)
	//2. This calls the manager's OnSelect, and all the OnSelect callbacks are done
	//3. Then, the remainder of the OnDeselect callbacks are run. 
	//This means it's possible for OnDeselect code to run after OnSelect, essentially the reverse of what should happen.
	//To get around this, these "post" callbacks were created, to ensure code that changes the Selected value is run last.
	//!!!With this being said, only use them for methods that will modify the selected value.!!!
	/// <summary> 
	/// Used for events that occur after all OnSelected Logic is done.  Only use these for events that will modify the selected value!
	/// WARNING: Due to technical reasons, it's possible for events in this to be mixed with the deselect callback. Use OnSelect to ensure your callback in invoked before OnDeselect's are.  
	/// </summary>
	public event Action<SelectableUIItem> OnPostSelectedUICallback = SelectableRow => {};
	/// <summary> 
	/// Used for events that occur after all OnDeselected Logic is done. Only use these for events that will modify the selected value!
	/// WARNING: Due to technical reasons, it's possible for events in this to be mixed with the onSelect callback. Use OnDeSelect to ensure your callback in invoked before OnSelect's are.   
	/// </summary>
	public event Action<SelectableUIItem> OnPostDeselectedUICallback = SelectableRow => {};
	#endregion

	//You don't need to touch this. Call ToggleSelected() to select this item. Delegates handle this.
	SelectableUIContentManager rowManager = null;

	[NonSerialized] bool selected = false;
	public bool Selected 
	{
		get
		{
			return selected;
		}

		set
		{
			if (value == selected)
				return;

			//If we are here, the value is different
			selected = value;
			if (value)
			{
				OnSelected(this);
				OnPostSelectedUICallback(this);
			} 
			else
			{
				OnDeselected(this);
				OnPostDeselectedUICallback(this);
			}

		}
	}

	protected virtual void Awake()
	{
		
	}

	protected virtual void Start()
	{
		//When we awake, we try to find a selectable row manager if possible.
		if (rowManager == null) //Don't set one if we already have one
		{
			rowManager = ExtensionMethods.GetComponentInAncestors<SelectableUIContentManager>(gameObject);
			if (rowManager != null)
				rowManager.Register(this);
		}
	}

	/// <summary> Sets the content manager. If you create a GO with this attached, The method in Awake() will fail (Unity Bug). Instead call this. </summary>
	public void SetContentManager(SelectableUIContentManager newRowManager)
	{
		if (rowManager != null)
			rowManager.DeRegister(this);

		rowManager = newRowManager;
		rowManager.Register(this);
	}

	protected virtual void OnDestroy()
	{
		if (rowManager != null)
			rowManager.DeRegister (this);
	}

	/// <summary> Called when this row is pressed. (Should be called from a button) </summary>
	public void ToggleSelected () 
	{
		//Toggle our selected
		Selected = !Selected;
	}
}
}
