﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using JW.Tween;

namespace JW.UI
{
public class SelectableColor : MonoBehaviour 
{ 
	public Image Image;
	public Color selectedColor = Color.gray;
	Color originalColor;
	public float TransitionDuration = 0.2f;

	public bool WithFlash = true;
	public Color FlashColor = Color.white;
	public float FlashDuration = 0.1f;

	SelectableUIItem selectableEventsClass;

	ITween activeTween;

	void Awake()
	{
		selectableEventsClass = GetComponent<SelectableUIItem> ();
		selectableEventsClass.OnSelected += row => onSelected();
		selectableEventsClass.OnDeselected += row => onDeselected();

		originalColor = Image.color;
	}

	void onSelected()
	{
		transitionColorTo(selectedColor, WithFlash);
	}

	void onDeselected()
	{
		transitionColorTo(originalColor);
	}

	#region Tween helpers
	void transitionColorTo(Color color, bool withFlash = false)
	{
		stopOldTween();

		Color startColor = (withFlash) ? FlashColor : Image.color;
		activeTween = SJTween.Value(startColor,color,TransitionDuration,Color.Lerp,col => Image.color = col);

		if (withFlash)
			activeTween = activeTween.Prepend(SJTween.Value(Image.color,FlashColor,FlashDuration,Color.Lerp,col => Image.color = col));
	}

	void stopOldTween()
	{
		if (activeTween != null && activeTween.State != TweenState.Stopped)
			activeTween.Stop();
	}

	void OnDestroy()
	{
		stopOldTween();
	}
	#endregion
}
}