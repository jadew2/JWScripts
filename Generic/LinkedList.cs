﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Generic
{
	public class LinkedListNode<T>
	{
		public T Value = default(T);
		public LinkedListNode<T> Next = null;
		
		public LinkedListNode(){}

		public LinkedListNode(T value)
		{
			this.Value = value;
		}
	}
	
	/// <summary>
	/// A custom linked list implementation because .net doesn't let you set Next. What Garbage!
	/// </summary>
	public class LinkedList<T>
	{
		public LinkedListNode<T> First;
		public LinkedListNode<T> Last;

		public LinkedList(){}

		public LinkedList(IEnumerable<T> values)
		{
			foreach (var value in values)
			{
				Add(value);
			}
		}

		public void Add(T value)
		{
			LinkedListNode<T> newNode = new LinkedListNode<T>(value);

			if (First == null)
			{
				First = newNode;
				Last = newNode;
			}
			else
			{
				Last.Next = newNode;
				Last = newNode;
			}
		}

		/* Too tired... Will make mistakes.
		public bool Remove(T value)
		{
			LinkedListNode<T> curr = First;
			while (curr != null)
			{
				if (curr.Value.Equals(value))
				{
					//Remove
					if (curr == First) //ishead
					{
						First = curr.Next;
						
						if (curr == Last)
							Last = First;
						return true;
					}
					else
					{
						
					}
	
				}
				
				curr = curr.Next;
			}

			return false;
		}
		*/
	}

}


