using UnityEngine;
using System.Collections.Generic;

namespace JW.Modules
{

/// <summary> A class for enabling/disabling monobehaviour "modules" attached to a gameobject only if nessesary. Important methods are OnlyUse, EnableAll and DisableAll. </summary>
/// <remarks> By: Jade White, 2014. For Project TP. </remarks>
public class ModuleEnableMonobehaviour<T> where T : MonoBehaviour
{
	readonly LinkedList<T> behaviourList = new LinkedList<T>();

	public ModuleEnableMonobehaviour(GameObject goWithModules)
	{
		RefreshModuleList(goWithModules.GetComponents<T>());
	}

	public ModuleEnableMonobehaviour(T[] modules)
	{
		RefreshModuleList(modules);
	}

	/// <summary> Replaces the internal Module list with the passed one. </summary>
	public void RefreshModuleList(T[] newModules)
	{
		behaviourList.Clear();
		for (int i = 0; i < newModules.Length; i++)
			behaviourList.AddFirst(newModules[i]);
	}

	/// <summary> Disables all other behaviours and enables the passed behaviours, only sets values were nessesary. </summary>
	/// <remarks>
	/// This is ineffecient, vs. e.g. "disabling all then enabling the specified ones" 
	/// but this nessesary for systems like a character controller. Where calling OnDisable then OnEnable instantly causes terrible problems.
	/// </remarks>
	public void OnlyUse(params T[] behaviours)
	{
		for (int i = 0; i < behaviours.Length; i++)
			behaviourList.Remove(behaviours[i]);
		
		//Must Disable all needed things before enabling anything else
		foreach (T behaviour in behaviourList)
			behaviour.enabled = false;
		
		for (int i = 0; i < behaviours.Length; i++)
			behaviours[i].enabled = true;

		for (int i = 0; i < behaviours.Length; i++)
			behaviourList.AddFirst(behaviours[i]);
	}

	/// <summary> Enables all tracked monobehaviours. </summary>
	public void EnableAll()
	{
		foreach (T element in behaviourList)
			element.enabled = true;
	}

	/// <summary> Disables all track monobehaviours. </summary>
	public void DisableAll()
	{
		foreach (T element in behaviourList)
			element.enabled = false;
	}
}
}

