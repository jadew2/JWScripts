using UnityEngine;
using System.Collections.Generic;

namespace JW.Modules
{
/// <summary> Interface for Modules to implement for JW's ModuleEnable. See ModuleEnable for a rundown of how it works. </summary>
public interface IModuleEnable
{
	bool ModEnabled {get; set;}
}
}

