using UnityEngine;
using System.Collections.Generic;

namespace JW.Modules
{
/// <summary>
/// A utility for enabling/disabling modules attached to something. 
/// !!!For Enabling/Disabling Monobehaviours.enabled use JWModuleEnableMonobehaviour!!!
/// To use this class, the modules must extend the interface IJWModuleEnable
/// </summary>
/// <remarks> By: Jade White, 2014. For Project TP. </remarks>
public class ModuleEnable<T> where T : IModuleEnable
{
	readonly LinkedList<T> behaviourList = new LinkedList<T>();

	public ModuleEnable(params T[] modules)
	{
		RefreshModuleList(modules);
	}

	/// <summary> Replaces the internal Module list with the passed one. </summary>
	virtual public void RefreshModuleList(params T[] modules)
	{
		behaviourList.Clear();
		for (int i = 0; i < modules.Length; i++)
			behaviourList.AddFirst(modules[i]);
	}

	/// <summary> Disables all other modules and enables the passed modules, only sets values were nessesary. </summary>
	/// <remarks>
	/// This is ineffecient, vs. e.g. "disabling all then enabling the specified ones" 
	/// but this nessesary for systems like a character controller. Where calling OnDisable then OnEnable instantly causes terrible problems.
	/// </remarks>
	virtual public void OnlyUse(params T[] behaviours)
	{
		for (int i = 0; i < behaviours.Length; i++)
			behaviourList.Remove(behaviours[i]);

		//Must Disable all things before enabling anything else
		foreach (T element in behaviourList)
			element.ModEnabled = false;

		for (int i = 0; i < behaviours.Length; i++)
			behaviours[i].ModEnabled = true;


		for (int i = 0; i < behaviours.Length; i++)
			behaviourList.AddFirst(behaviours[i]);
	}

	/// <summary> Enables all tracked monobehaviours. </summary>
	virtual public void EnableAll()
	{
		foreach (T element in behaviourList)
			element.ModEnabled = true;
	}

	/// <summary> Disables all track monobehaviours. </summary>
	virtual public void DisableAll()
	{
		foreach (T element in behaviourList)
			element.ModEnabled = false;
	}
}
}
