﻿using UnityEngine;
using System.Collections;
using System;

/// <summary> My own static math class. Use this to add methods that should be in Mathf.  </summary>
public static class MathJ 
{
	/// <summary>
	/// A Mathf.Repeat method for integers. The int returned will be in the range from 0 --> length -1. Also handles negative numbers.
	/// </summary>
	/// <example>
	/// t = 5, length = 4 returns 1
	/// t = 4, length = 4 returns 0
	/// t = 3, length = 4 returns 3
	/// t = 2, length = 4 returns 2
	/// t = 1, length = 4 returns 1
	/// t = 0, length = 4 returns 0
	/// t = -1, length = 4 returns 3 (+4)
	/// t = -2, length = 4 returns 2
	/// t = -3, length = 4 returns 1
	/// t = -4, length = 4 returns 0
	/// t = -5, length = 4 returns 3 (+8)
	/// </example>
	/// <param name="t"> The number. </param>
	/// <param name="lengthExclusive">The returned number will never be larger than this. </param>
	/// <returns></returns>
	/// <exception cref="ArgumentException"> length cannot be less than or equal to 0.</exception>
	public static int Repeat(int t, int lengthExclusive)
	{
		if (lengthExclusive <= 0)
			throw new ArgumentException("MathJ.Repeat: Passed length " + lengthExclusive + ", cannot be <= 0");

		//If t is negative, use this equation to make it positive, so it loops as expected.
		if (t < 0)
			t += (t / lengthExclusive + 1) * lengthExclusive;
		
		//Huh? Why can't we cancel out /length * length? Because /length actually floors the value.
		return t - (t / lengthExclusive * lengthExclusive);
	}

	/// <summary>
	/// Returns the number of times a integer t would be repeated in MathJ.Repeat. Handles negative numbers as well.
	/// </summary>
	/// <example>
	/// t = 5, length = 4 returns 1
	/// t = 4, length = 4 returns 1
	/// t = 3, length = 4 returns 0
	/// t = 2, length = 4 returns 0
	/// t = 1, length = 4 returns 0
	/// t = 0, length = 4 returns 0
	/// t = -1, length = 4 returns -1
	/// t = -2, length = 4 returns -1
	/// t = -3, length = 4 returns -1
	/// t = -4, length = 4 returns -1
	/// t = -5, length = 4 returns -2 
	/// t = -6, length = 4 returns -2 
	/// t = -7, length = 4 returns -2 
	/// t = -8, length = 4 returns -2 
	/// t = -9, length = 4 returns -3 
	/// </example>
	/// <param name="t"> The number. </param>
	/// <param name="lengthExclusive"> The number to loop over. Like Mathf.Repeat, it is exclusive. </param>
	public static int RepeatCount(int t, int lengthExclusive)
	{
		if (t >= 0)
			return t / lengthExclusive;
		//Otherwise, the number is negative
		return -(Math.Abs((t + 1) / lengthExclusive) + 1);
	} 
}
