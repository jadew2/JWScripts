﻿using UnityEngine;
using System.Collections;

namespace JW.Utils
{
///<summary>Contains various transform operations that may come in use.</summary>
public static class TransformUtils 
{

	/* getParentsAboveCount
	 * Returns how many parents above it a transform has.
	 * 		If trans has no parents, 0 is returned.
	 * 		If trans has a parent "x", and "x" has no parent, 1 is returned
	 *	 	If trans has a parent "x", and "x" has a parent "y", and "y" has no parent 2 is returned.
	 *		etc.
	 */

	/// <summary>  Returns true if A is behind B by using their local +z. </summary>
	public static bool IsABehindB(Transform a, Transform b)
	{
		return (Vector3.Dot(a.forward,(b.position - a.position).normalized) > 0f);
	}
	
	/// <summary>  Returns true if a world space position is behind the transform by using it's local +z. </summary>
	public static bool IsPositionBehindTransform(Vector3 worldPosition, Transform trans)
	{
		return (Vector3.Dot(trans.forward,(worldPosition - trans.position).normalized) <= 0f);
	}

	/// <summary> Takes a point inside/outside the rect transform, and places it on the border. Useful for special layouts. </summary>
	public static Vector2 ProjectPointOntoRectTransform(Vector2 localPostion, RectTransform trans)
	{
		localPostion = ClampPointToRectTransform(localPostion, trans); //Unsure why I need this, but I do.
		//I'm too lazy to search up how actually project a line onto a rectangle, (and chances are, it'll be less efficient) so I'll overshoot it, and then clamp the position.
		
		Vector2 direction = localPostion.normalized;
		Vector2 vector = direction * Mathf.Max(trans.sizeDelta.x, trans.sizeDelta.y);
		return ClampPointToRectTransform(vector, trans);
	}
	
	/// <summary> Takes a point outside the rect transform and places it on the border, otherwise leaves it alone. </summary>
	public static Vector2 ClampPointToRectTransform(Vector2 localPosition, RectTransform trans, float margin = 0.0f)
	{
		//* -0.5f because sizeDelta is the total pixels, so the result is the bottom left corner.
		Vector2 min = (trans.sizeDelta * -0.5f) + (Vector2.one * margin);
		if (localPosition.x < min.x)
			localPosition.x = min.x;
		if (localPosition.y < min.y)
			localPosition.y = min.y;

		//* 0.5f puts the point at the top right corner of the screen
		Vector2 max = (trans.sizeDelta * 0.5f) - (Vector2.one * margin);
		if (localPosition.x > max.x)
			localPosition.x = max.x;
		if (localPosition.y > max.y)
			localPosition.y = max.y;

		return localPosition;
	}	
}
}
