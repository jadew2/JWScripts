﻿using System;
using System.Collections;
using UnityEngine;

using System.Net;

namespace JW.Utils
{
    public class NetworkingUtils
    {
        /// <summary>
        /// Uses a web service to get the external (public) IP address of a computer
        /// </summary>
        /// <returns> Callback returns the IP address in string form if succsessful.
        /// Null otherwise.</returns>
        public static void GetExternalIP(Action<string> callback)
        {
            GameObject go = new GameObject("ExternalIPFetcher");
            ExternalIPFetcher fetcherScript = go.AddComponent<ExternalIPFetcher>();
            fetcherScript.Run(callback);
        }

        // /// <summary>
        // /// Returns the local IP of the application.
        // /// </summary>
        // public static string GetInternalIP()
        // {
        // 	return Network.player.ipAddress;
        // }
    }
}
