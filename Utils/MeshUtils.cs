﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JW.Utils
{
///<summary> 
/// Utilities for Copying/Combining meshes.
/// 
/// Also supports submeshes (materials)
/// </summary>
public static class MeshUtils
{
	
	public static void CopyTo(this Mesh from, Mesh to)
	{Copy(from,to,true);}

	/*public static void Copy(Mesh from, ref Mesh to, bool clear)
	{
		if (clear)
			to.Clear();

		to = Object.Instantiate<Mesh>(from);
	}*/

	public static void Copy(Mesh from, Mesh to, bool clear)
	{
		if (clear)
			to.Clear();
		to.vertices  = from.vertices;

		//Set Triangles and Preserve Materials
		to.subMeshCount = from.subMeshCount;
		for (int i = 0; i < from.subMeshCount; i++)
			to.SetTriangles(from.GetTriangles(i),i);

		to.uv 		 = from.uv;
		
		to.bounds  	= from.bounds;
		to.normals 	= from.normals;
		
		to.colors 	= from.colors;
	}

	/* MeshCombiner
	 * 
	 * Utility to help combine meshes. Use this over MeshCombine.
	 * To use:
	 * 1. Call AddMeshData to add your mesh. appendSubMeshes makes your added mesh materials seperate.
	 * 2. Call CombineTo(destmesh).
	 * 3. Clear
	 */
	/*public class MeshCombiner
	{
		List<CombineInstance> meshCIs = new List<CombineInstance>();

		public void AddMeshData(Mesh meshData)
		{
			for (int i = 0; i < meshData.subMeshCount; i++) //Add meshes to be combined
			{
				CombineInstance ci = new CombineInstance();
				ci.mesh = meshData;
				ci.subMeshIndex = i;
				meshCIs.Add(ci);
			}
		}

		public void CombineTo(ref Mesh destMesh, bool mergeSubMeshes = false)
		{
			destMesh.CombineMeshes(meshCIs.ToArray(),mergeSubMeshes,false);
		}

		public void Clear()
		{
			meshCIs.Clear();
		}
	}*/


	public class MeshCombiner
	{
		List<Vector3> vertices 	= new List<Vector3>();

		int subMeshCount = 0; //Must start at 0.
		List<List<int>> subMeshList = new List<List<int>>();

		List<Vector2> uv 		= new List<Vector2>();
		List<Color> colors 		= new List<Color>();

		public void AddMeshData(Mesh meshData, bool appendSubMeshes = false)
		{
			int triangleStartIndex = vertices.Count;
			vertices.AddRange(meshData.vertices);
			
			//Add triangles (Note! because they have integers that point at vertices, we need to increment them)
			//Also not we are using submeshes to preserve materials
			if (appendSubMeshes)
			{//Appending means we are adding new submesh lists (and therefore new submeshes) to this MeshCombiner instance.
				for (int k = 0; k < meshData.subMeshCount; k++) //Appending
					subMeshList.Add(new List<int>());

				for (int i = 0; i < meshData.subMeshCount; i++) 
				{
					int[] elementTriangles = meshData.GetTriangles(i);
					for (int j = 0; j < elementTriangles.Length; j++)
						elementTriangles[j] = elementTriangles[j] + triangleStartIndex;
					subMeshList[(subMeshCount - 1) + i].AddRange(elementTriangles); //Appending
				}
				subMeshCount += meshData.subMeshCount;
			}
			else //Use existing submesh lists. Anything we add will share the same materials as past meshadds.
			{
				for (int i = 0; i < meshData.subMeshCount; i++)
				{
					for (int k = 0; k < meshData.subMeshCount - subMeshCount; k++)
						subMeshList.Add(new List<int>());

					int[] elementTriangles = meshData.GetTriangles(i);
					for (int j = 0; j < elementTriangles.Length; j++)
						elementTriangles[j] = elementTriangles[j] + triangleStartIndex;
					subMeshList[i].AddRange(elementTriangles);
				}

				if (meshData.subMeshCount > subMeshCount)
					subMeshCount = meshData.subMeshCount;
			}
			//Done adding Triangles

			uv.AddRange(meshData.uv);
			colors.AddRange(meshData.colors);
		}

		public void CombineTo(Mesh destMesh)
		{
			//Array Counterparts
			Vector3[] verticesArr 	= new Vector3[0];
			//int[] trianglesArr		= new int[0]; //2d for submeshes. x is submesh, y is the triangles array.
			Vector2[] uvArr			= new Vector2[0];
			Color[] colorsArr		= new Color[0];

			//Optimization: Don't create new arrays if we don't have to.
			CopyListToArray(vertices,ref verticesArr);
			//CopyListToArray(triangles,ref trianglesArr);
			CopyListToArray(uv,ref uvArr);
			CopyListToArray(colors,ref colorsArr);

			//Set the arrays.
			destMesh.Clear();

			destMesh.vertices = verticesArr;

			destMesh.subMeshCount = subMeshCount;
			for (int i = 0; i < subMeshCount; i++)
				destMesh.SetTriangles(subMeshList[i].ToArray(),i);

			destMesh.uv = uvArr;
			destMesh.colors = colorsArr;

			destMesh.RecalculateNormals();
			destMesh.RecalculateBounds();
		}

		void CopyListToArray<T>(List<T> list, ref T[] array)
		{
			if (array.Length != list.Count)
				array = list.ToArray();
			else
			{
				for (int i = 0; i < array.Length; i++)
					array[i] = list[i];
			}
		}

		public void Clear()
		{
			vertices.Clear();

			subMeshCount = 0;
			foreach (var triangleList in subMeshList) 
				triangleList.Clear();
			subMeshList.Clear();

			uv.Clear();
			colors.Clear();
		}

	}

}
}