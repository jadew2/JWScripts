﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace JW.Utils
{
    public class ExternalIPFetcher : MonoBehaviour
    {
        Action<string> onIPReceived = null;

        void Awake()
        {
            updateStatus("Created");
        }

        public void Run(Action<string> callback)
        {
            this.onIPReceived = callback;

            StartCoroutine(FetchIPCoroute());
        }

        IEnumerator FetchIPCoroute()
        {
            updateStatus("Fetching...");

            UnityWebRequest webRequest = UnityWebRequest.Get("http://checkip.dyndns.org");
            if (webRequest == null)
                yield break;
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                updateStatus("Failed");
                Debug.LogError(webRequest.error);
                onIPReceived(null);
                Destroy(this.gameObject);
                yield break;
            }

            string myExtIP = webRequest.downloadHandler.text;
            myExtIP = myExtIP.Substring(myExtIP.IndexOf(":") + 1);
            myExtIP = myExtIP.Substring(0, myExtIP.IndexOf("<"));

            Debug.Log(typeof(ExternalIPFetcher).Name + ": Received IP: " + myExtIP);
            updateStatus("Success: " + myExtIP);


            onIPReceived(myExtIP);

            Destroy(this.gameObject);
        }

        void updateStatus(string status)
        {
            gameObject.name = typeof(ExternalIPFetcher).Name + ": " + status;
        }
    }
}
