
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Linq;
using Newtonsoft.Json;

namespace JW.Utils.IO
{
    /// <summary>
    /// Class for serializing objects to/from Json Strings.
    /// </summary>
    public class ClassStringSerializerJson<T> : IStringSerializer<T>
    {
        public ClassStringSerializerJson()
        {

        }

        /// <summary> Saves the object to the file. Overwrites old files. Returns true if it was successful. </summary>
        public bool Serialize(T theObject, out string result)
        {
            result = null;
            try
            {
                result = JsonConvert.SerializeObject(theObject, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto //When type being serialized doesn't match the base type include the type.
                });
            }
            catch (Exception e)
            {
                Debug.LogWarning(typeof(ClassStringSerializerJson<T>).Name + ": couldn't serialize " + typeof(T) + ": " + e);
                return false;
            }

            return true;
        }

        /// <summary> Loads the object. Returns true if it was successful. </summary>
        public bool DeSerialize(string objectString, out T obj)
        {
            obj = default(T);
            try
            {
                obj = JsonConvert.DeserializeObject<T>(objectString, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto //This is also required for the deserialize. Otherwise it doesn't use the included type.
                });
            }
            catch (Exception e)
            {
                Debug.LogWarning(typeof(ClassStringSerializerJson<T>).Name + ": couldn't deserialize " + typeof(T) + ": " + e);
                return false;
            }

            return true;
        }
    }
}
