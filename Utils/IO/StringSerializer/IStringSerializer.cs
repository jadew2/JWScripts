namespace JW.Utils.IO
{
    /// <summary>
    /// General interface for string serializers, so I can have both a XML and a Json Serializer.
    /// </summary>
    public interface IStringSerializer<T>
    {
        bool Serialize(T theObject, out string result);
        bool DeSerialize(string objectString, out T obj);
    }
}