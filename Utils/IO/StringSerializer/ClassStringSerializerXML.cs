﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Linq;

namespace JW.Utils.IO
{
    /// <summary>
    /// Class for serializing objects to/from XML Strings.
    /// </summary>
    public class ClassStringSerializerXML<T> : IStringSerializer<T>
    {
        readonly XmlSerializer xmlSerializer = null;

        public ClassStringSerializerXML()
        {
            xmlSerializer = new XmlSerializer(typeof(T));
        }

        /// <summary>
        /// Overload that allows additional types to be sepecifed. E.g. if you want to allow all types that extend T.
        /// </summary>
        public ClassStringSerializerXML(Type[] additionalSerializableTypes)
        {
            xmlSerializer = new XmlSerializer(typeof(T), additionalSerializableTypes);
        }

        /// <summary> Saves the object to the file. Overwrites old files. Returns true if it was successful. </summary>
        public bool Serialize(T theObject, out string result)
        {
            result = null;
            try
            {
                using (StringWriter textWriter = new StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, theObject);
                    result = textWriter.ToString();
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning(typeof(ClassStringSerializerXML<T>).Name + ": couldn't serialize " + typeof(T) + ": " + e);
                return false;
            }

            return true;
        }

        /// <summary> Loads the object. Returns true if it was successful. </summary>
        public bool DeSerialize(string objectString, out T obj)
        {
            obj = default(T);
            try
            {
                using (StringReader text = new StringReader(objectString))
                {
                    obj = (T)xmlSerializer.Deserialize(text);
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning(typeof(ClassStringSerializerXML<T>).Name + ": couldn't deserialize " + typeof(T) + ": " + e);
                return false;
            }

            return true;
        }
    }
}
