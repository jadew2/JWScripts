﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

using JW.Utils;

namespace JW.Utils.IO
{
    /// <summary>
    /// Simple class for saving/loading generic classes to a file. 
    /// Uses binary serialization. If it is a windows store app, uses XML serialization.
    /// Unless otherwise specified, uses string Encryption.
    /// </summary>
    /// <remarks>
    /// Why XML? Why not BinaryFormatter or Json eh?
    /// Windows Store Apps don't support BinaryFormatter, and other platforms don't support Json easily.
    /// So really, I'm left with XML serialization, encrypting the string to get similar security to BinaryFormatter.
    /// </remarks>
    public class ClassSaver<T>
    {
        //These should only be set once. Otherwise, you're going to lose the location of your file.
        //If you need to set it, create a new ClassSaver.
        public readonly string FilePath;
        public readonly string FileName;
        string combinedPath { get { return Path.Combine(FilePath, FileName); } }

        //Optional properties: 
        //Since I need so many optional arguments, figure it's better to expose them as fields. 
        public bool UseEncryption = true;
        public enum ErrorLogging
        {
            All,
            SavingOnly, //Only log save errors, the rest are hidden.
            None,
        }
        /// <summary>
        /// Whether to print debug logs for issues when Saving/Loading.
        /// Useful for when you want handle the result of the save/load operations yourself, and don't care about errors.
        /// </summary>
        public ErrorLogging Logging = ErrorLogging.All;

        public enum SerializationType
        {
            Json,   //Uses Newtonsoft Json .NET https://www.newtonsoft.com/json
            XML     //Legacy, works with everything
        }
        public SerializationType Serialization = SerializationType.XML;

        /// <summary>
        /// For XMLSerializer: If you want to add additional types that can be serialized, initialize it as a property here.
        /// </summary>
        public Type[] AdditionalXMLSerializableTypes = new Type[0];

        public ClassSaver(string filePath) : this(Path.GetDirectoryName(filePath), Path.GetFileName(filePath)) { }
        public ClassSaver(string path, string fileName)
        {
            this.FilePath = path;
            this.FileName = fileName;
        }

        /// <summary> Saves the object to the file. Overwrites old files. Creates the file and path if it does not exist. Returns true if it was successful. </summary>
        public virtual bool Save(T theObject)
        {
            try
            {
                string objectString;

                IStringSerializer<T> serializer = getSerializer();
                if (!serializer.Serialize(theObject, out objectString))
                    throw new Exception("Serialization Failure");

                if (UseEncryption)
                    objectString = Security.EncryptString(objectString);

                FileInfo file = new FileInfo(combinedPath);
                file.Directory.Create(); //Creates the full file path if it does not exist. 

                File.WriteAllText(combinedPath, objectString);
            }
            catch (Exception e)
            {
                if (Logging == ErrorLogging.All || Logging == ErrorLogging.SavingOnly)
                {
                    Debug.LogWarning(typeof(ClassSaver<T>).Name + ": couldn't save " + typeof(T) + " to " + combinedPath + ": " + e);
                }
                return false;
            }

            return true;
        }

        /// <summary> 
        /// Loads the object. Returns true if it was successful. 
        /// Optionally allows you to silence load failures, if you only care about the result.
		/// </summary>
        public virtual bool Load(out T theObject, bool debugWarnings = true)
        {
            theObject = default(T);

            if (!File.Exists(combinedPath))
            {
                if (Logging == ErrorLogging.All)
                {
                    Debug.LogWarning(typeof(ClassSaver<T>).Name + ": couldn't load " + typeof(T) + " from " + combinedPath + ": File does not exist.");
                }
                return false;
            }

            try
            {
                string objectString = File.ReadAllText(combinedPath);

                if (UseEncryption)
                    objectString = Security.DecryptString(objectString);

                IStringSerializer<T> serializer = getSerializer();
                if (!serializer.DeSerialize(objectString, out theObject))
                    throw new Exception("Deserialization Failure");
            }
            catch (Exception e)
            {
                if (Logging == ErrorLogging.All)
                {
                    Debug.LogWarning(typeof(ClassSaver<T>).Name + ": couldn't load " + typeof(T) + " from " + combinedPath + ": " + e);
                }
                return false;
            }

            return true;
        }

        IStringSerializer<T> getSerializer()
        {
            if (Serialization == SerializationType.XML)
            {
                return new ClassStringSerializerXML<T>(AdditionalXMLSerializableTypes);
            }
            else if (Serialization == SerializationType.Json)
            {
                return new ClassStringSerializerJson<T>();
            }
            else
            {
                throw new InvalidOperationException(typeof(ClassSaver<T>).Name + ": Unknown serialization enum type " + Serialization);
            }
        }
    }
}
