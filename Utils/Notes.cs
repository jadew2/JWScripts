﻿using UnityEngine;
using System.Collections;

namespace JW.Utils
{
/// <summary> Class for showing notes in the Unity Editor. Destroys itself in awake for runtime apps.  </summary>
public class Notes : MonoBehaviour 
{
	#if UNITY_EDITOR

	#pragma warning disable 414
	[TextArea(3,10)]
	[SerializeField] string Text = "No Notes";
	#pragma warning restore 414

	#endif

	void Awake()
	{
		if (!Application.isEditor)
			Destroy(this);
	}
}
}