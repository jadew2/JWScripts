﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JW.Utils
{
public class ListHelper
{
	public void AddBelowAt<T>(int index, IList<T> list, T newObj)
	{
		if (index >= list.Count)
			list.Add(newObj);
		else
			list.Insert(index + 1,newObj);
	}

	public void AddAboveAt<T>(int index, IList<T> list, T newObj)
	{
		if (index <= 1)
			list.Insert(0,newObj);
		else
			list.Insert(index - 1,newObj);
	}

	///<summary>Move a list element "up" 1 (Towards start of list)</summary>
	public void MoveUpAt<T>(int index, IList<T> list)
	{
		T temp = list[index];
		list.RemoveAt(index);
		if (index <= 0) //If at 0, add 1 to make it insert at 0
			index = 1;
		list.Insert(index - 1,temp);
	}	

	///<summary>Move a list element "down" 1 (Towards end of list)</summary>
	public void MoveDownAt<T>(int index, IList<T> list)
	{
		T temp = list[index];
		list.RemoveAt(index);
		if (index >= list.Count) //If beyond bounds of list, add
			index = list.Count - 1;
		list.Insert(index + 1,temp); 
	}

	///<summary>Mercessily resizes a list. Discards/Adds any elements from/to the end as needed.</summary>
	///<param name="createElementFunc"> The callback called when we want to create a new item. Defaults to null/0. </param>
	public void ResizeTo<T>(int newCount, IList<T> list, Func<T> createElementFunc = null)
	{
		if (newCount < 0)
			throw new System.ArgumentOutOfRangeException("newCount","Count cannot be negative.");
		if (createElementFunc == null)
			createElementFunc = () => default(T);
		
		if (newCount == list.Count)
			return;

		while (newCount > list.Count)
			list.Add(createElementFunc());
		while (newCount < list.Count)
			list.RemoveAt(list.Count-1);
	}

}
}
