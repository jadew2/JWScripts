﻿using UnityEngine;
using System.Collections;

namespace JW.Utils
{
/// <summary> Contains various math equations that may come in help </summary>
public static class VectorUtils 
{

	///<summary> Returns the closest point on a line. x1 and x2 are points on the infinite line. </summary>
	///<remarks> Thanks to: Collision Detection PPS, Essential math for games. </remarks>
	public static Vector3 ClosestPointOnLine(Vector3 to, Vector3 x1, Vector3 x2)
	{
		Vector3 normalizedDirection = Vector3.Normalize(x2 - x1);
		return (normalizedDirection * Vector3.Dot((to - x1),normalizedDirection)); //Line Point
	}

	///<summary> Returns the closest point clamped by start and end. If you need a infinite line, use closestPointOnLine(). </summary>
	///<remarks> Thanks to: Grumdog, see http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment </remarks>
	public static Vector3 ClosestPointOnLineSegment(Vector3 to, Vector3 lineStart, Vector3 lineEnd)
	{
		Vector3 line = (lineEnd - lineStart);
		float lengthSquared = line.sqrMagnitude;
		if (lengthSquared == 0f) //If the line points are equal, return one of them.
			return lineStart;
		
		float t = Vector3.Dot(to - lineStart, line)/lengthSquared;
		if (t < 0f)
			return lineStart;//We are beyond the 'lineStart' end of the segment.
		else if (t > 1f)
			return lineEnd;	 //We are beyond the 'lineEnd' end of the segment.
		else			
			return lineStart + t * line; //We are between the two points. Return the point on the line.
	}

	///<summary> Same as above, just returning a value t 0f-->1f </summary>
	public static float ClosestTOnLineSegment (Vector3 to, Vector3 lineStart, Vector3 lineEnd)
	{
		Vector3 line = (lineEnd - lineStart);
		float lengthSquared = line.sqrMagnitude;
		if (lengthSquared == 0f) //If the line points are equal, return one of them.
			return 0f;

		float t = Vector3.Dot(to - lineStart, line)/lengthSquared;
		if (t < 0f)
			return 0f;//We are beyond the 'lineStart' end of the segment.
		else if (t > 1f)
			return 1f;	 //We are beyond the 'lineEnd' end of the segment.
		else			
			return t; //We are between the two points. Return the point on the line.
	}

}
}
