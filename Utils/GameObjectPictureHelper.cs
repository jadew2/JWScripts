﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using JW.CharacterCustomization;

namespace JW.Utils
{
/// <summary>
/// Class which helps take photographs of gameobjects.
///  
/// Has methods which handle setting up the camera, positioning, and rendering of the object. 
/// If you want to get the setup and not the render, call GeneratePrefabToTextureObjects().
/// </summary>
public class GameObjectPictureHelper : MonoBehaviour 
{
	/// <summary> Freaking disgusting I know. There isn't a better supported way to set the Gameobject's layer. </summary>
	public string RenderingLayerName = "UIModelRender";

	public Vector3 CameraOffsetDirection = Vector3.forward;
	public float CameraFov = 60f;
	public float CameraDistanceMultiplier = 1.1f;

	/// <summary> Renders the prefab and generates a texture. Destroys all created GO's used to render it. </summary>
	public RenderTexture RenderPrefabToTexture(GameObject prefab, Vector2 textureDimensions)
	{
		Camera renderCamera;
		GameObject objectGO;
		RenderTexture renderTexture;
		GeneratePrefabToTextureObjects(prefab,textureDimensions,out renderCamera,out objectGO, out renderTexture);

		//Render to the renderTexture (This is instantaneous)
		renderCamera.Render();

		//Great, now clean up
		DestroyImmediate(objectGO);
		renderCamera.targetTexture = null; //Unset the render texture - otherwise it writes it black on destruction.
		DestroyImmediate(renderCamera.gameObject);

		return renderTexture;
	}

	/// <summary> Renders the prefab and generates a texture. Unlike it's overload, it does not destroy the object or the camera used to render it. </summary>
	public void GeneratePrefabToTextureObjects(GameObject prefab, Vector2 textureDimensions, out Camera renderCamera, out GameObject prefabObject, out RenderTexture renderTexture)
	{
		//Instantiate the prefab
		prefabObject = Instantiate<GameObject>(prefab);
		Transform objectTrans = prefabObject.transform;
		objectTrans.position = Vector3.zero;
		objectTrans.rotation = Quaternion.identity;

		prefabObject.layer = LayerMask.NameToLayer(RenderingLayerName); //Set the layer of this gameobject

		//Create and position the camera.
		GameObject cameraGO = new GameObject("RendererCamera");
		renderCamera = cameraGO.AddComponent<Camera>();
		renderCamera.clearFlags = CameraClearFlags.Color;
		renderCamera.backgroundColor = new Color(0,0,0,0);
		renderCamera.fieldOfView = CameraFov;
		renderCamera.cullingMask = LayerMask.GetMask(RenderingLayerName);
		//camera.pixelRect = new Rect(Vector3.zero,renderDimensions); (Ignored when .targetTexture is set)

		positionCamera(renderCamera,prefabObject,CameraDistanceMultiplier);

		//Create and render to the rendertexture
		renderTexture = new RenderTexture((int)textureDimensions.x,(int)textureDimensions.y,24);
		renderCamera.targetTexture = renderTexture;

		//I've got a lead with RenderTexture state shinegans
	}

	void singleFrameRender(Camera camera, RenderTexture renderTexture)
	{
		//Render to the texture in one frame
		RenderTexture tempRenderTexture = RenderTexture.active;
		RenderTexture.active = renderTexture;

		if (camera.clearFlags == CameraClearFlags.Skybox)
			GL.ClearWithSkybox(true,camera);
		else if (camera.clearFlags == CameraClearFlags.Color)
			GL.Clear(true,true,camera.backgroundColor);

		camera.Render();

		RenderTexture.active = tempRenderTexture;
	}

	/// <summary> Positions the camera such that it covers object and all of it's children. Uses the camera's FOV. </summary>
	/// <remarks> 
	/// Does so by using "camera frustum sphere encapsulation". Which is a fancy way of saying "Positions the camera to contain a sphere". 
	/// This isn't exact, but it makes the math so much easier. Use CameraDistanceMultiplier to tweak the distance. 
	/// 
	/// This is only good for objects that are small, and not spread far apart. 
	/// At which point you oughta code camera frutum box encapsulation. And encapsulate a box with the same rotation as the camera (Handles edge cases).
	/// </remarks>
	void positionCamera(Camera camera, GameObject objectToRender, float cameraDistanceMultiplier = 1.1f)
	{
		List<Renderer> renderers = GetComponentsOnAndInChildren<Renderer>(objectToRender);
		if (renderers.Count == 0) 
			return; //Nothing we can do! The object is invisibe.

		//First find the total worldspace bounds of the object.
		Bounds worldSpaceBounds = renderers.First().bounds;
		for (int i = 1; i < renderers.Count; i++)
			worldSpaceBounds.Encapsulate(renderers[i].bounds);

		//Pretend like the box is a sphere, it makes the math a ton easier.
		float sphereRadius = worldSpaceBounds.extents.magnitude;

		//Magic! See http://forum.unity3d.com/threads/frame-camera-on-set-of-gameobjects.15844/#post-108961
		float cameraDistance = sphereRadius / Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad * 0.5f);

		//Position the camera in front, looking at the object.
		Vector3 objectCentre = worldSpaceBounds.center;
		float wantedCameraDistance = cameraDistance * cameraDistanceMultiplier; //Custom multiplier for tweaking

		camera.transform.position = (CameraOffsetDirection.normalized * wantedCameraDistance) + objectCentre;
		camera.transform.rotation = Quaternion.LookRotation(objectCentre - camera.transform.position,Vector3.up);
	}

	/// <summary> Returns all the components of the specified type on the GO or in it's children. </summary>
	List<T> GetComponentsOnAndInChildren<T>(GameObject go)
	{
		List<GameObject> allGameObjects = new List<GameObject>();
		allGameObjects.Add(go);
		allGameObjects.AddRange(go.transform.GetAllChildren().Select(trans => trans.gameObject));
		return allGameObjects.Select(gameO => gameO.GetComponent<T>()).Where(component => component != null).ToList();
	}
}
}