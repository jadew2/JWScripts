using System.Collections;

#if NETFX_CORE
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
#else
using System.Security.Cryptography;
#endif

using System.Text;

namespace JW.Utils
{

public interface IJWSecurity
{
	string HashMd5(string str);
	string HashSHA2(string str);
	string EncryptString(string str,string key);
	string DecryptString(string str,string key);
}

/// <summary>
/// Various security methods to help prevent meddling with save files and shenanigans.
/// </summary>
public static class Security
{
#if NETFX_CORE
        /// <summary> 
        /// Encrypts the string into a hash using the Md5 algorithm. 
        /// Note that Md5 has been cracked since the 70's, so your string should be long. (+18 characters) 
        /// </summary>
        public static string HashMd5(string strToEncrypt)
        {
            return hashString(strToEncrypt, HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5));
        }

        /// <summary> 
        /// Encrypts the string into a hash using the SHA2 algorithm. 
        /// Note that SHA2 can still be brute forced, so your string should be long (+18 characters) 
        /// </summary>
        public static string HashSHA2(string strToEncrypt)
        {
            return hashString(strToEncrypt, HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256));
        }

        static string hashString(string str, HashAlgorithmProvider algorithm)
        {
            IBuffer buffer = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8); //Convert to bytes
            IBuffer result = algorithm.HashData(buffer); //Encrypt bytes
            return CryptographicBuffer.EncodeToHexString(result); //Return string
        }

        const string DefaultEncryptionKey = "12345698562845567890123456789012";

        ///<summary>
        /// Encrypts a string using the RijndaelManaged algorithm. http://unitynoobs.blogspot.ca/2012/01/xml-encrypting.html
        /// If no key is provided, uses a default one.
        ///</summary>
        public static string EncryptString(string str, string key = null)
        {
            if (key == null)
                key = DefaultEncryptionKey;

            //First create a key. Roundabout thanks to UWP.
            CryptographicKey resultKey;
            {
                IBuffer keyBuffer = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8); //Convert to bytes
                var keyProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesEcbPkcs7); //AesEcbPkcs7 is RijndaelManaged
                resultKey = keyProvider.CreateSymmetricKey(keyBuffer);
            }
            
            IBuffer stringBuffer = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8); //Convert to bytes

            //Do Encryption
            IBuffer result = CryptographicEngine.Encrypt(resultKey, stringBuffer, null);
            return CryptographicBuffer.EncodeToBase64String(result);
        }

        ///<summary>
        /// Decrypts a string created using Encrypt(). Uses the RijndaelManaged algorithm. http://unitynoobs.blogspot.ca/2012/01/xml-encrypting.html
        /// If no key is provided, uses a default one.
        ///</summary>
        public static string DecryptString(string str, string key = null)
        {
            if (key == null)
                key = DefaultEncryptionKey;

            //First create a key. Roundabout thanks to UWP.
            CryptographicKey resultKey;
            {
                IBuffer keyBuffer = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8); //Convert to bytes
                var keyProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesEcbPkcs7); //AesEcbPkcs7 is RijndaelManaged
                resultKey = keyProvider.CreateSymmetricKey(keyBuffer);
            }

            IBuffer stringBuffer = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8); //Convert to bytes

            //Do Decryption
            IBuffer result = CryptographicEngine.Decrypt(resultKey, stringBuffer, null);
            return CryptographicBuffer.EncodeToBase64String(result);
        }


#else
        /// <summary> 
        /// Encrypts the string into a hash using the Md5 algorithm. 
        /// Note that Md5 has been cracked since the 70's, so your string should be long. (+18 characters) 
        /// </summary>
        public static string HashMd5(string strToEncrypt)
	{
		return hashString(strToEncrypt,MD5.Create());
	}

	/// <summary> 
	/// Encrypts the string into a hash using the SHA2 algorithm. 
	/// Note that SHA2 can still be brute forced, so your string should be long (+18 characters) 
	/// </summary>
	public static string HashSHA2(string strToEncrypt)
	{
		return hashString(strToEncrypt,SHA256.Create());
	}

	static string hashString(string str,HashAlgorithm algorithm)
	{
		System.Text.UnicodeEncoding ue = new System.Text.UnicodeEncoding();
		byte[] bytes = ue.GetBytes(str);

		//Encrypt bytes
		byte[] hashBytes = algorithm.ComputeHash(bytes);

		// Convert the encrypted bytes back to a string (base 16)
		return System.Text.Encoding.Unicode.GetString(hashBytes);
	}

	const string DefaultEncryptionKey = "12345698562845567890123456789012";
	///<summary>
	/// Encrypts a string using the RijndaelManaged algorithm. http://unitynoobs.blogspot.ca/2012/01/xml-encrypting.html
	/// If no key is provided, uses a default one.
	///</summary>
	public static string EncryptString(string str,string key = null)
	{
		if (key == null)
			key = DefaultEncryptionKey;

		byte[] keyArray = UTF8Encoding.UTF8.GetBytes(key);
		// 256-AES key
		byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(str);
		RijndaelManaged rDel = new RijndaelManaged();
		rDel.Key = keyArray;
		rDel.Mode = CipherMode.ECB;
		// http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
		rDel.Padding = PaddingMode.PKCS7;
		// better lang support
		ICryptoTransform cTransform = rDel.CreateEncryptor();
		byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray,0,toEncryptArray.Length);
		return System.Convert.ToBase64String(resultArray,0,resultArray.Length);
	}

	///<summary>
	/// Decrypts a string created using Encrypt(). Uses the RijndaelManaged algorithm. http://unitynoobs.blogspot.ca/2012/01/xml-encrypting.html
	/// If no key is provided, uses a default one.
	///</summary>
	public static string DecryptString(string str,string key = null)
	{
		if (key == null)
			key = DefaultEncryptionKey;
		
		byte[] keyArray = UTF8Encoding.UTF8.GetBytes(key);
		// AES-256 key
		byte[] toEncryptArray = System.Convert.FromBase64String(str);
		RijndaelManaged rDel = new RijndaelManaged();
		rDel.Key = keyArray;
		rDel.Mode = CipherMode.ECB;
		// http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
		rDel.Padding = PaddingMode.PKCS7;
		// better lang support
		ICryptoTransform cTransform = rDel.CreateDecryptor();
		byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray,0,toEncryptArray.Length);
		return UTF8Encoding.UTF8.GetString(resultArray);
	}
	#endif

}
}