﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(TransformHerarchyCopier))]
public class TransformHerarchyCopierEditor : Editor 
{
	private SerializedObject mainObject;
	private TransformHerarchyCopier transformHerarchyCopier;

	public void OnEnable ()
	{
		mainObject = new SerializedObject(target);
		transformHerarchyCopier = (TransformHerarchyCopier)target;
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (GUILayout.Button("Copy Transforms From Other"))
		{
			transformHerarchyCopier.SyncPosition();
		}
	}
}
