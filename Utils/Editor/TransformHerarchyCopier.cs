﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TransformHerarchyCopier : MonoBehaviour
{
	public Transform Other;
	
	public void SyncPosition()
	{
		if (Other == null)
			return;
		
		SyncPositionRecursive(transform,Other);
	}

	void SyncPositionRecursive(Transform parent, Transform otherParent)
	{
		if (parent == null || otherParent == null)
			return;

		if (parent.name.Trim() == otherParent.name.Trim())
		{
			parent.transform.localPosition = otherParent.transform.localPosition;
			parent.transform.localRotation = otherParent.transform.localRotation;
			parent.transform.localScale = otherParent.transform.localScale;
		}

		Transform[] directChildren = parent.GetDirectChildren();
		Transform[] otherDirectChildren = otherParent.GetDirectChildren();

		foreach (var child in directChildren)
		{
			Transform matchingOtherChild = otherDirectChildren.FirstOrDefault(x => x.name.Trim() == child.name.Trim());
			if (matchingOtherChild == null)
				continue;
			
			SyncPositionRecursive(child,matchingOtherChild);
		}

	}
}
