﻿using UnityEngine;
using UnityEditor;

namespace JW.Utils
{
public static class InspectorUtils
{
	/// <summary> 
	/// (Editor Only) This makes it easy to create, name and place unique new ScriptableObject asset files.
	/// Courtesy of http://wiki.unity3d.com/index.php?title=CreateScriptableObjectAsset 
	/// </summary>
	public static void CreateScriptableObjectEditor<T> () where T : ScriptableObject
	{
		T asset = ScriptableObject.CreateInstance<T> ();

		string path = AssetDatabase.GetAssetPath (Selection.activeObject);
		if (path == "") 
		{
			path = "Assets";
		} 
		else if (System.IO.Path.GetExtension (path) != "") 
		{
			path = path.Replace (System.IO.Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");
		}

		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New " + typeof(T) + ".asset");

		AssetDatabase.CreateAsset (asset, assetPathAndName);

		AssetDatabase.SaveAssets ();
		AssetDatabase.Refresh();
		EditorUtility.FocusProjectWindow ();
		Selection.activeObject = asset;
	}

	/// <summary> (For use in OnInspectorGUI()) Shows the script field. Commonly seen in default Monobehaviour scripts.</summary>
	public static void ShowScriptField(SerializedObject obj)
	{
		SerializedProperty prop = obj.FindProperty("m_Script");
		EditorGUILayout.PropertyField(prop, true, new GUILayoutOption[0]);
	}
}
}