﻿using UnityEngine;
using System.Collections;

namespace JW.Utils
{
    ///<summary> Contains various physics operations that may come in help </summary>
    public static class PhysicsUtils
    {

        /// <summary>
        /// <para> Returns the smooth normal of a face if possible.</para>
        /// <para> The normal isn't smoothed if the object is flat shaded, a convex mesh collider, or not a mesh collider.</para>
        /// </summary>
        /// <remarks> If you're getting weird normals, Be sure you're exporting the faces from blender as Tris. Quads can get funky. </remarks>
        public static Vector3 GetSmoothNormal(RaycastHit hitInfo)
        {
            MeshCollider meshCollider = hitInfo.collider as MeshCollider;
            if (meshCollider != null && meshCollider.sharedMesh != null && !meshCollider.convex)
            {
                Mesh mesh = meshCollider.sharedMesh;
                Vector3[] normals = mesh.normals;
                int[] triangles = mesh.triangles;
                int resultTriangleIndex = hitInfo.triangleIndex * 3;
                Vector3 n0 = normals[triangles[resultTriangleIndex]];
                Vector3 n1 = normals[triangles[resultTriangleIndex + 1]];
                Vector3 n2 = normals[triangles[resultTriangleIndex + 2]];

                Vector3 baryCenter = hitInfo.barycentricCoordinate;
                Vector3 interpolatedNormal = (n0 * baryCenter.x + n1 * baryCenter.y + n2 * baryCenter.z).normalized;
                interpolatedNormal = hitInfo.transform.TransformDirection(interpolatedNormal);

                //Debug.DrawRay(hitInfo.point,interpolatedNormal*10f);
                return interpolatedNormal;

            }
            else //We hit a collider that wasn't a meshcollider, get the basic normal
                return hitInfo.normal;
        }

        public static bool LayerIsInLayerMask(int layer, LayerMask layerMask)
        {
            return ((layerMask.value & (1 << layer)) > 0);
        }

        /// <summary> Sets the rigidbody velocity using AddForce. This prevents most tunnelling. </summary>
        public static void SetRigidbodyVelocitySafe(Rigidbody rigidbody, Vector3 velocity)
        {
            Vector3 velocityDiff = velocity - rigidbody.velocity;
            rigidbody.AddForce(velocityDiff, ForceMode.VelocityChange);
        }

    }
}
