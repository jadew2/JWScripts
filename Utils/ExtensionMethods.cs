﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public static class ExtensionMethods
{
    public static bool IsNullOrDestroyed(UnityEngine.Object obj)
    {
        //Object.Equals is overriden by the UnityEngine, and it checks for destruction.
        return (obj == null || obj.Equals(null));
    }

    public static T FindObjectOfTypeIfNull<T>(T obj) where T : UnityEngine.Object
    {
        if (obj == null)
            obj = UnityEngine.Object.FindObjectOfType<T>();
        return obj;
    }

    #region PlaneExtensions
    public static Vector3 RayPoint(this Plane that, Ray ray)
    {
        float d;
        that.Raycast(ray, out d);
        return ray.GetPoint(d);
    }
    #endregion


    #region TransformExtenstions

    /// <summary> Returns all the children in the heriarchy under a transform. </summary>
    public static Transform[] GetAllChildren(this Transform that)
    {
        return that.GetComponentsInChildren<Transform>(true);
    }

    /// <summary> Reutrns the children under a transform, but not their children. </summary>
    public static Transform[] GetDirectChildren(this Transform that)
    {
        int childCount = that.childCount;
        Transform[] children = new Transform[childCount];

        for (int i = 0; i < childCount; i++)
            children[i] = that.GetChild(i);

        return children;
    }


    /// <summary> Returns the topmost parent transform of a transform. </summary>
    public static Transform FindRoot(this Transform that)
    {
        Transform root = that;
        while (root.parent != null)
            root = root.parent;
        return root;
    }
    #endregion

    #region GetComponent Extensions
    public static T GetComponentIfNull<T>(this GameObject that, T component)
    {
        if (component == null)
            return that.GetComponent<T>();
        return default(T);
    }

    public static T[] GetComponentsOnAndInChildren<T>(this Component that) where T : UnityEngine.Object
    {
        return GetComponentsOnAndInChildren<T>(that.gameObject);
    }

    public static T[] GetComponentsOnAndInChildren<T>(this GameObject that) where T : UnityEngine.Object
    {
        List<Transform> allTransforms = new List<Transform>() { that.transform };
        allTransforms.AddRange(that.transform.GetAllChildren());

        List<T> components = new List<T>();
        foreach (var trans in allTransforms)
            components.AddRange(trans.GetComponents<T>());

        return components.ToArray();
    }

    public static T GetComponentInDirectChildren<T>(this Component that) where T : UnityEngine.Object
    {
        return GetComponentInDirectChildren<T>(that.gameObject);
    }

    public static T GetComponentInDirectChildren<T>(this GameObject that) where T : UnityEngine.Object
    {
        Transform[] transforms = that.transform.GetDirectChildren();
        for (int i = 0; i < transforms.Length; i++)
        {
            T result = transforms[i].GetComponent<T>();
            if (result != default(T))
                return result;
        }
        return null;
    }

    /// <summary> Shorthand for "x.GetComponent<T>() ?? x.GetComponentInAncestors<T>()" </summary>
    public static T GetComponentOnOrInAncestors<T>(this Component that) where T : UnityEngine.Object
    {
        T component = that.GetComponent<T>();
        if (!IsNullOrDestroyed(component))
            return component;

        return that.GetComponentInAncestors<T>();
    }

    public static T[] GetComponentsOnAndInAncestors<T>(this Component that) where T : UnityEngine.Object
    {
        List<T> components = new List<T>();
        components.AddRange(that.GetComponents<T>());
        components.AddRange(that.GetComponentsInAncestors<T>());
        return components.ToArray();
    }

    public static T GetComponentInAncestors<T>(this Component that) where T : UnityEngine.Object
    {
        return GetComponentInAncestors<T>(that.gameObject);
    }

    public static T GetComponentInAncestors<T>(this GameObject that) where T : UnityEngine.Object
    {
        Transform curr = that.transform;
        while (curr.parent != null)
        {
            curr = curr.parent;

            T component = curr.GetComponent<T>();
            if (!(component == default(T)))
                return component;
        }
        return default(T);
    }

    public static T[] GetComponentsInAncestors<T>(this Component that) where T : UnityEngine.Object
    {
        return GetComponentsInAncestors<T>(that.gameObject);
    }

    public static T[] GetComponentsInAncestors<T>(this GameObject that) where T : UnityEngine.Object
    {
        List<T> components = new List<T>();
        Transform curr = that.transform;
        while (curr.parent != null)
        {
            curr = curr.parent;

            T component = curr.GetComponent<T>();
            if (!(component == default(T)))
                components.Add(component);
        }
        return components.ToArray();
    }


    public static GameObject FindIncludingInactive(string name)
    {
        GameObject[] allGos = (GameObject[])Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (GameObject go in allGos)
        {
            if (go.name == name)
                return go;
        }
        return null;

    }



    #endregion

    #region UI Extensions

    public static void Deselect(this UnityEngine.UI.Selectable that)
    {
        var eventSystems = UnityEngine.Object.FindObjectsOfType<UnityEngine.EventSystems.EventSystem>();
        foreach (var eventSystem in eventSystems)
        {
            if (eventSystem.currentSelectedGameObject == that.gameObject)
                eventSystem.SetSelectedGameObject(null); //Select the event system, a hacky way of selecting nothing.
        }
    }

    public static bool IsHeadless()
    {
        return SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.Null;
    }


    /// <summary>
    /// Transforms a world position into a anchoredPosition. Takes into account all scaling.
    /// Note you should pass the canvas RectTransform for optimization.
    /// Assumes a top left anchor.
    /// </summary>
    public static Vector2 WorldToCanvas(this Canvas canvas, Vector3 worldPosition, Camera camera = null)
    {
        RectTransform canvasTransform = canvas.GetComponent<RectTransform>();
        return canvas.WorldToCanvas(canvasTransform, worldPosition, camera);
    }

    /// <summary>
    /// Transforms a world position into a anchoredPosition. Takes into account all scaling.
    /// Assumes a top left anchor.
    /// </summary>
    /// <remarks>
    /// Thanks to work done by Vahradrim https://answers.unity.com/comments/1341810/view.html and cmberryau https://answers.unity.com/answers/931269/view.html
    /// </remarks>
    public static Vector2 WorldToCanvas(this Canvas canvas, RectTransform canvasTransform, Vector3 worldPosition, Camera camera = null)
    {
        if (camera == null)
            camera = Camera.main;

        //Thanks to https://answers.unity.com/comments/1341810/view.html
        // Get the position on the canvas
        Vector2 viewportPoint = camera.WorldToViewportPoint(worldPosition);
        Vector2 proportionalPosition = Vector2.Scale(viewportPoint, canvasTransform.sizeDelta);

        // Calculate the Screen Offset for the center.
        Vector2 center = canvasTransform.sizeDelta * 0.5f;
        return proportionalPosition - center;
    }

    #endregion

}