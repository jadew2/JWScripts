﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace JW.Utils
{

/// <summary>
/// Helps print Debug.Log messages into a UI textbox
/// </summary>
public class TextDebugLog : MonoBehaviour 
{
	public enum MessageLengthEnum
	{
		Short,
		LongForErrors,
		Long
	}

	public Text TextPanel;
	public int DisplayedMessages = 6;
	public readonly List<string> Messages = new List<string> ();
	public MessageLengthEnum MessageLength = MessageLengthEnum.Long;

	void Awake()
	{
	}

	void OnEnable()
	{
		Application.logMessageReceivedThreaded += onLogMessage;
	}

	void OnDisable()
	{
		Application.logMessageReceivedThreaded -= onLogMessage;
	}

	void onLogMessage(string message, string stackTrace, LogType logType)
	{
		bool longMessage = false;
		if (MessageLength == MessageLengthEnum.Short)
			longMessage = false;
		else if (MessageLength == MessageLengthEnum.Long)
			longMessage = true;
		else if (MessageLength == MessageLengthEnum.LongForErrors)
		{
			if (logType == LogType.Warning || logType == LogType.Error || logType == LogType.Exception)
				longMessage = true;
			else
				longMessage = false;
		}

		logMessage(message,stackTrace,logType,longMessage);
	}

	void logMessage(string message, string stackTrace, LogType logType, bool longMessage = false)
	{
		string logEntry;

		if (longMessage)
			logEntry = string.Format("{0} : {1}\n{2}"
			,logType,message,stackTrace);
		else //Short message
			logEntry = string.Format("{0} : {1}\n",logType, message);
		
		if (logType == LogType.Error || logType == LogType.Exception) //Give errors and Exceptions a red color
			logEntry = "<color=red>" + logEntry + "</color>";
		else if (logType == LogType.Warning) //Give warnings a orange color
			logEntry = "<color=orange>" + logEntry + "</color>";

		Messages.Add (logEntry);
		updateTextPanel(Messages,DisplayedMessages);
	}

	/// <summary>
	/// Takes the last x messages in the list and displays them
	/// </summary>
	void updateTextPanel(List<string> messages, int messagesToDisplay)
	{
		string displayedString = "";
		for (int i = messages.Count - 1, j = messagesToDisplay; i >= 0 && j > 0; i--,j--) 
			displayedString += messages [i];
		
		TextPanel.text = displayedString;
		TextPanel.SetAllDirty();
	}

}
}