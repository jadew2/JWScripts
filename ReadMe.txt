ReadMe
JW Scripts are scripts are for use in any Unity Game Project.

There are several systems in the JW namespace.

Installation:
    To use JWScripts in your project, you need to import Unity IAP and Unity Ads.

JW.Standard:
    Helpful scripts that do simple things. E.g. Slide a Texture, Rotate a object over time etc. 
JW.Utils:
    Helpful extension methods and static logic/math classes.
JW.UI:
    Scripts that help with UI work. E.g. Has scripts that help with creating a selectable list.
JW.Constraints:
    A orderable 3D constraint system. Positions, rotates, and scales objects using the properties of other objects.
JW.Modifiers:
    A orderable mesh deforming system. E.g. Helps deform meshes along a curve.
JW.Tween:
    Contains SJTween, a custom tweener to animate things.
JW.CharacterController:
    Contains a character controller system based on animator state behaviours.
JW.CharacterCustomization:
    Contains a character customization system based on binding gameobjects to skeletons.
JW.Modules:
    Scripts that help with module based things.
JW.Pool:
    Contains generic scripts for object pooling optimization.
JW.Spline:
    A spline system. Performs the complex math for positioning things on a curved line.



