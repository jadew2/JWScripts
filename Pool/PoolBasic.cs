﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using JW.Pool.Internal;

namespace JW.Pool
{
/// <summary>  
/// <para> By: Jade White 2014, for Project Invertro </para>
/// <para> Simple non-tracking pooling solution with a optional max. This is to avoid creating stuff during runtime, because it can be slow.
/// If the max is set and the user breaches it, null is returned and a warning is given. </para>
/// 
/// <para> Main Methods are: </para>
/// <list type="number">
/// 	<item> GetFromPool T (). Returns the last object in the list and removes it. If none exist, instantate a new object.</item>
/// 	<item> ReturnToPool (T obj). Adds obj back into the pool list. </item>
/// </list>
/// 	
/// </summary>
/// <remarks> Getting or returning objects to the pool does not modify them in any way. You may want to enable/reset them as needed. </remarks>
public class PoolBasic<T> : PoolBase<T> 
{	
	readonly List<T> pooledObjects = new List<T>();
	int createdObjects = 0;

	int maxObjects = -1;
	int initialObjects = 0;

	/// <summary> Initializes a Empty pool. You need to call Init() before using the pool.</summary>
	public PoolBasic(){}
	/// <summary> Creates a empty pool with a maxObject limit. You need to call Init() before using the pool.</summary>
	public PoolBasic (int maxObjects, int initialObjects = 0)
	{
		this.maxObjects = maxObjects;

		if (initialObjects > maxObjects)
		{
			Debug.LogWarning(typeof(PoolBasic<T>).Name + ": InitialObjects cannot be larger than the maxObjects. Using maxObjects.");
			initialObjects = maxObjects;
		}
		this.initialObjects = initialObjects;
	}

	/// <summary> Initialzies the pool, filling it with initial objects. </summary>
	override public void Init()
	{
		base.Init();

		for (int i = 0; i < initialObjects; i++) 
		{
			T newObj = OnCreateObject();
			pooledObjects.Add(newObj);
		}
	}

	/// <summary> Clears the pool and destroys all objects in it. </summary>
	override public void Clear()
	{
		base.Clear();

		createdObjects = 0;
		for (int i = 0; i < pooledObjects.Count; i++) 
		{
			if (pooledObjects[i].Equals(default(T)))
				OnDestroyObject(pooledObjects[i]);
		}
		pooledObjects.Clear();
	}

	/// <summary> Retrieves a object from the pool calling OnGetObject. If a avaliable one does not exist, it creates a new one using OnCreateObject. </summary>
	override public T GetObject()
	{
		base.GetObject();

		T returnedObject = default(T);

		if (pooledObjects.Count > 0)
		{//Return last object, remove object from list
			int lastIndex = pooledObjects.Count - 1;
			returnedObject = pooledObjects[lastIndex];
			pooledObjects.RemoveAt(lastIndex);
		}
		else //Count <= 0
		{
			if ((maxObjects != -1) && (createdObjects >= maxObjects))
			{
				Debug.LogWarning("JWPoolBasic: You have already created the maximum amount of objects for this pool. Change the max if you want.");
				return default(T);
			}

			createdObjects++;
			returnedObject = OnCreateObject();
		}

		OnGetObject(returnedObject);
		return returnedObject;
	}

	/// <summary> Returns the object back to pool, calling OnReturnObject. </summary>
	override public void ReturnObject(T obj)
	{
		base.ReturnObject(obj);
		
		pooledObjects.Add(obj);

		OnReturnObject(obj);
		return;
	}
		
}
}
