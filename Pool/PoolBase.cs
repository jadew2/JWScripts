﻿using UnityEngine;
using System;
using System.Collections;

namespace JW.Pool.Internal
{
///<summary> Base for Pools simply throws errors if the user forgets to initialize things. Also contains the event delegates. This is not a working pool. </summary>
public class PoolBase<T> : IPool<T>
{
	//---Callbacks (Outside implementation)

	///<summary> Function that must create an object </summary>
	public Func<T> OnCreateObject = () => default(T); 
	///<summary> Function that must destroy an object </summary>
	public Action<T> OnDestroyObject = x => {};
	///<summary> Called when the Object is retrived </summary>
	public Action<T> OnGetObject = x => {};
	///<summary> Called when the Object is returned </summary> 
	public Action<T> OnReturnObject = x => {};

	bool initialized = false;
	public bool IsInitialized
	{
		get { return initialized; }
	}
	
	
	//---Pool Class Methods

	/// <summary> Method that intializes the pool with it's default contents. </summary>
	virtual public void Init() 
	{
		if (initialized)
			Clear();
		initialized = true;
	}
	/// <summary> Method that clears the pool and destroys it's contents. </summary>
	virtual public void Clear() 
	{
		initialized = false;
	}

	/// <summary> Method that gets an object from the pool. </summary>
	virtual public T GetObject()
	{
		if (!initialized)
			Debug.Log(typeof(PoolBasic<T>).Name + ": GetObject: Pool is not Initialized!"); 
		return default(T);
	}
	/// <summary> Method that returns an object back to the pool. </summary>
	virtual public void ReturnObject(T obj)
	{
		if (!initialized)
			Debug.Log(typeof(PoolBasic<T>).Name + ": ReturnObject: Pool is not Initialized!"); 
	}

}
}