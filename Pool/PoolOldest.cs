﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using JW.Pool.Internal;

namespace JW.Pool
{
/// <summary>
/// <para> Pooling solution with a set max. Unlike the normal JWPool, JWPoolOldest returns the oldest returned object if the max is exceeded. </para>
///
/// <para> Main Methods are: </para>
/// <list type="number">
/// 	<item> GetFromPool T (). Returns the "oldest" object at front of queue and removes it, also re-adds it at the back of the queue "newest". </item>
/// 	<item> ReturnToPool (T obj). Place returned object at front of queue to be used again. </item>
/// </list>
/// </summary>
/// <remarks> 
/// <para> RoundRobin uses a Linked list, but it's handled like a Queue. The "oldest" object is at the front, the newest object is at the back. </para>
/// <para> By: Jade White 2014, for Project Invertro </para>
/// </remarks>
public class PoolOldest<T> : PoolBase<T> {

	readonly LinkedList<T> pooledObjects = new LinkedList<T>();
	int maxObjects = -1;

	/// <summary> Creates a pool with the specified number of objects. You still need to set the event delegates before you can use the pool. </summary>
	public PoolOldest(int maxObjects)
	{
		this.maxObjects = maxObjects;
	}

	/// <summary> Initializes Pool with default objects. </summary>
	override public void Init()
	{
		base.Init();
		
		for (int i = 0; i < maxObjects; i++) 
		{
			T obj = OnCreateObject();
			pooledObjects.AddFirst(obj);
		}
	}

	/// <summary> Clear pool and destroy all it's contained objects. </summary>
	override public void Clear()
	{
		base.Clear();

		int count = pooledObjects.Count;
		for (int i = 0; i < count; i++) 
		{
			OnDestroyObject(pooledObjects.First.Value);
			pooledObjects.RemoveFirst();
		}
		pooledObjects.Clear();
	}

	/// <summary> Retrieves a free object from the pool. If a avaliable one does not exist, retrieves the oldest last-retrieved object. </summary>
	override public T GetObject()
	{
		base.GetObject();

		if (!IsInitialized)
		{
			Debug.LogError(typeof(PoolOldest<T>).Name + ": GetObject(): Not initialized!");
			return default(T);
		}

		T obj = pooledObjects.First.Value; //Return first element
		pooledObjects.RemoveFirst();
		pooledObjects.AddLast(obj); //Add to end of list again

		OnGetObject(obj);
		return obj;
	}
		
	/// <summary> Returns the object back to pool, calling OnReturnObject. </summary>
	override public void ReturnObject(T obj)
	{
		base.ReturnObject(obj);

		if (pooledObjects.Remove(obj) == false) //Remove it
		{
			Debug.LogError("JWPoolOldest: Object many not be part of initilized LinkedList!");
			return;
		}
		pooledObjects.AddFirst(obj);

		OnReturnObject(obj);
	}


}
}