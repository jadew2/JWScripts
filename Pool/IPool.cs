﻿using UnityEngine;
using System;
using System.Collections;

namespace JW.Pool
{
///<summary> Interface for Pools, if you're looking to add features, change this first. </summary>
public interface IPool<T>
{
	//---Pool Class Methods

	/// <summary> Method that intializes the pool with it's default contents. </summary>
	void Init();
	/// <summary> Method that clears the pool and destroys it's contents. </summary>
	void Clear(); 
	/// <summary> Method that gets an object from the pool. </summary>
	T GetObject();
	/// <summary> Method that returns an object back to the pool. </summary>
	void ReturnObject(T obj);
}
}