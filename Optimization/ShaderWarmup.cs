﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JW.Optimization
{
	/// <summary>
	/// Ensures all shaders in the scene are compiled.
	/// Avoids many hiccups when instantiating objects - but substitutes it for one enormous hiccup.
	/// </summary>
	public class ShaderWarmup : MonoBehaviour
	{
		public bool PerformWarmup = true;

		void Awake()
		{
			SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
		}

		void OnDestroy()
		{
			SceneManager.sceneLoaded -= SceneManagerOnSceneLoaded;
		}

		void SceneManagerOnSceneLoaded(Scene arg0, LoadSceneMode loadSceneMode)
		{
			if (PerformWarmup)
				Shader.WarmupAllShaders();
		}
	}
}
