﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Optimization
{
public class ShaderVariantCollectionWarmup : MonoBehaviour
{
        [SerializeField]
        ShaderVariantCollection ShaderVariantCollection = null;

	void Awake()
	{
		if (ShaderVariantCollection == null)
		{
			Debug.LogWarning(typeof(ShaderVariantCollectionWarmup).Name + ": Missing collection to load. Doing nothing.");
			return;
		}
		
		if (!ShaderVariantCollection.isWarmedUp)
			ShaderVariantCollection.WarmUp();
	}
	
	void OnDestroy()
	{

	}


}
}
