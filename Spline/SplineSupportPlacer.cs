﻿using System;
using System.Collections;
using System.Collections.Generic;
using JW.Spline;
using UnityEngine;

public class SplineSupportPlacer : MonoBehaviour
{
	public GameObject SupportPrefab = null;
	public Vector3 SupportScaling = Vector3.one;
	public SplineManager Spline;

	
	
	[Space]
	public float StartSpacing = 5.0f;
	public float EndSpacing = 5.0f;
	public float Spacing = 10.0f;
	[Space] 
	public float FloorY = 0.0f;
	
	public void PlaceSupports()
	{
		if (Spline == null)
		{
			Debug.LogError(typeof(SplineSupportPlacer).Name + ": No spline set!");
			return;
		}
		if (SupportPrefab == null)
		{
			Debug.LogError(typeof(SplineSupportPlacer).Name + ": No support prefab set!");
			return;
		}
		
		//First destroy everything in the children
		Transform[] children = transform.GetDirectChildren();
		foreach (var child in children)
		{
			//Destroy immediate if in the editor
			if (Application.isPlaying)
				Destroy(child.gameObject);
			else
				DestroyImmediate(child.gameObject);
		}

		//Now, the for loop of legend.
		float maxArcLength = Spline.GetArcLength() - EndSpacing;
		float currArcLength = StartSpacing;
		
		while (currArcLength < maxArcLength)
		{
			float t = Spline.GetTForArcLength(currArcLength);
			Vector3 topPosition = Spline.PointAt(t);
			Vector3 bottomPosition = new Vector3(topPosition.x,FloorY,topPosition.z);
			Vector3 midPoint = (topPosition - bottomPosition) * 0.5f + bottomPosition;
			
			GameObject newSupport = Instantiate<GameObject>(SupportPrefab);
			newSupport.transform.parent = transform;
			Mesh supportMesh = newSupport.GetComponent<MeshFilter>().sharedMesh;

			Vector3 wsScale = supportMesh.bounds.extents;
			//This scale makes the object 1,1,1
			Vector3 inverseScale = new Vector3(
				(Math.Abs(wsScale.x) < float.Epsilon) ? 0.0f : 1.0f / wsScale.x ,
				(Math.Abs(wsScale.y) < float.Epsilon) ? 0.0f : 1.0f / wsScale.y,
				(Math.Abs(wsScale.z) < float.Epsilon) ? 0.0f : 1.0f / wsScale.z);
			
			newSupport.transform.position = midPoint;
			newSupport.transform.localScale = Vector3.Scale(inverseScale,new Vector3(SupportScaling.x,midPoint.y,SupportScaling.z));
			
			currArcLength += Spacing;
		} 


	}
}
