﻿using UnityEngine;
using System.Collections;

namespace JW.Spline
{
/// <summary> 
/// <para> Node Types: </para>
/// <list type="bullet">
/// 	<item> Smooth: Aligned Tangents, not equalized </item>
/// 	<item> Corner: Tangents not aligned or at zero, not equalized </item>
/// 	<item> Symmetric: Aligned Tangents, Equalized </item>
/// 	<item> AutoSmooth: Aligned Tangents, not nessesarily equalized but set for a round shape. </item>
/// </list>
/// </summary>
public enum SplineNodeType {Smooth,Corner,Symmetric,AutoSmooth}

[System.Serializable]
public class SplineNode
{
	public Transform Middle, LTangent, RTangent;
	public SplineNodeType NodeType;

	public SplineNode(int indexNumber)
	{//Create a new node with GO's
		Middle 		= new GameObject().transform;
		LTangent 	= new GameObject().transform;
		RTangent 	= new GameObject().transform;

		LTangent.parent = Middle;
		RTangent.parent = Middle;
		
		ResetTransforms();
		RenameTransforms(indexNumber);
		NodeType = SplineNodeType.Smooth;
	}

	public SplineNode(Transform middle, Transform lTangent, Transform rTangent)
	{//Create a node by refrences
		Middle = middle;
		LTangent = lTangent;
		RTangent = rTangent;
		NodeType = SplineNodeType.Smooth;
	}

	public void ResetTransforms()
	{
		Middle.rotation = Quaternion.identity;
		LTangent.rotation = Quaternion.identity;
		RTangent.rotation = Quaternion.identity;
		
		LTangent.localPosition = new Vector3(0f,0f,-1f);
		RTangent.localPosition = new Vector3(0f,0f,1f);
	}

	public void RenameTransforms(int indexNumber)
	{
		Middle.name 	= "Node" + indexNumber;
		LTangent.name 	= "Node" + indexNumber + " LTangent";
		RTangent.name 	= "Node" + indexNumber + " RTangent";
	}

	public void DestroyTransforms()
	{
		if (Middle != null)
			GameObject.DestroyImmediate(Middle.gameObject);
	}

	public bool HasNullTransforms()
	{
		return (Middle == null || LTangent == null || RTangent == null);
	}
}
}