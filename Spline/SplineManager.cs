﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JW.Spline.Internal;

namespace JW.Spline
{

/// <summary> A SplineObject MonoBehaviour, used for switching between multiple spline types easily </summary>
[System.Serializable]
public class SplineManager : MonoBehaviour , ISplineObject
{
	public enum SplineType {Beizer,Polyline};

	#region Properties
	[SerializeField] SplineType managerSplineType = SplineType.Beizer;
	public SplineType ManagerSplineType
	{
		get {return managerSplineType;}
		set
		{
			if (value != managerSplineType)
				createNewSplineObject(value);
			managerSplineType = value;
		}
	}
		
	[SerializeField] AbstractSplineObject internalSpline;
	public AbstractSplineObject InternalSpline 
	{
		get
		{
			if(internalSpline == null)
				createNewSplineObject(ManagerSplineType);
			return internalSpline;

		}
		set{internalSpline = value;}
	}

	void createNewSplineObject(SplineType type)
	{
		if (type == SplineType.Beizer)
			internalSpline = new BeizerSplineObject();
		else if (type == SplineType.Polyline)
			internalSpline = new PolylineSplineObject();
		internalSpline.Nodes = splineNodes;
	}

	public List<SplineNode> Nodes {get{return splineNodes;} set{splineNodes = value;}}
	[SerializeField] public List<SplineNode> splineNodes;
	#endregion

	#region IJWSpline implementation
	public float MaxT   		  {get {return InternalSpline.MaxT;}}
	public SplineNode FirstNode {get {return InternalSpline.FirstNode;}}
	public SplineNode LastNode  {get {return InternalSpline.LastNode;}}
	
	public Vector3 PointAt (float t)
	{return InternalSpline.PointAt(t);}

	public Vector3 TangentAt (float t)
	{return InternalSpline.TangentAt(t);}

	public Quaternion RotationAt (float t)
	{return InternalSpline.RotationAt(t);}

	public Vector3 ScaleAt (float t)
	{return InternalSpline.ScaleAt(t);}

	public float GetArcLength ()
	{return InternalSpline.GetArcLength();}

	public float GetTForArcLength (float arcLength, List<float> pooledArcLengthList = null)
	{return InternalSpline.GetTForArcLength(arcLength,pooledArcLengthList);}

	public float GetTClosestTo (Vector3 point, int? sampleSegments = null)
	{return InternalSpline.GetTClosestTo(point, sampleSegments);}

	public float GetTForDistance (float t, float distance)
	{return InternalSpline.GetTForDistance(t,distance);}

	public bool IsWellFormed()
	{return InternalSpline.IsWellFormed();}

	#endregion
}
}