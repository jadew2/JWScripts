using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using JW.Spline.Internal;

/* Various Spline Editor Methods
 * 
 * Does things like rename all nodes in a list, set the position of a new node, set the position like it was subdivided etc.
 * 
 * Has methods that deal with:
 * 1. Adding/Deleting/Setting/Renaming Nodes
 * 2. Editing Nodes in viewport
 */

namespace JW.Spline
{
	public static class SplineEditorUtils {
		
		//--- Adding/Removing Nodes
		public static SplineNode AddNode(int at, ISplineObject spline)
		{//At: Index. 
		 //Creates and adds a new node to the list, decides to use Subdivide or not.
			var splineNodes = spline.Nodes;
			SplineNode newNode;

			bool atEnd = (at >= splineNodes.Count);
			if ((at == 0) || atEnd)
			{
				newNode = new SplineNode(splineNodes.Count);
				newNode = SetNodeAtAEnd(atEnd,spline,newNode); //PrePend

				if (atEnd)
					splineNodes.Add(newNode);
				else
					splineNodes.Insert(0,newNode);
			}
			else //Subdivide
			{
				int lNodeIndex = Mathf.Clamp(at - 1,0,splineNodes.Count - 2);
				newNode = Subdivide(lNodeIndex,spline); //Also adds a new node
			}

			return newNode;
		}

		public static void RemoveNode(int at, ISplineObject spline) //Defunct, using reorderable list.
		{//Decides to use DeSubdivide or not. 
			var splineNodes = spline.Nodes;

			if ((at <= 0) || (at >= splineNodes.Count - 1))
			{
				var oldNode = splineNodes[at];
				oldNode.DestroyTransforms();
				splineNodes.RemoveAt(at); //Throw Errors if needed
			}
			else //De Subdivide
			{//1 <= at <= Count-2
				int lNodeIndex = Mathf.Clamp(at - 1,0,splineNodes.Count - 2);
				DeSubdivide(lNodeIndex,spline);
			}
		}

		public static SplineNode Subdivide(int lNodeIndex, ISplineObject spline)
		{//Subdivide between lNode and rNode. Adds a new node.
			var splineNodes = spline.Nodes;

			//Find the nodes left and right to the newNode
			var lNode = splineNodes[lNodeIndex];
			var rNode = splineNodes[lNodeIndex + 1];

			if (lNode.HasNullTransforms() || rNode.HasNullTransforms())
			{
				Debug.LogError("SetNodePositionSubdivide: One of the surrounding nodes has null transforms.");
				return null;
			}

			//Create new node
			var newNode = new SplineNode(splineNodes.Count);

			//Set New Node Position
			float tMiddle = (float)lNodeIndex + 0.5f;
			newNode.Middle.position = spline.PointAt(tMiddle);
			newNode.Middle.rotation = spline.RotationAt(tMiddle);

			//Set Tangents
			float scaleToLNode = (lNode.Middle.position - newNode.Middle.position).magnitude/3f;
			float scaleToRNode = (rNode.Middle.position - newNode.Middle.position).magnitude/3f;
			
			lNode.RTangent.localPosition = new Vector3(0f,0f,scaleToLNode);
			rNode.LTangent.localPosition = new Vector3(0f,0f,-scaleToRNode);
			
			newNode.LTangent.localPosition = new Vector3(0f,0f,-scaleToLNode);
			newNode.RTangent.localPosition = new Vector3(0f,0f,scaleToRNode);

			splineNodes.Insert(lNodeIndex + 1,newNode);
			return newNode;
		}

		public static void DeSubdivide(int lNodeIndex, ISplineObject spline)
		{//Desubdivide. Deletes oldNode
			var splineNodes = spline.Nodes;
			
			//Find the nodes left and right to the newNode
			var lNode = splineNodes[lNodeIndex];
			var oldNode = splineNodes[lNodeIndex + 1];
			var rNode = splineNodes[lNodeIndex + 2];

			float scaleToTangents = (rNode.Middle.position - lNode.Middle.position).magnitude / 3f;
			lNode.RTangent.localPosition = new Vector3(0f,0f,scaleToTangents);
			rNode.LTangent.localPosition = new Vector3(0f,0f,-scaleToTangents);

			oldNode.DestroyTransforms();
			splineNodes.RemoveAt(lNodeIndex + 1);
		}

		//--- Methods setting up Nodes, deal with Game object names
		public static void SetNodeByTransform(SplineNode node, Transform newMiddle)
		{//Update JWSplineNode l and r tangents based on the children of the newMiddle
			if (node.Middle == newMiddle)
				return; //Return if there is no change to the Middle
			if (newMiddle == null)
			{
				node.Middle = null;
				node.LTangent = null;
				node.RTangent = null;
				return;
			}

			//If we are here, the Middle is different
			Transform foundLTangent = null;
			Transform foundRTangent = null;
			
			for (int i = 0; i < newMiddle.childCount; i++) 
			{
				Transform child = newMiddle.GetChild(i);
				if (child.name.IndexOf("LTangent") != -1)
					foundLTangent = child;
				else if (child.name.IndexOf("RTangent") != -1)
					foundRTangent = child;
			}
			
			if (foundLTangent == null || foundRTangent == null)
				Debug.LogError("JWSplineEditorUtils: SetNode: Couldn't find tangents for Node with transform \"" + newMiddle.name + "\".\n Be sure it has two children containing \"LTangent\" and \"RTangent\" in the name.");

			node.Middle = newMiddle;
			node.LTangent = foundLTangent;
			node.RTangent = foundRTangent;
		}
		
		public static void RenameAllNodes(List<SplineNode> nodes)
		{
			for (int i = 0; i < nodes.Count; i++)
			{
				SplineNode node = nodes[i];
				if (node.HasNullTransforms())
					continue;
				
				nodes[i].RenameTransforms(i);
			}
		}

		static SplineNode SetNodeAtAEnd(bool atEnd, ISplineObject spline, SplineNode node)
		{//Set Node position/rotation as if it was extrapolated off of the start/end.
						
			if (atEnd)
			{
				if (spline.LastNode != null)
				{
					//Place the next one double the length of the middle to the tangent
					float tangentSpacing = 2.5f; 
					if (spline.LastNode.RTangent != null)
						tangentSpacing = (spline.LastNode.RTangent.position - spline.LastNode.Middle.position).magnitude;
					
					node.Middle.position = spline.LastNode.Middle.position + spline.LastNode.Middle.forward * (tangentSpacing * 2);
					node.Middle.rotation = spline.LastNode.Middle.rotation;
					
					if (node.LTangent != null)
						node.LTangent.localPosition = new Vector3(0,0,-tangentSpacing);
					if (node.RTangent != null)
						node.RTangent.localPosition = new Vector3(0,0,tangentSpacing);
				}
			}
			else //At Start
			{
				if (spline.FirstNode != null)
				{
					//Place the next one double the length of the middle to the tangent
					float tangentSpacing = 2.5f;
					if (spline.FirstNode.LTangent != null)
						tangentSpacing = (spline.FirstNode.LTangent.position - spline.FirstNode.Middle.position).magnitude;
					
					node.Middle.position = spline.FirstNode.Middle.position - spline.FirstNode.Middle.forward * (tangentSpacing * 2);
					node.Middle.rotation = spline.FirstNode.Middle.rotation;
					
					if (node.LTangent != null)
						node.LTangent.localPosition = new Vector3(0,0,-tangentSpacing);
					if (node.RTangent != null)
						node.RTangent.localPosition = new Vector3(0,0,tangentSpacing);
				}
			}

			return node;
		}

		//---Reconstruction Methods
		public static void ReconstructNodeListFromParent(List<SplineNode> nodeList, Transform parentOfNodes)
		{
			Debug.Log("JWSplineEditorUtils: Attempting to reconstruct node list by order of child transforms.");
			nodeList.Clear();
			foreach (Transform child in parentOfNodes)
			{
				SplineNode newNode = new SplineNode((Transform)null,(Transform)null,(Transform)null);
				SetNodeByTransform(newNode,child);
				nodeList.Add(newNode);
			}
			Debug.Log("JWSplineEditorUtils: List reconstruction completed.");
		}

		//---Node Editing Methods
		public static void HandleNodeEditing(List<SplineNode> nodes, bool autoSmoothRotFixing)
		{//Modifies Node Tangents if the NodeType is AutoSmooth.
			for (int i = 0; i < nodes.Count; i++)
			{
				SplineNode node = nodes[i];
				if (node.HasNullTransforms())
					continue;

			
				if (node.NodeType == SplineNodeType.AutoSmooth)
				{
					if (i == 0 || i == (nodes.Count-1)) //if we are at the ends, don't smooth.
						continue;

					//Code Thanks to Inkscape (Open source)
					//http://stackoverflow.com/questions/13037606/how-does-inkscape-calculate-the-coordinates-for-control-points-for-smooth-edges
					SplineNode lNode = nodes[i-1];
					SplineNode rNode = nodes[i+1];
					if (lNode.HasNullTransforms() || rNode.HasNullTransforms())
						continue;

					Vector3 vecNext = rNode.Middle.position - node.Middle.position;
					Vector3 vecPrev = lNode.Middle.position - node.Middle.position;
					float lenNext = vecNext.magnitude; 
					float lenPrev = vecPrev.magnitude;
					if ((lenNext <= 0f) || (lenPrev <= 0))
						continue;

					Vector3 dir = ((lenPrev/lenNext) * vecNext - vecPrev).normalized;

					if (autoSmoothRotFixing && (node.Middle.gameObject != Selection.activeGameObject)) //Rotate node if it isn't selected. Run always, as we may be editing other parts of the spline.
						node.Middle.rotation = Quaternion.LookRotation(dir,node.Middle.up);
					node.LTangent.position = -dir * (lenPrev/3f) + node.Middle.position;
					node.RTangent.position = dir * (lenNext/3f) + node.Middle.position;
				}
			}

		}

		//---Tangent Editing Methods
		public static void HandleTangentEditing(List<SplineNode> nodes, bool rotFixing)
		{//Equalize/Align tangents while editing. Depends on the Node Type
	     //If we deselect the tangent, run RotateToZeroOutTangents to rotate the node and tangents so the tangent's local vector will be (0,0,x).
			for (int i = 0; i < nodes.Count; i++)
			{
				SplineNode node = nodes[i];
				if (node.HasNullTransforms())
					continue;
				
				if (node.LTangent.gameObject == Selection.activeGameObject)
				{
					lastEditedNodeForTangent = node;
					editedNodeTangentwasRight = false;

					if (node.NodeType == SplineNodeType.Symmetric)
						node.RTangent.localPosition = -node.LTangent.localPosition;
					else if (node.NodeType == SplineNodeType.Smooth)
						node.RTangent.localPosition = -node.LTangent.localPosition.normalized * node.RTangent.localPosition.magnitude;
					else if (node.NodeType == SplineNodeType.AutoSmooth)
						node.NodeType = SplineNodeType.Smooth;
				}
				else if (node.RTangent.gameObject == Selection.activeGameObject)
				{
					lastEditedNodeForTangent = node;
					editedNodeTangentwasRight = true;

					if (node.NodeType == SplineNodeType.Symmetric)
						node.LTangent.localPosition = -node.RTangent.localPosition;
					else if (node.NodeType == SplineNodeType.Smooth)
						node.LTangent.localPosition = -node.RTangent.localPosition.normalized * node.LTangent.localPosition.magnitude;
					else if (node.NodeType == SplineNodeType.AutoSmooth)
						node.NodeType = SplineNodeType.Smooth;

				}
				else if (rotFixing && (node == lastEditedNodeForTangent))
				{ //If this passes, we aren't selecting any tangents of the node who's tangents we were just editing. We can use this opportunity to fix the rotation once.
					if ((node.NodeType == SplineNodeType.Symmetric) || (node.NodeType == SplineNodeType.Smooth))
						RotateToZeroOutTangents(lastEditedNodeForTangent,editedNodeTangentwasRight);
					lastEditedNodeForTangent = null; //Back to null, so we only do this once.
				}
			}
		}

		static SplineNode lastEditedNodeForTangent; //Keeps track of last node who's tangent was selected
		static bool editedNodeTangentwasRight = false; //Whether it was left/right
		static void RotateToZeroOutTangents(SplineNode node, bool rightTangent)
		{//Rotate Node so L/RTangent's local vector3 will be (0,0,x). This keeps the node Up rotation aligned
		 //Should only be run once when needed. Doesn't equalizie tangents.
			Transform RTangent = node.RTangent;
			Transform LTangent = node.LTangent;
			Transform Middle = node.Middle;

			Vector3 LTangentPos = LTangent.position;
			Vector3 RTangentPos = RTangent.position;
			Vector3 mPos = Middle.position;

			if (rightTangent)
			{
				Middle.rotation = Quaternion.LookRotation(RTangentPos - mPos,Middle.up);
				RTangent.localPosition = new Vector3(0f,0f,RTangent.localPosition.magnitude);
				LTangent.position = LTangentPos;
			}
			else
			{
				Middle.rotation = Quaternion.LookRotation(-(LTangentPos - mPos),Middle.up);
				LTangent.localPosition = new Vector3(0f,0f,-LTangent.localPosition.magnitude);
				RTangent.position = RTangentPos;
			}

		}	
	}
}
