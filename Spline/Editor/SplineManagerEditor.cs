using UnityEditor;
using UnityEngine;
using System.Collections;

using System; //Activator
using System.Collections.Generic;
using System.Linq;
//List

using UnityEditorInternal; //Reorderable list
using JW.Spline.Internal;

namespace JW.Spline
{
    /// <summary>
    /// <para>
    /// Editor Class for the SplineManager: 
    /// <list type="bullet">
    /// 	<item> Holds the reorderable list implementation + interface. </item>
    /// 	<item> provides instpector GUI interface. </item>
    /// 	<item> Manages when to draw things in viewport </item>
    /// </list>
    /// </para>
    /// This class makes heavy use of JWSplineDrawUtils (Drawing) and JWSplineEditorutils (Adding/Modifying/Editing Nodes)
    /// </summary>
    [CustomEditor(typeof(SplineManager))]
    public class SplineManagerEditor : Editor
    {
        private SerializedObject mainObject;
        private SplineManager splineManager;

        //---Static variables for Inspector
        static bool showEditOptions = false;

        static bool rotationFixingAutoSmooth = true;
        static bool rotationFixingOther = true;

        static bool showDrawOptions = false;

        static bool showCurve = true;
        static bool showTangents = true;
        static bool showNodeLabels = true;
        static bool showNormals = false;
        static Color nodeHandleColor = new Color32(255, 0, 0, 200);
        static Color tangentHandleColor = new Color32(255, 255, 255, 200); //White
        static Color splineColor = new Color32(0, 163, 255, 255); //Sky Blue
        static Color tangentColor = Color.white;

        static bool showTools = false;

        GUIStyle redLabelStyle;
        GUIStyle defaultStyle;

        ReorderableList reorderableList;

        public void OnEnable()
        {
            if (target == null)
                return;

            mainObject = new SerializedObject(target);
            splineManager = (SplineManager)target;

            localOnSceneDelegate = this.OnScene;
            SceneView.duringSceneGui -= StaticOnScene;
            SceneView.duringSceneGui += StaticOnScene;

            //splineManager.Init(); The InternalSpline property does this via lazy loading.
            setUpReorderableList();

            redLabelStyle = new GUIStyle();
            redLabelStyle.normal.textColor = Color.red;
            redLabelStyle.fontStyle = FontStyle.Bold;
            defaultStyle = new GUIStyle();
        }

        void setUpReorderableList()
        {
            var splineNodes = splineManager.Nodes;

            reorderableList = new ReorderableList(splineNodes, typeof(SplineNode), true, true, true, true);

            //---Header
            reorderableList.drawHeaderCallback =
                (Rect rect) => EditorGUI.LabelField(rect, "Node List: " + splineNodes.Count);

            //---Elements
            reorderableList.drawElementCallback =
                (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = splineNodes[index];
                if (element == null)
                    return;

                float xOffset = rect.x;
                float width = 25f;
                EditorGUI.LabelField(new Rect(xOffset, rect.y, width, EditorGUIUtility.singleLineHeight), index + ".");
                xOffset += width;

                width = 15f;
                EditorGUI.LabelField(new Rect(xOffset, rect.y, width, EditorGUIUtility.singleLineHeight),
                                     "N:", (element.HasNullTransforms()) ? redLabelStyle : defaultStyle);
                xOffset += width;


                width = 100f;
                Transform newMiddle = (Transform)EditorGUI.ObjectField(new Rect(xOffset, rect.y, width, EditorGUIUtility.singleLineHeight),
                                                        element.Middle, typeof(Transform), true);
                xOffset += width;

                if (element.Middle != newMiddle)
                    SplineEditorUtils.SetNodeByTransform(element, newMiddle);

                if (rect.width > 420f)
                {
                //Left Tangent
                width = 24f;
                    EditorGUI.LabelField(new Rect(xOffset, rect.y, width, EditorGUIUtility.singleLineHeight),
                                         "LT:", (element.LTangent == null) ? redLabelStyle : defaultStyle);
                    xOffset += width;

                    width = 84f;
                    element.LTangent = (Transform)EditorGUI.ObjectField(new Rect(xOffset, rect.y, width, EditorGUIUtility.singleLineHeight),
                                                                            element.LTangent, typeof(Transform), true);
                    xOffset += width;

                //Right Tangent
                width = 24f;
                    EditorGUI.LabelField(new Rect(xOffset, rect.y, width, EditorGUIUtility.singleLineHeight),
                                         "RT:", (element.RTangent == null) ? redLabelStyle : defaultStyle);
                    xOffset += width;

                    width = 84f;
                    element.RTangent = (Transform)EditorGUI.ObjectField(new Rect(xOffset, rect.y, width, EditorGUIUtility.singleLineHeight),
                                                                         element.RTangent, typeof(Transform), true);
                    xOffset += width;
                }
                else
                {
                //...
                width = 15f;
                    EditorGUI.LabelField(new Rect(xOffset, rect.y, width, EditorGUIUtility.singleLineHeight),
                                         "...");
                    xOffset += width;
                }

                element.NodeType = (SplineNodeType)EditorGUI.EnumPopup(new Rect(xOffset, rect.y, 80f, EditorGUIUtility.singleLineHeight)
                                                       , element.NodeType);

            };

            //OnAdd (Popup menu)
            reorderableList.onAddCallback =
                (ReorderableList list) =>
            {
                SplineNode node = SplineEditorUtils.AddNode(list.index + 1, splineManager);
                node.Middle.transform.parent = splineManager.transform;
                list.index++;
            };

            //On Selected
            /*reorderableList.onSelectCallback =
                (ReorderableList list) =>
            {};*/

            //OnRemove
            reorderableList.onRemoveCallback =
                (ReorderableList list) =>
            {
                splineNodes[list.index].DestroyTransforms();
                ReorderableList.defaultBehaviours.DoRemoveButton(list);
            //list.onSelectCallback(list); //Force a onSelectCallback to update the GUI
        };

        }


        public override void OnInspectorGUI()
        {
            mainObject.Update(); //Load changes from the Gameobject into the GUI

            GUILayout.BeginHorizontal();
            GUILayout.Label("JWSplineManager V2.1 - Now with GetTForArcLength!");

            if (splineManager == null || splineManager.Nodes == null) //Nessesary for Unity 4.6+. First frame isn't ready.
            {
                GUILayout.EndHorizontal();
                GUILayout.Label("List not initialized.");
                if (GUILayout.Button("Start New List"))
                {
                    splineManager.Nodes = new List<SplineNode>();
                }
            }
            else
            {
                splineManager.ManagerSplineType = (SplineManager.SplineType)EditorGUILayout.EnumPopup(splineManager.ManagerSplineType, GUILayout.MaxWidth(100f));
                GUILayout.EndHorizontal();

                reorderableList.DoLayoutList();
            }

            //--- "Edit Options"
            showEditOptions = EditorGUILayout.Foldout(showEditOptions, "Edit Options");
            if (showEditOptions)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Auto Rotation Fixing:  AutoSmooth:");
                rotationFixingAutoSmooth = EditorGUILayout.Toggle(rotationFixingAutoSmooth);
                GUILayout.Label("Other Node Types:");
                rotationFixingOther = EditorGUILayout.Toggle(rotationFixingOther);
                GUILayout.EndHorizontal();
            }

            showTools = EditorGUILayout.Foldout(showTools, "Tools");
            if (showTools)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Rename All Nodes"))
                    SplineEditorUtils.RenameAllNodes(splineManager.Nodes);
                if (GUILayout.Button("Reconstruct NodeList"))
                    SplineEditorUtils.ReconstructNodeListFromParent(splineManager.Nodes, splineManager.transform);
                GUILayout.EndHorizontal();
            }

            //--- "Draw Options"
            showDrawOptions = EditorGUILayout.Foldout(showDrawOptions, "Draw Options");
            if (showDrawOptions)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Curve:");
                showCurve = EditorGUILayout.Toggle(showCurve);
                GUILayout.Label("Tangents:");
                showTangents = EditorGUILayout.Toggle(showTangents);
                GUILayout.Label("Rotation Normals:");
                showNormals = EditorGUILayout.Toggle(showNormals);
                GUILayout.Label("Node Labels:");
                showNodeLabels = EditorGUILayout.Toggle(showNodeLabels);
                GUILayout.EndHorizontal();

                //---"Colors"
                GUILayout.Label("Colors:");

                GUILayout.BeginHorizontal();
                GUILayout.Label("Spline:");
                splineColor = EditorGUILayout.ColorField(splineColor, GUILayout.MaxWidth(150));
                GUILayout.Label("Tangents:");
                tangentColor = EditorGUILayout.ColorField(tangentColor, GUILayout.MaxWidth(150));
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Nodes:");
                nodeHandleColor = EditorGUILayout.ColorField(nodeHandleColor, GUILayout.MaxWidth(150));
                GUILayout.Label("Handles:");
                tangentHandleColor = EditorGUILayout.ColorField(tangentHandleColor, GUILayout.MaxWidth(150));
                GUILayout.EndHorizontal();
            }

            mainObject.ApplyModifiedProperties(); //Apply any changes
        }

        //---OnScene Stuff
        static Action localOnSceneDelegate = null;
        static void StaticOnScene(SceneView sceneView)
        {
            if (localOnSceneDelegate != null)
                localOnSceneDelegate();
        }

        void OnScene()
        {
            if (splineManager == null)
                SceneView.duringSceneGui -= StaticOnScene;

            if (splineManager.Nodes == null)
                return;
            else
                drawSpline();
        }

        void drawSpline()
        {
            if (showCurve)
            {
                try
                {
                    SplineEditorDrawing.DrawSplineCurve(splineManager, splineColor, null, 2f);
                }
                catch (SplineCannotEvaluateException e)
                {
                    Debug.LogError(e.Message);
                    Debug.LogError("JWSplineManagerEditor: Drawing Failed: Fix faulty node and re-enable \"Curve\" from \"Draw Options\"");
                    showCurve = false;
                    showDrawOptions = true;
                }
            }

            if (showTangents)
            {
                SplineEditorDrawing.DrawTangents(splineManager.Nodes, tangentColor, null, 2f);
                SplineEditorDrawing.DrawTangentHandles(splineManager.Nodes, tangentHandleColor, 0.1f);
            }

            SplineEditorDrawing.DrawNodeHandles(splineManager.Nodes, nodeHandleColor, 0.2f);

            if (showNodeLabels)
                SplineEditorDrawing.DrawNodeLabels(splineManager.Nodes, Color.gray, 0.4f);

            if (showNormals)
            {
                try
                {
                    SplineEditorDrawing.DrawRotationNormals(splineManager, tangentColor, null, 3f);
                }
                catch (SplineCannotEvaluateException e)
                {
                    Debug.LogError(e.Message);
                    Debug.LogError("JWSplineManagerEditor: Drawing Failed: Fix faulty node and re-enable \"Rotation Normals\" from \"Draw Options\"");
                    showNormals = false;
                    showDrawOptions = true;
                }
            }

            SplineEditorUtils.HandleNodeEditing(splineManager.Nodes, rotationFixingAutoSmooth);
            SplineEditorUtils.HandleTangentEditing(splineManager.Nodes, rotationFixingOther);
        }

    }
}
