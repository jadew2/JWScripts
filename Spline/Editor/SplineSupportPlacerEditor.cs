﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(SplineSupportPlacer))] 
public class SplineSupportPlacerEditor : Editor
{
	SerializedObject mainObject;
	SplineSupportPlacer splineSupportPlacer;
	
	public void OnEnable ()
	{
		if (target == null)
			return;

		mainObject = new SerializedObject(target);
		splineSupportPlacer = (SplineSupportPlacer)target;
	}

	public override void OnInspectorGUI()
	{
		mainObject.Update();
		
		GUILayout.Label(typeof(SplineSupportPlacerEditor).Name + " V1.0");

		DrawDefaultInspector();
		
		if (GUILayout.Button("Place Supports"))
		{
			splineSupportPlacer.PlaceSupports();
		}

		mainObject.ApplyModifiedProperties();
	}

}
