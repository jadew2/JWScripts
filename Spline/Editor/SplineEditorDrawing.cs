﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JW.Spline.Internal;


namespace JW.Spline
{
/// <summary>
/// Various Methods to draw full curve handles/ nodes/ you name it.
/// Works for all spline types.
/// </summary>
public static class SplineEditorDrawing 
{
	/// <summary> General draw spline curve function. Makes line segments. </summary>
	public static void DrawSplineCurve(ISplineObject spline, Color colour, Texture2D tex, float width)
	{
		int segments = 10; //for each node
		float diffT = 1f/segments;
		List<SplineNode> nodes = spline.Nodes;
		if (nodes == null)
			return;

		Handles.color = colour;

		for (int i = 0; i < (nodes.Count - 1); i++) 
		{
			for (int j = 0; j < segments; j++) 
			{
				Vector3 p0 = spline.PointAt(diffT*j 	+ i);
				Vector3 p1 = spline.PointAt(diffT*(j+1) + i);
				Handles.DrawAAPolyLine(tex,width,p0,p1);
			}
		}
	}

	public static void DrawSplineCurveLine(List<SplineNode> nodes, Color colour, Texture2D tex, float width)
	{
		Color temp = Handles.color;
		Handles.color = colour;
		
		for (int i = 0; i < (nodes.Count - 1); i++)
		{
			SplineNode lNode = nodes[i];
			SplineNode rNode = nodes[i + 1];
			
			if ((lNode.Middle == null) || (rNode.Middle == null))
				continue;
			
			Handles.DrawAAPolyLine(tex,width,lNode.Middle.position,rNode.Middle.position);
		}
		
		Handles.color = temp;
	}


	public static void DrawTangents (List<SplineNode> nodes, Color colour, Texture2D tex, float width)
	{
		Handles.color = colour;

		for (int i = 0; i < nodes.Count; i++)
		{
			SplineNode node = nodes[i];

			if (node == null || node.Middle == null)
				continue;
			
			if (node.LTangent != null)
				Handles.DrawAAPolyLine(tex,width, node.Middle.position, node.LTangent.position);
			if (node.RTangent != null)
				Handles.DrawAAPolyLine(tex,width, node.Middle.position, node.RTangent.position);
		}

	}

	public static void DrawNodeHandles (List<SplineNode> nodes, Color colour, float size)
	{
		Handles.color = colour;

		for (int i = 0; i < nodes.Count; i++)
		{
			SplineNode node = nodes[i];
			if (node == null || node.Middle == null)
				continue;

			SplineDrawingUtils.HandleSelecting(node.Middle,size * HandleUtility.GetHandleSize(node.Middle.position),Handles.SphereHandleCap);
		}
	}

	public static void DrawTangentHandles (List<SplineNode> nodes, Color colour, float size)
	{
		Handles.color = colour;
	
		for (int i = 0; i < nodes.Count; i++)
		{
			SplineNode node = nodes[i];
			if (node == null || node.Middle == null)
				continue;

			if (node.LTangent != null)
				SplineDrawingUtils.HandleSelecting(node.LTangent,size * HandleUtility.GetHandleSize(node.LTangent.position),Handles.CubeHandleCap);
			if (node.RTangent != null)
				SplineDrawingUtils.HandleSelecting(node.RTangent,size * HandleUtility.GetHandleSize(node.RTangent.position),Handles.CubeHandleCap);
		}
	}

	public static void DrawRotationNormals (ISplineObject spline, Color colour,Texture2D tex, float width)
	{//Not a spline reference, because we can have null elements in edit mode
		int normals = 8; //for each segment
		float diffT = 1f/normals;
		List<SplineNode> nodes = spline.Nodes;
		if (nodes == null)
			return;

		Handles.color = colour;
		
		for (int i = 0; i < (nodes.Count - 1); i++) 
		{
			for (int j = 0; j < normals; j++) 
			{
				Vector3 point =  spline.PointAt(diffT*j+i);
				Vector3 normal = spline.RotationAt(diffT*j+i)*Vector3.up;
				Handles.DrawAAPolyLine(tex,width,point,point+normal * HandleUtility.GetHandleSize(point) * 0.5f);
			}
		}
	}

	public static void DrawNodeLabels(List<SplineNode> nodes, Color colour, float distFromNode)
	{
		for (int i = 0; i < nodes.Count; i++)
		{
			SplineNode node = nodes[i];
			if (node == null || node.Middle == null)
				continue;
			
			SplineDrawingUtils.DrawLabel(node.Middle,colour,distFromNode);
		}
	}
}

/// <summary>
/// More general drawing utilities, can be used by other systems.
/// </summary>
public static class SplineDrawingUtils
{
	/// <summary> If object is selected, return true. If we are clicking on it, select it, return true. Otherwise return false. </summary>
	public static bool HandleSelecting(Transform trans, float size, Handles.CapFunction func)
	{
		if (Selection.transforms.Contains(trans))
			return true; //The object is already selected
		else if(Handles.Button(trans.position,trans.rotation,size,size,func))
		{
			//If you click on the circle, you select it
			Event e = Event.current;
			if (e.shift)
			{
				//Add it as a additional selection
				var selectedGos = Selection.gameObjects.ToList();
				selectedGos.Add(trans.gameObject);
				Selection.objects = selectedGos.Cast<Object>().ToArray();
			}
			else
			{
				Selection.activeGameObject = trans.gameObject;
			}
			return true;
		}
		return false;
	}

	public static void DrawLabel(Transform transform, Color colour, float distFromNode)
	{
		Handles.color = colour;

		Handles.Label(transform.position + Vector3.up * distFromNode,transform.gameObject.name);
	}
}
}

/*
		public static void DrawSplineCurveBeizer(List<JWSplineNode> nodes, Color colour, Texture2D tex, float width)
		{
			for (int i = 0; i < (nodes.Count - 1); i++)
			{
				JWSplineNode lNode = nodes[i];
				JWSplineNode rNode = nodes[i + 1];

				if ((lNode.Middle == null) || (rNode.Middle == null))
					continue;
				if ((lNode.RTangent == null) || (rNode.LTangent == null))
					continue;

				Handles.DrawBezier(lNode.Middle.position,
				                   rNode.Middle.position,
				                   lNode.RTangent.position,
				                   rNode.LTangent.position,
				                   colour,tex,width);
			}
		}

		public static void DrawSplineCurveLine(List<JWSplineNode> nodes, Color colour, Texture2D tex, float width)
		{
			Color temp = Handles.color;
			Handles.color = colour;

			for (int i = 0; i < (nodes.Count - 1); i++)
			{
				JWSplineNode lNode = nodes[i];
				JWSplineNode rNode = nodes[i + 1];

				if ((lNode.Middle == null) || (rNode.Middle == null))
					continue;

				Handles.DrawAAPolyLine(tex,width,lNode.Middle.position,rNode.Middle.position);
			}

			Handles.color = temp;
		}
		*/