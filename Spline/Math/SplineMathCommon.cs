﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JW.Spline.Internal;
using JW.Utils;


namespace JW.Spline.Math
{
    ///<summary>
    /// Class for common spline functions that work for all splines.
    /// <para>However, because they are broad, they aren't all efficient (and mostly approximite divide and conquer algorithms) You should try to code custom methods to do these operations as much as possible.</para>
    /// </summary>
    public class SplineMathCommon
    {
        /// <summary>
        /// Number of line segments on each spline curve to approximate from. Must be >= 1.
        /// Greater Subdivisions are more accurate, but exponentially slower. 
        /// </summary>
        public const int SAMPLE_SEGMENTS = 9;
        public const float DELTA_T = 1f / (float)SAMPLE_SEGMENTS;
        ///<summary>
        /// The amount of distance for a step in GetTForDistance().
        /// Smaller values are more precise, but exponentially slower.
        /// Target Party: Average speed on rails is 40u/s / 60fps = 0.66, reducing that so ~3 iterations are made per frame = 0.2f.
        ///</summary>
        public const float T_FOR_DISTANCE_STEP_LENGTH = 0.20f;

        ListHelper listHelper = new ListHelper();

        public float GetArcLength(ISplineObject spline)
        {
            var nodes = spline.Nodes;
            if (nodes == null || nodes.Count <= 0)
                return 0f;

            float length = 0f;
            float t = 0f;

            Vector3 currPoint;
            Vector3 prevPoint = spline.PointAt(0f);

            int numberOfArcLengths = Mathf.Clamp((nodes.Count - 1), 0, int.MaxValue) * SAMPLE_SEGMENTS + 1; //+1 to include the end of the curve. 
            for (int i = 0; i < numberOfArcLengths; i++)
            {
                t += DELTA_T;
                currPoint = spline.PointAt(t);

                length += Vector3.Distance(currPoint, prevPoint);

                prevPoint = currPoint;
            }

            return length;
        }

        public float GetTForArcLength(ISplineObject spline, float arcLength, List<float> pooledList = null)
        {
            if (pooledList == null)
                pooledList = new List<float>();

            if (spline.Nodes == null || spline.Nodes.Count <= 0)
                return arcLength;

            List<float> arcLengthList = GetArcLengthList(spline, pooledList);

            if (arcLength >= arcLengthList[arcLengthList.Count - 1]) //Beyond Max
                return (arcLength - arcLengthList[arcLengthList.Count - 1]) + spline.MaxT;
            else if (arcLength <= 0f)//Beyond min
                return arcLength;

            //Otherwise, find the first length larger than arcLength
            int index = -1;
            for (int i = 0; i < arcLengthList.Count; i++)
            {
                if (arcLengthList[i] > arcLength)
                {
                    index = i;
                    break;
                }
            }
            //At this point, index must be >= 1.

            //Cool. Compute T.
            float prev = arcLengthList[index - 1]; //Safe, because 0 is not larger than any positive number (even 0) 
            float next = arcLengthList[index];

            float prevT = ((float)(index - 1) * DELTA_T);

            return prevT + (Mathf.InverseLerp(prev, next, arcLength) * DELTA_T); //prevT + (distance- prev)/difference * deltaT
        }

        /// <summary>Create list of elapsed ArcLengths e.g. (0f,0.5f,1f,3f....,7f, ArcLength)</summary>
        /// <returns>The arc length list.</returns>
        List<float> GetArcLengthList(ISplineObject spline, List<float> pooledList = null)
        {
            if (pooledList == null)
                pooledList = new List<float>();
            var list = pooledList;

            var nodes = spline.Nodes;

            if (nodes.Count <= 1)
            {
                listHelper.ResizeTo(1, list);
                list[0] = 0f;
                return list;
            }

            //First, resize the arc length list to match the length we're looking for
            int numberOfArcLengths = Mathf.Clamp((nodes.Count - 1), 0, int.MaxValue) * SAMPLE_SEGMENTS + 2; //+1 to include 0 and the end of the curve. //Important: If you change SAMPLE_SEGMENTS here, be sure to change it for GetTForArcLength.
            listHelper.ResizeTo(numberOfArcLengths, list);
            list[0] = 0f;

            float length = 0f;
            float currT = 0f;

            Vector3 currPoint;
            Vector3 prevPoint = spline.FirstNode.Middle.position;

            for (int i = 1; i < numberOfArcLengths; i++) //Start from 1, to ignore the first 0.
            {
                currT += DELTA_T;
                currPoint = spline.PointAt(currT);

                length += Vector3.Distance(currPoint, prevPoint);

                list[i] = length;

                prevPoint = currPoint;
            }
            return list;
        }

        public float GetTForDistance(ISplineObject spline, float t, float distance)
        {
            //If the distance travelled is very large, split it up into smaller distances and iterate.
            //This is because Bezier curve tangents are huge in the middle of the curve but small on the edges.
            //This way, we prevent erroneously going huge distances if the passed distance is large.
            while (distance >= T_FOR_DISTANCE_STEP_LENGTH)
            {
                distance -= T_FOR_DISTANCE_STEP_LENGTH;
                //Are the parenthesis there? if not you may wanna try adding those first.
                t += (1f / spline.TangentAt(t).magnitude) * T_FOR_DISTANCE_STEP_LENGTH;
            }

            //Add the remaining distance
            return t + (1f / spline.TangentAt(t).magnitude) * distance;
        }

        public float GetTClosestTo(ISplineObject spline, Vector3 worldPoint, int sampleSegments = SAMPLE_SEGMENTS)
        {
            var nodes = spline.Nodes;

            float deltaT = 1.0f / sampleSegments;

            float currT = 0f;
            float prevT = 0f;

            Vector3 currPoint = spline.PointAt(0f);
            Vector3 prevPoint = currPoint;

            float closestDist = Mathf.Infinity;
            float closestT = 0f;

            //Split the spline into line segments and return the closest point to them.
            int numberOfIterations = Mathf.Clamp((nodes.Count - 1), 0, int.MaxValue) * sampleSegments + 1; //+1 to include the end of the curve. 
            for (int i = 0; i < numberOfIterations; i++)
            {
                currT = prevT + deltaT;
                currPoint = spline.PointAt(currT);

                //Find the T of the closest point on the line segment.
                float localT = closestTTo(worldPoint, prevPoint, currPoint);
                float globalT = prevT + (localT * deltaT);

                //Find the distance to that point
                Vector3 linePoint = Vector3.Lerp(prevPoint, currPoint, localT);
                float sqrDist = (worldPoint - linePoint).sqrMagnitude;

                //If that's the shortest distance we've found...
                if (sqrDist <= closestDist)
                {
                    closestDist = sqrDist;
                    closestT = globalT;
                }

                prevT = currT;
                prevPoint = currPoint;
            }

            return closestT;
        }


        ///<summary>Finds the closest T on a line segment</summary>
        float closestTTo(Vector3 point, Vector3 p0, Vector3 p1)
        {
            Vector3 line = (p1 - p0);
            float lengthSquared = line.sqrMagnitude;
            if (lengthSquared == 0f) //If the line points are equal, return one of them.
                return 1f;

            float t = Vector3.Dot(point - p0, line) / lengthSquared;
            if (t < 0f)
                return 0f;   //We are beyond the 'lineStart' end of the segment.
            else if (t > 1f)
                return 0f;   //We are beyond the 'lineEnd' end of the segment.
            else
                return t;   //We are between the two points. Return the point on the line.
        }
    }
}
