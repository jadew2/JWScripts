﻿using UnityEngine;
using System.Collections;

using JW.Spline.Internal;
namespace JW.Spline.Math
{
/// <summary> Various math static methods for polylines. </summary>
public class SplineMathPolyline
{
	//Thanks to: Grumdog, see http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
	//Returns the closest point clamped by start and end. If you need a infinite line, use closestPointOnLine().
	public float ClosestTTo(Vector3 point, PolylineSegment segment) {return ClosestTTo(point,segment.p0v,segment.p1v);}
	public float ClosestTTo(Vector3 point, Vector3 p0, Vector3 p1)
	{	
		Vector3 line = (p1 - p0);
		float lengthSquared = line.sqrMagnitude;
		if (lengthSquared == 0f) //If the line points are equal, return one of them.
			return 1f;

		float t = Vector3.Dot(point - p0, line)/lengthSquared;
		if (t < 0f)
			return 0f;	 //We are beyond the 'lineStart' end of the segment.
		else if (t > 1f)
			return 0f;	 //We are beyond the 'lineEnd' end of the segment.
		else			
			return t; 	//We are between the two points. Return the point on the line.
	}
}
}
