using UnityEngine;
using System.Collections;

using JW.Spline.Internal;
namespace JW.Spline.Math
{

/// <summary>
/// Contains various mathy static methods for evaluating Bezier curves.
/// </summary>
public class SplineMathBeizer 
{
	public Vector3 PointAt(float t, BeizerSegment curve) {return PointAt(t,curve.p0v,curve.p1v,curve.p2v,curve.p3v);}
	public Vector3 PointAt(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
	{
		float u = 1 - t;
		float tt = t*t;
		float uu = u*u;
		float uuu = uu * u;
		float ttt = tt * t;
		
		Vector3 p = uuu * p0; //first term
		p += 3 * uu * t * p1; //second term
		p += 3 * u * tt * p2; //third term
		p += ttt * p3; //fourth term
		
		return p;
	}

	public Vector3 TangentAt(float t, BeizerSegment curve) {return TangentAt(t,curve.p0v,curve.p1v,curve.p2v,curve.p3v);}
	public Vector3 TangentAt(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
	{	//Calculate the Bezier Point Derivative
		float tt = t*t;
		
		Vector3 A = p3 - 3 * p2 + 3 * p1 - p0;
		Vector3 B = 3 * p2 - 6 * p1 + 3 * p0;
		Vector3 C = 3 * p1 - 3 * p0;
		
		Vector3 raw_tangent = 3 * A * tt;
		raw_tangent += 2 * B * t;
		raw_tangent += C;
		
		return raw_tangent;
	}
}
}
