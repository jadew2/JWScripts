﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* JWPolylineSplineObject : A Polyline Spline
 * See IJWSpline for a full rundown of the Spline System
 * 
 * Contains methods to evaluate a polyline spline
 * 
 */
using JW.Spline.Math;
using JW.Spline.Internal;

namespace JW.Spline
{
public class PolylineSplineObject : AbstractSplineObject
{
	readonly SplineMathPolyline polylineMath = new SplineMathPolyline();

	//---Spline Evaluation Methods
	public override Vector3 PointAt(float globalT)
	{
		EditorCheckNodesSafeAt(globalT,"PointAt");

		if (Nodes.Count == 0)  //No Nodes
		{
			Debug.LogError(typeof(PolylineSplineObject).Name + ": PointAt: Trying To find a point with no nodes!");
			return Vector3.zero;
		}
		else if (globalT <= 0f) //At Start
		{
			SplineNode firstNode = FirstNode;
			return firstNode.Middle.position + (firstNode.Middle.forward * globalT); //Extrapolate
		}
		else if (globalT >= (float)(Nodes.Count - 1)) //At End
		{
			SplineNode lastNode = LastNode;
			return lastNode.Middle.position + (lastNode.Middle.forward * (globalT - MaxT)); //Extrapolate
		}
			
		int startIndex = (int)globalT;
		float localT = globalT - (float)startIndex;

		PolylineSegment segment = getSegment(startIndex);
		return Vector3.Lerp(segment.p0v,segment.p1v,localT);
	}

	public override Vector3 TangentAt(float globalT)
	{
		EditorCheckNodesSafeAt(globalT,"TangentAt");

		int startIndex;

		if (Nodes.Count == 0) //No Nodes
		{
			Debug.LogError(typeof(PolylineSplineObject).Name + ": TangentAt: Trying To find the tangent with no nodes!");
			return Vector3.forward;
		}
		else if (globalT < 0f) 	//At start
			return FirstNode.Middle.forward; 
		else if (globalT > (float)(Nodes.Count - 1)) //At End
			return LastNode.Middle.forward; 
		else if (globalT == (float)(Nodes.Count - 1))
			startIndex = Nodes.Count - 2;
		else
			startIndex = (int)globalT; 

		PolylineSegment segment = getSegment(startIndex);
		return (segment.p1v - segment.p0v);
	}

	public override Quaternion RotationAt(float globalT)
	{
		EditorCheckNodesSafeAt(globalT,"RotationAt");

		if (Nodes.Count == 0)  //No Nodes
		{
			Debug.LogError(typeof(PolylineSplineObject).Name + ": RotationAt: Trying To find the rotation with no nodes!");
			return Quaternion.identity;
		}
		else if (globalT < 0f) 	//At Start
			return FirstNode.Middle.rotation;
		else if (globalT >= (float)(Nodes.Count - 1)) //At End
			return LastNode.Middle.rotation;

		int startIndex = (int)globalT;
		float localT = globalT - (float)startIndex;

		PolylineSegment segment = getSegment(startIndex);
		Vector3 tangent = (segment.p1v - segment.p0v);

		Vector3 lNodeUp = segment.p0t.up;
		Vector3 rNodeUp = segment.p1t.up;
		Vector3 lerpedUp = Vector3.Slerp(lNodeUp, rNodeUp, localT);

		return Quaternion.LookRotation(tangent, lerpedUp);
	}

	public override Vector3 ScaleAt(float globalT)
	{
		EditorCheckNodesSafeAt(globalT,"ScaleAt");

		if (Nodes.Count == 0)  //No Nodes
		{
			Debug.LogError(typeof(PolylineSplineObject).Name + ": ScaleAt: Trying To find the scale with no nodes!");
			return new Vector3(1f, 1f, 1f);
		}
		else if (globalT <= 0f) 	//At Start
				return Nodes[0].Middle.localScale;
		else if (globalT >= (float)(Nodes.Count - 1)) //At End
				return Nodes[Nodes.Count - 1].Middle.localScale;

		int startIndex = (int)globalT;
		float localT = globalT - (float)startIndex;
			
		PolylineSegment segment = getSegment(startIndex);
		Vector3 lNodeScale = segment.p0t.localScale;
		Vector3 rNodeScale = segment.p1t.localScale;
		return Vector3.Slerp(lNodeScale, rNodeScale, localT);
	}

	//---Spline Information Methods
	public override float GetArcLength()
	{
		EditorCheckNodesSafe("GetArcLength");

		float length = 0f;
		for (int i = 0; i < (Nodes.Count - 1); i++)
		{
			PolylineSegment segment = getSegment(i);
			length += (segment.p1v - segment.p0v).magnitude;
		}
		return length;
	}

	public override float GetTClosestTo(Vector3 point, int? sampleSegments = null)
	{	//Returns a global T
		EditorCheckNodesSafe("GetTClosestTo");

		float closestT = 0f;
		float smallestDistance = Mathf.Infinity;

		for (int i = 0; i < (Nodes.Count - 1); i++)
		{
			PolylineSegment segment = getSegment(i);
			float t = polylineMath.ClosestTTo(point, segment);
			Vector3 foundPoint = Vector3.Lerp(segment.p0v,segment.p1v,t);
			float foundDistance = (point - foundPoint).sqrMagnitude;

			if (foundDistance < smallestDistance)
			{
				closestT = (float)i + t;
				smallestDistance = foundDistance;
			}
		}
		return closestT;
	}

	public override bool IsWellFormed()
	{
		for (int i = 0; i < Nodes.Count; i++)
		{
			SplineNode currNode = Nodes[i];
			if (currNode == null || currNode.Middle == null)
				return false;
		}
		return true;
	}
		
	PolylineSegment getSegment(int index)
	{
		return new PolylineSegment(Nodes[index], Nodes[index + 1]);
	}

#region Editor Error Checking
	protected override void EditorCheckNodesSafeAt(float globalT, string operationName = "unknown operation")
	{
#if UNITY_EDITOR
		if (Nodes.Count == 0)
			return;

		if (globalT <= 0f) //Past Start
		{
			SplineNode firstNode = FirstNode;
			if (firstNode == null || firstNode.Middle == null)
				throw new SplineCannotEvaluateException(string.Format(NodesSafeAtErrorMessage,this.GetType().Name,operationName,globalT,"Node 0 is missing or has null transforms. Recreate it."));
			return;
		}
		else if (globalT >= MaxT) //At End
		{
			SplineNode lastNode = LastNode;
			if (lastNode == null || lastNode.Middle == null)
				throw new SplineCannotEvaluateException(string.Format(NodesSafeAtErrorMessage,this.GetType().Name,operationName,globalT,"Last Node is missing or has null transforms. Recreate it."));
			return;
		}

		int startIndex = (int)globalT;
		SplineNode n0 = Nodes[startIndex];
		if (n0 == null || n0.Middle == null)
			throw new SplineCannotEvaluateException(string.Format(NodesSafeAtErrorMessage,this.GetType().Name,operationName,globalT,"Node " + startIndex + " is missing or has null transforms. Recreate it."));
		
		SplineNode n1 = Nodes[startIndex + 1];
		if (n1 == null || n1.Middle == null)
			throw new SplineCannotEvaluateException(string.Format(NodesSafeAtErrorMessage,this.GetType().Name,operationName,globalT,"Node " + (startIndex + 1) + " is missing or has null transforms. Recreate it."));
#endif
	}
#endregion
}
}

namespace JW.Spline.Internal
{
/// <summary> Class for holding a line segment </summary>
public struct PolylineSegment
{
	SplineNode leftNode;
	SplineNode rightNode;

	public SplineNode lNode { get { return leftNode; } }

	public SplineNode rNode { get { return rightNode; } }

	public Transform p0t { get { return lNode.Middle; } }

	public Transform p1t { get { return rNode.Middle; } }

	public Vector3 p0v { get { return lNode.Middle.position; } }

	public Vector3 p1v { get { return rNode.Middle.position; } }

	public PolylineSegment(SplineNode lNode, SplineNode rNode)
	{
		this.leftNode = lNode;
		this.rightNode = rNode;
	}
}
}