﻿using UnityEngine;
using System.Collections.Generic;
using JW.Spline.Math;

namespace JW.Spline.Internal
{
[System.Serializable]
public abstract class AbstractSplineObject : ISplineObject
{
	readonly SplineMathCommon commonMath = new SplineMathCommon();

	///Spline, arranged from left (beginning) to right (end)
	public List<SplineNode> Nodes{get{return internalSplineNodes;} set{internalSplineNodes = value;}}
	List<SplineNode> internalSplineNodes = new List<SplineNode>();
	//Note: When making a editor for this, pass a serialized Node List on init. InternalSplineNodes are wiped on editor restart (Yes I've tried SerializeFeild)

	//---Node Info
	public float MaxT 				{get {return (Nodes.Count <= 0) ? 0f   : (float)(Nodes.Count - 1);}}
	public SplineNode FirstNode 	{get {return (Nodes.Count <= 0) ? null : Nodes[0];}}
	public SplineNode LastNode 		{get {return (Nodes.Count <= 0) ? null : Nodes[Nodes.Count - 1];}}

	//---Spline Evaluation Methods
	public abstract Vector3 PointAt(float t);
	public abstract Vector3 TangentAt(float t);	  		//Forward Tangent
	public abstract Quaternion RotationAt(float t);		//Forward Rotation of Spline Segment (Uses tangent + Slerped Rotation of Nodes)
	public abstract Vector3 ScaleAt(float t);		  	//Slerped Scale of Nodes

	//---Full curve methods
	public virtual float GetArcLength()				  	//Total Length of Spline
	{
		EditorCheckNodesSafe("GetArcLength"); 
		return commonMath.GetArcLength(this);
	}

	public virtual float GetTForArcLength(float arcLength, List<float> pooledArcLengthList = null)
	{
		EditorCheckNodesSafe("GetTForArcLength"); 
		return commonMath.GetTForArcLength(this,arcLength,pooledArcLengthList);
	}

	public virtual float GetTClosestTo(Vector3 point, int? sampleSegments = null)
	{
		EditorCheckNodesSafe("GetTClosestTo");

		if (sampleSegments == null)
			sampleSegments = SplineMathCommon.SAMPLE_SEGMENTS;
		
		return commonMath.GetTClosestTo(this,point, (int)sampleSegments);
	}
	
	public virtual float GetTForDistance(float t, float worldDistance) 
	{
		EditorCheckNodesSafe("GetTForDistance");
		return commonMath.GetTForDistance(this,t,worldDistance);
	} 

	public virtual bool IsWellFormed() //If the spline can be evaluated fully without error 
	{
		for (int i = 0; i < Nodes.Count; i++)
		{
			var currNode = Nodes[i];
			if (currNode == null || currNode.HasNullTransforms())
				return false;
		}
		return true;
	}

	#region Editor Error Checking
	///(Editor Only) Check if all nodes are flawless.
	protected virtual void EditorCheckNodesSafe(string operationName = "unknown")
	{
#if UNITY_EDITOR
		if (! IsWellFormed())
			throw new SplineCannotEvaluateException(this.GetType().Name + "Error during \"" + operationName + "\", Node is missing or has null transforms. Recreate it.");
#endif
	}

	protected const string NodesSafeAtErrorMessage = "{0}: {1}: Cannot evaluate at T={2}. {3}";
	///(Editor Only) Check if the nodes required to evaluate at globalT are flawless.
	protected virtual void EditorCheckNodesSafeAt(float globalT, string operationName = "unknown operation")
	{	
#if UNITY_EDITOR
		if (Nodes.Count == 0)
			return;

		if (globalT <= 0f) //Past Start
		{
			SplineNode firstNode = FirstNode;
			if (firstNode == null || firstNode.HasNullTransforms())
				throw new SplineCannotEvaluateException(string.Format(NodesSafeAtErrorMessage,this.GetType().Name, operationName,globalT,"Node 0 is missing or has null transforms. Recreate it."));
			return;
		}
		else if (globalT >= MaxT) //At End
		{
			SplineNode lastNode = LastNode;
			if (lastNode == null || lastNode.HasNullTransforms())
				throw new SplineCannotEvaluateException(string.Format(NodesSafeAtErrorMessage,this.GetType().Name,operationName,globalT,"Last Node is missing or has null transforms. Recreate it."));
			return;
		}

		int startIndex = (int)globalT;
		SplineNode n0 = Nodes[startIndex];
		if (n0 == null || n0.HasNullTransforms())
			throw new SplineCannotEvaluateException(string.Format(NodesSafeAtErrorMessage,this.GetType().Name,operationName,globalT,"Node " + startIndex + " is missing or has null transforms. Recreate it."));

		SplineNode n1 = Nodes[startIndex + 1];
		if (n1 == null || n1.HasNullTransforms())
			throw new SplineCannotEvaluateException(string.Format(NodesSafeAtErrorMessage,this.GetType().Name,operationName,globalT,"Node " + (startIndex + 1) + " is missing or has null transforms. Recreate it."));
#endif
	}
	#endregion
}
}