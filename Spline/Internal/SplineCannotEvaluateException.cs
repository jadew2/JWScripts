﻿using UnityEngine;
using System.Collections;

namespace JW.Spline.Internal
{
[System.Serializable]
public class SplineCannotEvaluateException : System.Exception
{
	public SplineCannotEvaluateException(string message) : base(message)
	{
	}
}
}
