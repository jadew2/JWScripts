﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JW.Spline
{
/// <summary> 
/// <para>
/// JWSpline2: A simpler bezier spline system.
/// Unlike splines from Invertro, this spline system focuses mostly on Bezier Splines, but still is open to multiple spline types.
/// </para>
/// 
/// <para>
/// ---Main things:
/// IJWSplineObject: Interface for all Spline Types (and Spline Objects)
/// JWAbstractSplineObject: The object holding the spline itself. Has methods to access it's data. Can be extended to handle different curves.
/// JWSplineManager (Monobehaviour): A Facade to allow you to switch between spline types, access the spline/data from other GO's, and edit the spline in viewport.
/// </para>
/// 
/// <para>
/// ---Helper Classes:
/// SplineNode: The Node Class. Contains NodeType and transforms. LTangent --> Middle --> RTangent. This class is used for all spline types.
/// JWSplineBSegment: (Struct) holds two nodes and lets you reference them by p0,p1,p2,p3.
/// JWBeizerUtils: Static bezier methods that deal with p0,p1,p2,p3 of a beizer curve and produce a result. (Lowest level of beizer stuff)
/// JWPolylineUtils: Static polyline methods that deal with p0,p1 of a line segment. Most polyline stuff is pretty simple, so this class is light.
/// </para>
/// 
/// <para>
/// ---Node Types: 
/// Smooth: Aligned Tangents, not equalized
/// Corner: Tangents not aligned or at zero, not equalized
/// Symmetric: Aligned Tangents, Equalized
/// AutoSmooth: Aligned Tangents, not nessesarily equalized but set for a round shape.
/// </para>
/// 
/// <para>
/// ---Classes For the Inspector Editor:
/// JWSplineManagerEditor: Inspector Editor. Type of Spline Manager.
/// JWSplineEditorUtils: Static Methods to help the Manager Editor Add/Remove/Modify Nodes, and control node editing in the editor.
/// JWSplineDrawingUtils: Static methods for drawing splines in the editor.
/// JWListUtils: static methods for rearranging stuff in a linked list
/// </para>
/// </summary>
/// <remarks> By: Jade White (2014), for Project Target Party </remarks>
public interface ISplineObject
{
	List<SplineNode> Nodes {get; set;}

	float MaxT {get;}
	SplineNode FirstNode {get;}
	SplineNode LastNode  {get;}

	Vector3 PointAt(float t);
	Vector3 TangentAt(float t);
	Quaternion RotationAt(float t);
	Vector3 ScaleAt(float t);

	float GetArcLength();
	float GetTForArcLength(float arcLength,  List<float> pooledArcLengthList = null);
	///Get T closest to the world point
	float GetTClosestTo(Vector3 worldPoint, int? sampleSegments = null);			
	///Get T such that it will move a point at a constant velocity (Approximite)
	float GetTForDistance(float oldT, float worldDistance); 

	///If the spline can be evaluated fully without error
	bool IsWellFormed(); 
}
	
}