﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using JW.Spline.Math;
using JW.Spline.Internal;

namespace JW.Spline
{
/// <summary>
/// A Bezier Spline. Contains methods to evaluate a bezier curve spline.
/// </summary> 
/// <remarks>
/// See IJWSpline for a full rundown of the Spline System
/// </remarks>
public class BeizerSplineObject : AbstractSplineObject
{
	readonly SplineMathBeizer beizerMath = new SplineMathBeizer();

	//---Spline Evaluation Methods
	public override Vector3 PointAt(float globalT)
	{
		EditorCheckNodesSafeAt(globalT,"PointAt");

		if (Nodes.Count == 0)  //No Nodes
		{
			Debug.LogError("JWBeizerSplineObject: PointAt: Trying To find a point with no nodes!");
			return Vector3.zero;
		}
		else if (globalT <= 0f) //At Start
		{
			SplineNode firstNode = FirstNode;
			return firstNode.Middle.position + -(firstNode.LTangent.position - firstNode.Middle.position).normalized * globalT; //Extrapolate
		}
		else if (globalT >= (float)(Nodes.Count - 1)) //At End
		{
			SplineNode lastNode = LastNode;
			return lastNode.Middle.position + (lastNode.RTangent.position - lastNode.Middle.position).normalized * (globalT - MaxT); //Extrapolate
		}
			
		int startIndex = (int)globalT;
		float localT = globalT - (float)startIndex;

		BeizerSegment curve = getSegment(startIndex);
		return beizerMath.PointAt(localT, curve);
	}

	public override Vector3 TangentAt(float globalT)
	{
		EditorCheckNodesSafeAt(globalT,"TangentAt");

		int startIndex;
		float localT;

		if (Nodes.Count == 0) //No Nodes
		{
			Debug.LogError("JWBeizerSplineObject: TangentAt: Trying To find the tangent with no nodes!");
			return Vector3.forward;
		}
		else if (globalT < 0f) 	//At start
			return -(FirstNode.LTangent.position - FirstNode.Middle.position).normalized;
		else if (globalT > (float)(Nodes.Count - 1)) //At End
			return (LastNode.RTangent.position - LastNode.Middle.position).normalized;
		else if (globalT == (float)(Nodes.Count - 1))
		{
			startIndex = Nodes.Count - 2;
			localT = 1f;
		}
		else
		{
			startIndex = (int)globalT; 
			localT = globalT - (float)startIndex;
		}
			
		BeizerSegment curve = getSegment(startIndex);
		return beizerMath.TangentAt(localT, curve);
	}

	public override Quaternion RotationAt(float globalT)
	{
		EditorCheckNodesSafeAt(globalT,"RotationAt");

		if (Nodes.Count == 0)  //No Nodes
		{
			Debug.LogError("JWBeizerSplineObject: RotationAt: Trying To find the rotation with no nodes!");
			return Quaternion.identity;
		}
		else if (globalT <= 0f) 	//At Start
				return Nodes[0].Middle.rotation;
		else if (globalT >= (float)(Nodes.Count - 1)) //At End
				return Nodes[Nodes.Count - 1].Middle.rotation;

		int startIndex = (int)globalT;
		float localT = globalT - (float)startIndex;

		BeizerSegment curve = getSegment(startIndex);
		Vector3 bezierTangent = beizerMath.TangentAt(localT, curve);

		Vector3 lNodeUp = curve.p0t.up;
		Vector3 rNodeUp = curve.p3t.up;
		Vector3 lerpedUp = Vector3.Slerp(lNodeUp, rNodeUp, localT);
			
		return Quaternion.LookRotation(bezierTangent, lerpedUp);
	}

	public override Vector3 ScaleAt(float globalT)
	{
		EditorCheckNodesSafeAt(globalT,"ScaleAt");

		if (Nodes.Count == 0)  //No Nodes
		{
			Debug.LogError("JWBeizerSplineObject: ScaleAt: Trying To find the scale with no nodes!");
			return new Vector3(1f, 1f, 1f);
		}
		else if (globalT <= 0f) 	//At Start
				return Nodes[0].Middle.localScale;
		else if (globalT >= (float)(Nodes.Count - 1)) //At End
				return Nodes[Nodes.Count - 1].Middle.localScale;

		int startIndex = (int)globalT;
		float localT = globalT - (float)startIndex;
			
		BeizerSegment curve = getSegment(startIndex);
		Vector3 lNodeScale = curve.p0t.localScale;
		Vector3 rNodeScale = curve.p3t.localScale;
		return Vector3.Slerp(lNodeScale, rNodeScale, localT);
	}

	public override bool IsWellFormed()
	{
		for (int i = 0; i < Nodes.Count; i++)
		{
			SplineNode currNode = Nodes[i];
			if (currNode == null || currNode.HasNullTransforms())
				return false;
		}
		return true;
	}

	//---Spline Information Methods
	BeizerSegment getSegment(int index)
	{
		return new BeizerSegment(Nodes[index], Nodes[index + 1]);
	}
		
}
}
namespace JW.Spline.Internal
{
/// <summary> A segment in a bezier spline. Consists of two nodes. Allows you to access the 4 points or transforms to calculate a beizer point. </summary>
[System.Serializable]
public struct BeizerSegment
{
	SplineNode leftNode;
	SplineNode rightNode;

	public SplineNode lNode { get { return leftNode; } }

	public SplineNode rNode { get { return rightNode; } }

	///Left Middle
	public Transform p0t { get { return lNode.Middle; } }
	///Left rTangent
	public Transform p1t { get { return lNode.RTangent; } }
	///Right lTangent
	public Transform p2t { get { return rNode.LTangent; } }
	///Right Middle
	public Transform p3t { get { return rNode.Middle; } }
		
	///Left Middle
	public Vector3 p0v { get { return lNode.Middle.position; } }
	///Left rTangent
	public Vector3 p1v { get { return lNode.RTangent.position; } }
	///Right lTangent
	public Vector3 p2v { get { return rNode.LTangent.position; } }
	///Right Middle
	public Vector3 p3v { get { return rNode.Middle.position; } }
		
	public BeizerSegment(SplineNode lNode, SplineNode rNode)
	{
		this.leftNode = lNode;
		this.rightNode = rNode;
	}
		
}	
}