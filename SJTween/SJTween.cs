using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using JW.Tween.Internal;
namespace JW.Tween
{
/// <summary> 
/// Main factory class of SJTween, used for creating tweens.
/// </summary>
/// <description>
/// SJTween: A fast generic tweener developed by Jade White and Shane Beck (2014).
/// Version: ~2.0
/// It supports Playing, Pausing, Stopping, Reversing, Looping, and Sequences.
/// 
/// <para>
///  ---To use:--- 
/// Like every Tweener out there, the purpose behind SJTween is to interpolate values over a duration.
/// There are only 3 main methods you need to worry about.
/// <list type="bullet">
/// 	<listheader>
/// 		<term>Method</term>
/// 		<description>Description</description>
/// 	</listheader>
/// 	<item> 
/// 		<term> SJTween.Value(T start,T end,duration,(T start,T end,float t) => {}) </term> 
/// 		<description> Tweens a value for (duration), from t = 0 to 1, and performs callback. Plays automatically. </description> 
/// 	</item>
/// 	<item> 
/// 		<term> SJTween.Value(T start,T end,duration,(T result) => {},T lerpMethod) </term>
/// 		<description> Tweens a value for (duration), returns the result. The Lerp method is required for this. Plays automatically. </description> 
/// 	</item>
/// 	<item>
/// 		<term>  SJTween.Sequence(params SJIComponents) </term>
/// 		<description> Creates a sequence with the ordered components. Plays automatically. </description>
/// 	</item>
/// </list>
/// 
/// See ITween for chained methods, these enable you to change easing, use realtime, loop etc.
/// </para>
/// <para>
/// ---Some Important Classes--- 
/// <list type="bullet">
/// 	<item> SJIComponent: Interface all tweens/sequences/etc. adhere to.</item>
/// 	<item> SJAbstractComponent: Partial implementation of this. </item>
/// 	<item> SJSingle: Single Tween</item>
/// 	<item> SJSequence: Sequences of SJIComponents</item>
/// 	<item> SJEase: Holds easing equations.</item>
/// 	<item> SJTweenEvaluator: Handles Evaluation + Changing GO name.</item>
/// </list>
/// </para>
/// </description>
public static class SJTween
{
	public static ITween Empty { get { return new EmptyTween(); } }
	
	public static ITween Value<T>(Func<T> from, Func<T> to, float duration, Func<T,T,float,T> lerpMethod, Action<T> callback) //Result Callback allowing changing start and end points.
	{return new ValueTween<T>(from,to,duration, (a, b, t) => callback(lerpMethod(a, b, t)));}

	public static ITween Value<T>(T from, T to, float duration, Func<T,T,float,T> lerpMethod, Action<T> callback) //Result Callback
	{return new ValueTween<T>(from,to,duration, (a, b, t) => callback(lerpMethod(a, b, t)));}
	
	public static ITween Value<T>(T from, T to, float duration, Action<T,T,float> callback) //Normal Callback
	{return new ValueTween<T>(from,to,duration,callback);}
	
	public static ITween Sequence (params ITween[] sequence) //Sequence
	{return new SequenceTween(sequence);}

	public static ITween Collection (params ITween[] collection)
	{return new CollectionTween(collection);}

	/// <summary> Waits for "delay" and invokes "callback" when finished. </summary>
	public static ITween Invoke(float delay, Action callback)
	{
		ITween invokeTween = Value(0f, 1f, delay, Mathf.Lerp, result => {});
		invokeTween.OnEdge += callback;
		return invokeTween;
	}
		
	/// <summary> Waits for "startDelay" and invokes callback, then waits for "repeatDelay" and invokes callback in a loop. Warning, Untested. </summary>
	public static ITween InvokeRepeating(float startDelay, float repeatDelay, Action callback) 
	{			
		ITween startDelayTween = SJTween.Value(0f, 1f, startDelay, Mathf.Lerp, result => {});
		startDelayTween.OnEdge += callback;
		ITween repeatDelayTween = SJTween.Value(0f, 1f, repeatDelay,Mathf.Lerp,result => {}).SetLoopType(TweenLoop.Repeat, -1);
		repeatDelayTween.OnEdge += callback;

		return startDelayTween.Append(repeatDelayTween);
	}

	/// <summary>
	/// When "when" returns true, invokes "execute".
	/// </summary>
	public static ITween InvokeWhen(Func<bool> when, Action execute)
	{
		return InvokeWhen(when, result => execute(), Mathf.Infinity);
	}

	/// <summary>
	/// When "when" returns true, invokes "execute". Has a timeout parameter.
	/// If there is a timeout, "execute" is called with false. Otherwise true.
	/// </summary>
	public static ITween InvokeWhen(Func<bool> when, Action<bool> execute, float timeout)
	{
		bool done = false;
		ITween invokeTween = null;
		invokeTween = Value(0f,1f,timeout,Mathf.Lerp,x => 
		{
				if (!done && when())
				{
					done = true;
					execute(true);
					invokeTween.Stop();
				}
		});
		invokeTween.OnEdge += () =>
		{
				if (!done)
				{
					done = true;
					execute(false);
					invokeTween.Stop();
				}
		};
		return invokeTween;
	}


	#region Methods For more specific types
	public static ITween Value(float from, float to, float duration, Action<float> callback)
	{
		return Value<float>(from,to,duration,Mathf.Lerp,callback);
	}

	public static ITween Value(Vector2 from, Vector2 to, float duration, Action<Vector2> callback)
	{
		return Value<Vector2>(from,to,duration,Vector2.Lerp,callback);
	}

	public static ITween Value(Vector3 from, Vector3 to, float duration, Action<Vector3> callback)
	{
		return Value<Vector3>(from,to,duration,Vector3.Lerp,callback);
	}

	public static ITween Value(Quaternion from, Quaternion to, float duration, Action<Quaternion> callback)
	{
		return Value<Quaternion>(from,to,duration,Quaternion.Lerp,callback);
	}

	public static ITween Value(Color from, Color to, float duration, Action<Color> callback)
	{
		return Value<Color>(from,to,duration,Color.Lerp,callback);
	}
	#endregion
}
}
