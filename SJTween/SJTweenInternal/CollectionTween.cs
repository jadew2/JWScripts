﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using JW.Tween;

using System.Linq;
using MoreLinq;
using JW.Standard;

namespace JW.Tween.Internal
{

/// <summary>
/// A class which holds a list of tweens temporarily. 
/// Useful for when you want to execute commands on a collection of tweens.
/// </summary>
/// <remarks> 
/// Collections state parameter changes depending on their child tweens. 
/// </remarks>
public class CollectionTween : AbstractTween 
{
	/// <summary> The tweens in this collection. Operations are applied on all of these. </summary>
	public readonly ObservableList<ITween> Tweens = new ObservableList<ITween>();

	int playingTweens = 0;

	/// <summary> Creates a new tween collection. Does not change the state of the tweens. </summary>
	public CollectionTween(params ITween[] tweens)
	{
		Tweens.OnListChanged += ListChangedArgs =>
		{
			foreach (ITween tween in ListChangedArgs.AddedItems)
			{
				if (tween.State == TweenState.Playing)
					increasePlayingTweens();

				tween.OnPlay += increasePlayingTweens;
				tween.OnStop += decreasePlayingTweens;
			}

			foreach (ITween tween in ListChangedArgs.RemovedItems)
			{
				if (tween.State == TweenState.Playing)
					decreasePlayingTweens();

				tween.OnPlay -= increasePlayingTweens;
				tween.OnStop -= decreasePlayingTweens;
			}
		};
		
		Tweens.AddRange(tweens);
	}

	void increasePlayingTweens()
	{
		if (playingTweens == 0)
			State = TweenState.Playing;

		playingTweens++;
	}

	void decreasePlayingTweens()
	{
		if (playingTweens == 1)
			State = TweenState.Stopped;

		playingTweens--;
		playingTweens = Mathf.Max(0, playingTweens);
	}

	public override CollectionTween Add(params ITween[] components)
	{
		Tweens.AddRange(components);
		return this;
	}

	public virtual CollectionTween Remove(params ITween[] components)
	{
		foreach (ITween component in components)
			Tweens.Remove(component);
		return this;
	}

	/// <summary>
	/// Returns the max elapsed time of the Tweens in seconds.
	/// When set, it sets the elapsed time of all the Tweens.
	/// </summary>
	public override float ElapsedTime 
	{
		get {return Tweens.Any() ? Tweens.MaxBy(x => x.ElapsedTime).ElapsedTime : 0f;} 
		set
            {
                foreach (var tween in Tweens)
                    tween.ElapsedTime = value;
            }
	}

	/// <summary> 
	/// Returns the max duration of the all the tweens in seconds. 
	/// When set, it sets all tween durations to the passed value. 
	/// </summary>
	public override float Duration 
	{
		get {return Tweens.Any() ? Tweens.MaxBy(x => x.Duration).Duration : 0f;} 
		set
            {
                foreach (var tween in Tweens)
                    tween.Duration = value;
            }
	}

	/// <summary> Scales all Tween durations so the new max duration is the passed value. </summary>
	public ITween ScaleTo(float newDuration)
	{
		//Scale the max duration of all the tweens to the passed value. 
		float multiplier = newDuration / Duration; //Calling get{}
		for (int i = 0; i < Tweens.Count; i++)
			Tweens[i].Duration *= multiplier;
		return this;
	}

	/// <summary>
	/// Returns the max last "t" value (0f --> 1f) out of the tweens.
	/// When set, it sets the LastEasedT of all the Tweens. 
	/// </summary>
	public override float LastEasedt 
	{
		get {return Tweens.Any() ? Tweens.MaxBy(x => x.LastEasedt).LastEasedt : 0f;}
		set
            {
                foreach (var tween in Tweens)
                    tween.LastEasedt = value;
            }
	}

	/// <summary> 
	/// Evaluates. Note this does nothing for collections, the tween's evaluation is seperate for technical reasons. 
	/// Do a simple foreach loop on Tweens if you want to force a update. 
	/// </summary>
	public override ITween Evaluate (float deltaTime)
	{
		//Do nothing. The tweens evaluate themselves.
		//This method is called by the evaluator, so adding evaluation here would double the speed.
		return this;
	}

	#region Chained methods
	/// <summary>  Starts playing all tweens. </summary>
	public override ITween Play ()
	{
		base.Play();
        foreach (var tween in Tweens)
            tween.Play();
		return this;
	}

	/// <summary> Pauses all tweens. </summary>
	public override ITween Pause ()
	{
		base.Pause();
        foreach (var tween in Tweens) { tween.Pause(); }
        return this;
	}

	/// <summary> Stops all tweens. </summary>
	public override ITween Stop ()
	{
		base.Stop();
		//We copy the tweens because stopping tweens removes it from Tweens.
		//Issues with IEnumerable
		List<ITween> copiedTweens = Tweens.ToList();
		foreach (var tween in copiedTweens) { tween.Stop(); }

		return this;
	}

	/// <summary> Rewinds all tweens. </summary>
	public override ITween Rewind ()
	{
        foreach (var tween in Tweens) { tween.Rewind(); }
		return this;
	}

	/// <summary> Reverses all tweens. Instead of A --> B, goes B --> A. The current evaluation time is preseved. </summary>
	public override ITween Reverse ()
	{
        foreach (var tween in Tweens) { tween.Reverse(); }
		return this;
	}

	/// <summary> Sets the type of the loop for all tweens. </summary>
	public override ITween SetLoopType (TweenLoop loopType, int loopCount)
	{
		base.SetLoopType(loopType, loopCount);
        foreach (var tween in Tweens) { tween.SetLoopType(loopType, loopCount); }
		return this;
	}

	/// <summary> Sets the type of the ease for all tweens. </summary>
	public override ITween SetEaseType (Func<float, float> easeType)
	{
		base.SetEaseType(easeType);
        foreach (var tween in Tweens) { tween.SetEaseType(easeType); }
		return this;
	}

	/// <summary> Sets the type of the update for all tweens. </summary>
	public override ITween SetUpdateType (TweenUpdate updateType)
	{
		base.SetUpdateType(updateType);
        foreach (var tween in Tweens) { tween.SetUpdateType(updateType); }
		return this;
	}

	/// <summary> Sets the persistance of all tweens. </summary>
	public override ITween SetPersistant (bool val)
	{
		base.SetPersistant(val);
        foreach (var tween in Tweens) { tween.SetPersistant(val); }
		return this;
	}

	#endregion


}
}