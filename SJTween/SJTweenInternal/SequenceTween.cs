using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

using JW.Tween.Internal;

namespace JW.Tween
{

/// <summary>
/// Component responsible for managing tweens in a sequence
/// </summary>
/// <remarks>
/// By: Jade White 2014
/// 
///  ---Implementation notes---
/// Uses a list for tweens
///  
/// Evaluate() only does anything when the activeComponent (the current acting tween) is set to SJTweenState.Kill.
/// In other words, the sequence decides to advance if the current tween kills itself (finishes). 
/// Evaluate() currently doesn't call Evaluate() on the activeComponent. This is keep the activeComonent.State consistent.
/// </remarks>
public class SequenceTween : AbstractTween
{
	readonly List<ITween> sequence = new List<ITween>();
	ITween activeComponent = SJTween.Empty; //Current evaluated tween reference
	int activeTweenIndex = 0;		//Current evaluated tween
	
	//Normal
	public SequenceTween(params ITween[] components)
	{
		if (components == null || components.Length == 0)
			throw new ArgumentException(typeof(SequenceTween).Name + ": You cannot create a Sequence tween with no tweens.");

		this.IsReversed = false;
		this.activeTweenIndex = 0;

		sequence.AddRange(components);
		sequence.ForEach(component => component.Stop()); //Start off stopped.

		//Handle errors and cases to do with differing evaluators.
		handleDifferentPersistance(sequence);
	}

	/// <summary> Makes the activeComponent and index be the one at index, and starts playing it. </summary>
	void switchActiveTweenTo(int index)
	{
		activeComponent.Stop();
		activeTweenIndex = index;
		activeComponent = sequence[activeTweenIndex];
		activeComponent.Rewind();
		activeComponent.Play();
	}

	///<summary>  Evaluates this sequence. </summary>
	public override ITween Evaluate (float deltaTime)
	{
		if (State == TweenState.Created)
		{
			State = TweenState.Playing;

			switchActiveTweenTo(0); //We can run this because sequences have at least one tween.
		}
		
		if (this.State == TweenState.Playing)
		{
			if (activeComponent.State == TweenState.Stopped)
			{ 
				//The current tween is stopped, it's time to move to the next one.
				int nextIndex = currIsReversed ? activeTweenIndex - 1 : activeTweenIndex + 1;

				//First, check if we are at a endpoint.
				int endpointCode = -1; //-1 = No endpoint, 0 = < 0 end, 1 = > 1 end (positive end)
				if (nextIndex < 0)
					endpointCode = 0;
				else if (nextIndex > sequence.Count-1)
					endpointCode = 1;

				if (endpointCode != -1) //If we are at a endpoint... 
				{
					bool atPositiveEnd = endpointCode == 1;
					if (handleLoopingAtEnds(atPositiveEnd)) //handleLoopingAtEnds returns true if the tween should stop.
					{
						State = TweenState.Stopped;
						return this;
					}
				}
				else //If we're not at a endpoint...
				{
					switchActiveTweenTo(nextIndex);
				}
			}
		}
		else if (State == TweenState.Paused)
		{}
		else if (State == TweenState.Stopped)
		{}

		return this;
	}

	private bool handleLoopingAtEnds(bool atPositiveEnd)
	{//Only called if we are at the end of the tween
		//Normal end. (1f)
		//Reversed end. (0f)

		base.CallOnEdge();

		if (LoopType == TweenLoop.Repeat)
		{
			if (CurrLoopCount == 0) //Same regardless of tween "end"
				return true; //We should stop.
			else //We can keep going
			{//Rewind, decrement currloop
				switchActiveTweenTo( (this.currIsReversed) ? (sequence.Count - 1) : 0 ); //We can't use Rewind(), because it reverts currloop

				if (CurrLoopCount > -1)
					CurrLoopCount--;
				return false;
			}
		}
		else if (LoopType == TweenLoop.PingPong)
		{//Only decrement counter when you reach the start again (or if reversed, the end)
			bool atPong = (IsReversed) ? !atPositiveEnd : atPositiveEnd; 
			if (atPong)
			{
				this.currIsReversed = (!currIsReversed); //Switch Direction
				int i = 0;
				for (i = 0; i < sequence.Count; i++)
					sequence[i].Reverse();
				activeComponent.Play();
				return false;
			}
			else //At Ping (Start)
			{
				if (CurrLoopCount == 0)
					return true; //Stop
				else
				{
					this.currIsReversed = (!currIsReversed); //Switch Direction
					int i = 0;
					for (i = 0; i < sequence.Count; i++)
						sequence[i].Reverse();
					activeComponent.Play();

					if (CurrLoopCount > -1)
						CurrLoopCount--;
					return false;
				}
			}
		}

		else
			throw new NotImplementedException();
	}

	public override ITween Play ()
	{
		base.Play();
		activeComponent.Play();
		return this;
	}
	
	public override ITween Pause ()
	{
		base.Pause();
		activeComponent.Pause();
		return this;
	}
	
	public override ITween Stop ()
	{
		base.Stop();
		activeComponent.Stop();
		return this;
	}

	public override ITween Rewind ()
	{
		switchActiveTweenTo((this.IsReversed) ? (sequence.Count-1) : 0);
		currIsReversed = this.IsReversed;
		this.CurrLoopCount = this.LoopCount;
		return this;
	}

	public override ITween Reverse ()
	{
		for (int i = 0; i < sequence.Count; i++)
			sequence[i].Reverse();
		this.IsReversed = (!this.IsReversed);
		currIsReversed = this.IsReversed;
		return this;
	}

	public override SequenceTween Append (params ITween[] components)
	{
		sequence.AddRange(components);
		return this;
	}

	public override SequenceTween Prepend (params ITween[] components)
	{
		sequence.InsertRange(0, components);
		return this;
	}

	public override ITween SetEaseType (Func<float, float> easeType)
	{
		for (int i = 0; i < sequence.Count; i++)
			sequence[i].SetEaseType(easeType);
		return this;
	}

	public override ITween SetPersistant(bool val)
	{
		for (int i = 0; i < sequence.Count; i++)
			sequence[i].SetPersistant(val);
		base.SetPersistant(val); //Set our own persistance
		return this;
	}

	public override float ElapsedTime {
		get
		{//Sum of all previous Durations + Elapsed time of current tween
			float sum = 0f;
			for (int i = 0; i < activeTweenIndex; i++)
				sum += sequence[i].Duration;
			return sum + activeComponent.ElapsedTime;
		}
		set
		{//Set activeTweenIndex based off the Elapsed Time
			if (value >= Duration) //Larger than Duration
				switchActiveTweenTo(sequence.Count - 1);
			else if (value < 0f) //Smaller than 0
				switchActiveTweenTo(0);
			else
			{
				float sum = 0f;
				for (int i = 0; (i < sequence.Count) && (value >= sum); i++)
				{//Add durations
					float duration = sequence[i].Duration;
					sum += duration;
					if (value < sum)
					{
						switchActiveTweenTo(i);
						activeComponent.ElapsedTime = value - sum + duration;
					}
				}
			}
			return;
		}
	}

	public override float Duration {
		get
		{//Sum of all the durations in the sequence
			float sum = 0f;
			for (int i = 0; i < sequence.Count; i++)
				sum += sequence[i].Duration;
			return sum;
		}
		set
		{//Set duration by streching or squishing all the tweens
			float ratio = value/Duration; //newDuration/Duration
			for (int i = 0; i < sequence.Count; i++)
				 sequence[i].Duration = ratio * sequence[i].Duration;
		}
	}
	public override float LastEasedt {
		get{return activeComponent.LastEasedt;}
		set{activeComponent.LastEasedt = value;}
	}
	
	public override string ToString ()
	{
		return string.Format ("SJSequence: Duration={0}, LastEasedt={1}, ElapsedTime={2}", Duration,LastEasedt,ElapsedTime);
	}


	/// <summary> 
	/// Ensures all tweens in a created sequence are using the same evaluator. 
	/// If they aren't, throws a error and sets them all to use the default evaluator.
	/// 
	/// If they are, makes this sequence match it.
	/// </summary>
	void handleDifferentPersistance(List<ITween> tweens)
	{
		if (tweens == null || tweens.Count == 0)
			return;

		if (!allPersistanceTheSame(tweens))
		{
			Debug.LogError(typeof(SequenceTween).Name + ": One of the tweens in the new sequence has a evaluator not like the others. This is not allowed. For now, I'm setting them to all use the default evaluator.");
			this.SetPersistant(false);
		}
		else
		{
			//Match their persistance
			base.SetPersistant(tweens[0].Persistant);
		}
	}

	/// <summary> Returns true if all the evaluators are the same </summary>
	bool allPersistanceTheSame(List<ITween> tweens)
	{
		if (tweens == null || tweens.Count == 0)
			return true;

		bool firstPersistance = tweens[0].Persistant;
		for (int i = 1; i < tweens.Count; i++)
		{
			ITween currTween = tweens[i];
			if (firstPersistance != currTween.Persistant)
				return false;
		}

		return true;
	}
}
}
