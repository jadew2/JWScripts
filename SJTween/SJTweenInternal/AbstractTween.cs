using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace JW.Tween.Internal
{
public abstract class AbstractTween : ITween
{
	public event Action OnPlay = () => {};
	public event Action OnPause = () => {};
	public event Action OnStop = () => {};
	public event Action OnEdge = () => {};

	/// <summary> Helper class that adds/removes this tween to/from the evaluator. </summary>
	protected readonly TweenEvaluatorHelper EvaluatorHelper = null; 

	protected AbstractTween() //Constructor
	{
		EvaluatorHelper = new TweenEvaluatorHelper(this);
		EvaluatorHelper.AddToEvaluator(); //Add ourselves to the default evaluator
	}
		
	#region Properties
	bool isReversed = false;
	public bool IsReversed //Reversed value to revert to
	{
		get{return isReversed;}
		set{isReversed = value;}
	} 
	protected bool currIsReversed = false; //Used during evaluation (Important for reversed PingPong)

	TweenLoop loopType = TweenLoop.Repeat;
	public TweenLoop LoopType 
	{
		get{return loopType;} 
		set{loopType = value;}
	}

	int loopCount = 0; //Loop count to rewind to. 
	public int LoopCount 
	{
		get {return loopCount;} 
		set 
		{
			loopCount = value; 
			CurrLoopCount = value;
		}
	}
	protected int CurrLoopCount = 0;

	Func<float,float> easeType = TweenEase.linear;
	public Func<float,float> EaseType 
	{
		get{return easeType;} 
		set{easeType = value;}
	}

	TweenUpdate updateType = TweenUpdate.Update;
	public TweenUpdate UpdateType 
	{
		get{return updateType;} 
		set
		{
			if (updateType == value)
				return;
			EvaluatorHelper.ChangeUpdateType(updateType, value);
			updateType = value;
		}
	}

	bool persistant = false;
	public bool Persistant 
	{
		get {return persistant;} 
		set
		{
			if (persistant == value)
				return;
			EvaluatorHelper.ChangePersistance(value);
			persistant = value;
		}
	}


	protected TweenState state = TweenState.Created;
	public TweenState State {get {return state;}
		set
		{
			if (value == TweenState.Created)
				throw new InvalidOperationException(typeof(AbstractTween).Name + ": You cannot set the tween state to Created from outside.");

			if (state == value)
				return; //No Change

			if (value == TweenState.Playing)
			{
				InvokeAndIgnoreExceptions(OnPlay);
				EvaluatorHelper.AddToEvaluator();
			}
			else if (value == TweenState.Paused)
			{
				InvokeAndIgnoreExceptions(OnPause);
			}
			else if (value == TweenState.Stopped) //Set when we reached the end of this tween.
			{
				EvaluatorHelper.RemoveFromEvaluator();
				InvokeAndIgnoreExceptions(OnStop);
			}
			state = value;
		}
	}

	/// <summary> This simply invokes OnEdge </summary>
	protected virtual void CallOnEdge()
	{
		InvokeAndIgnoreExceptions(OnEdge);
	}

	public virtual float ElapsedTime 
	{
		get {throw new NotImplementedException ();}
		set {throw new NotImplementedException ();}
	}
	
	public virtual float Duration 
	{
		get {throw new NotImplementedException ();}
		set {throw new NotImplementedException ();}
	}
	
	public virtual float LastEasedt 
	{
		get {throw new NotImplementedException ();}
		set {throw new NotImplementedException ();}
	}

	#endregion

	public virtual ITween Evaluate(float deltaTime)
	{
		throw new NotImplementedException ();
	}

	/// <summary> Sets LastEasedT and ElapsedTime for the passed t </summary>
	protected virtual void SetEvaluatedT(float t)
	{
		LastEasedt = t;
		ElapsedTime = t * Duration;
	}

	#region Chained methods
	public virtual ITween Play ()
	{
		this.State = TweenState.Playing;
		return this;
	}
	
	public virtual ITween Pause ()
	{
		this.State = TweenState.Paused;
		return this;
	}
	
	public virtual ITween Stop ()
	{
		this.State = TweenState.Stopped;
		return this;
	}
	
	public virtual ITween Rewind ()
	{
		
		this.ElapsedTime = (this.IsReversed) ? this.Duration : 0f;
		currIsReversed = this.IsReversed;
		this.CurrLoopCount = this.LoopCount;
		return this;
	}
	
	public virtual ITween Reverse ()
	{
		this.IsReversed = (!this.IsReversed); //Toggle
		currIsReversed = this.IsReversed;
		return this;
	}
	
	public virtual SequenceTween Append (params ITween[] components)
	{
		return new SequenceTween(new[]{this}.Concat(components).ToArray());
	}
	
	public virtual SequenceTween Prepend (params ITween[] components)
	{
		return new SequenceTween(components.Concat(new[]{this}).ToArray());
	}

	public virtual CollectionTween Add(params ITween[] components)
	{
		return new CollectionTween(new[]{this}.Concat(components).ToArray());
	}

	public virtual ITween SetLoopType (TweenLoop loopType, int loopCount)
	{
		this.LoopType = loopType;
		this.LoopCount = loopCount; //If set to -1, the loops should be infinite
		return this;
	}

	public virtual ITween SetEaseType (Func<float, float> easeType)
	{
		this.EaseType = easeType;
		return this;
	}

	public virtual ITween SetUpdateType (TweenUpdate updateType)
	{
		this.UpdateType = updateType;
		return this;
	}

	public virtual ITween SetPersistant (bool val)
	{
		this.Persistant = val;
		return this;
	}
	#endregion

	/// <summary> Simply runs the passed action, printing any errors to the console. </summary>
	protected void InvokeAndIgnoreExceptions(Action exec)
	{
		try
		{
			exec();
		}
		catch (Exception e)
		{
			Debug.LogException(e);
		}
	}

}
}
