using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using JW.Tween.Internal;
namespace JW.Tween
{
/// <summary>
/// Class that holds specific implementation for a single tween.
/// A single tween is like, a float, a vector3 etc.
/// </summary>
/// <remarks> By: Jade White 2014 for Project Target Party</remarks>
public class ValueTween<T> : AbstractTween
{
	public Func<T> Start { get; set;} //Stored as Functions, allows them to change over time.
	public Func<T> End { get; set;}

	public Action<T,T,float> Callback { get; set;}

	public override float ElapsedTime { get; set;}

	public override float Duration { get; set;}

	public override float LastEasedt { get; set;}

	public ValueTween() : this(default(T),default(T),0f,(start,end,t) =>{}){}
	public ValueTween(T start, T end, float duration, Action<T,T,float> callback) : this(()=> start, () => end, duration,callback){}
	public ValueTween(Func<T> start, Func<T> end, float duration, Action<T,T,float> callback)
	{
		Start = start;
		End = end;

		ElapsedTime = 0f;
		Duration = duration;

		LastEasedt = 0f;

		Callback = callback;
	}

	//--Chained Methods

	/// <summary> Invokes this tween's evaluation update. This is also called by the evaluator behind the scenes. </summary>
	/// <remarks> 
	/// When a tween is created, it is not evaluated until the next frame. 
	/// The first call to Evaluate calls Callback(0f) and Callback(deltaTime/Duration) in the same frame.
	/// After that, Callbacks are once per frame.
	/// If the tween reaches a end (Such as T=0 or T=1), Callback(0f) or Callback(1f) is called. This way, you can be sure the value is 0 or 1f.
	/// 	This is paticularily useful for loopable tweens, as it ensures the tween is 100% finished. Regardless of deltaTime fluctuations.
	/// </remarks>
	public override ITween Evaluate (float deltaTime)
	{
		//Handle the first frame.
		if (State == TweenState.Created)
		{
			State = TweenState.Playing;

			ElapsedTime = 0f;
			LastEasedt = EaseType(0f);
			Callback(Start(), End(), LastEasedt);

			//return this; //De comment this line if you want a one frame delay.
		}

		if (State == TweenState.Playing)
		{
			//If this is a infinite tween...
			if (Duration < 0)
			{	
				ElapsedTime = 0f;
				LastEasedt = EaseType(0f);
				Callback(Start(), End(), LastEasedt);
				return this;
			}

			//First Apply Time Changes		
			ElapsedTime += currIsReversed ? -deltaTime : deltaTime; 

			//Now, check if the elapsed time is at a endpoint.
			int endpointCode = -1; //-1 = No endpoint, 0 = < 0 end, 1 = > 1 end (positive end)
			if (ElapsedTime <= 0f)
				endpointCode = 0;
			else if (ElapsedTime >= Duration)
				endpointCode = 1;

			bool safeToDeclareEndpoint = Math.Abs(deltaTime) > float.Epsilon; //Check that deltaTime isn't 0. Otherwise, we would rapid-fire a endpoint.
			if (endpointCode != -1 && safeToDeclareEndpoint) //If we are at a endpoint... 
			{
				ElapsedTime = endpointCode * Duration;
				LastEasedt = EaseType(endpointCode);
				Callback(Start(), End(), LastEasedt);

				bool atPositiveEnd = endpointCode == 1;
				if (handleLoopingAtEnds(atPositiveEnd)) //handleLoopingAtEnds returns true if the tween should stop.
					State = TweenState.Stopped;
				
				return this;
			}

			//Otherwise, handle easing like normal
			LastEasedt = EaseType(ElapsedTime/Duration);
			Callback(Start(), End(), LastEasedt);

			return this;
		}
		else if (State == TweenState.Paused)
			return this;
		else if (State == TweenState.Stopped)
			return this; //Killing is handled within the Params themselves

		return this;
	}

	///<summary> Handle Loops: Handles logic behind looping. Returns bool "kill". If this returns true, we can't loop anymore. </summary>
	bool handleLoopingAtEnds(bool atPositiveEnd)
	{//Only called if we are at the end of the tween
		//Normal end. (1f), elapsedTime >= duration
	 //Reversed end. (0f), elapsedTime <= 0f

		base.CallOnEdge();

		if (LoopType == TweenLoop.Repeat)
		{
			if (CurrLoopCount == 0) //Same regardless of tween "end"
				return true; //We should stop.
			else //We can keep going
			{//Rewind, decrement currloop
				this.ElapsedTime = (currIsReversed) ? this.Duration : 0f; //We can't use Rewind(), because it reverts currloop

				if (CurrLoopCount > -1)
					CurrLoopCount--;
				return false;
			}
		}
		else if (LoopType == TweenLoop.PingPong)
		{//Only decrement counter when you reach the start again (or if reversed, the end)
			bool atPong = (IsReversed) ? !atPositiveEnd : atPositiveEnd; 
			if (atPong)
			{
				this.currIsReversed = (!currIsReversed); //Switch Direction
				this.ElapsedTime = (currIsReversed) ? this.Duration : 0f; //Clamp the elapsed time
				return false;
			}
			else //At Ping (Start)
			{
				if (CurrLoopCount == 0)
					return true; //Stop
				else
				{
					this.currIsReversed = (!currIsReversed); //Switch Direction
					this.ElapsedTime = (currIsReversed) ? this.Duration : 0f; //Clamp the elapsed time

					if (CurrLoopCount > -1)
						CurrLoopCount--;
					return false;
				}
			}
		}
		/*else if (LoopType == SJTweenLoop.Incremental)
		{
			if (currLoopCount <= 0) //Same regardless of tween "end"
				return true; //We should stop.
			else
			{
				T newEnd = End + (End - Start);
				Start = End;
				End = (T)newEnd;

				this.elapsedTime = (this.IsReversed) ? this.duration : 0f; //We can't use Rewind(), because it reverts currloop
				currLoopCount--;
				return false;
			}
		}*/
		else
			throw new NotImplementedException();
	}

	public override string ToString ()
	{
		return string.Format ("SJTweenSingle: Start {0}, End {1}, Duration {2}, Callback {3}, LastEasedt {4}, ElapsedTime {5}", Start(),End(),Duration,Callback,LastEasedt,ElapsedTime);
	}
}
}
