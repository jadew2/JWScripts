﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using JW.Tween.Internal;

namespace JW.Tween
{
/// <summary> A enum for the state of a tween. </summary>
public enum TweenState {
	Created, 	//Tween has been created and added to the evaluator.
	Playing, 	//Add tween to Evaluator (if needed) and play.
	Paused, 	//Pause tween
	Stopped 	//Remove tween from Evaluator (if needed) and stop.
}

/// <summary>  The loop method of a tween. </summary>
public enum TweenLoop {
	Repeat, 		//x to y; count--; x to y; count--...
	PingPong,   	//x to y; y to x; count--;; x to y...
	//Incremental 	//x to y; count--; y to y+(y-x); <-- If possible
}

/// <summary> The update method the tween is evaluated in. </summary>
public enum TweenUpdate {
	/// <summary> Tween is evaluated in update using deltaTime. </summary>
	Update,	
	/// <summary> Tween is evaluated in late update using deltaTime. </summary>
	LateUpdate,	
	/// <summary> Tween is Evaluated in FixedUpdate useing fixedDeltaTime. </summary>
	FixedUpdate, 		
	/// <summary> Tween is Evaluated in Update using unscaledDeltaTime. </summary>
	RealTime,			
}


/// <summary> Interface for all tweens to have. </summary>
public interface ITween
{
	///<summary> Called when playing or resuming from pause </summary>
	event Action OnPlay;
	///<summary> Called when tween switches to a paused state </summary>
	event Action OnPause; 
	///<summary> Called when tween is stopped (removed from the evaluator) </summary>
	event Action OnStop; 
	///<summary> Called when the tween reaches a start or end value where looping logic is used. Not invoked on the first t = 0. </summary>
	event Action OnEdge;

	TweenLoop LoopType {get; set;}
	int LoopCount {get; set;}
	Func<float,float> EaseType {get; set;}
	TweenUpdate UpdateType {get; set;}
	bool Persistant { get; set;}

	/// <summary> The current state of this tween. </summary>
	TweenState State {get; set;}
	/// <summary> Whether or not this tween is playing backwards </summary>
	bool IsReversed { get; set;}
	/// <summary> The current time this tween has been evaluated for in seconds. </summary>
	float ElapsedTime {get; set;}
	/// <summary> The duration of the tween in seconds. If less than 0, the tween is infinite. </summary>
	float Duration {get; set;}
	/// <summary> The last "t" value (0f --> 1f). It is overwritten on each evaluate. If you want to set t, go ElapsedTime *= yourT. </summary>
	float LastEasedt {get; set;}

	/// <summary> Starts playing the tween. </summary> <remarks> Adds Tween to the evaluator </remarks>
	ITween Play(); 
	/// <summary> Pauses the tween. </summary> <remarks> Tween remains in the evaluator </remarks>
	ITween Pause();
	/// <summary> Stops the tween. </summary> <remarks> Removes tween from evaluator </remarks>
	ITween Stop();

	/// <summary> Resets the tween to it's start state. This also resets the loop counter. </summary>
	ITween Rewind();
	/// <summary> Reverses the tween. Instead of A --> B, goes B --> A. The current evaluation time is preseved. </summary>
	ITween Reverse();

	/// <summary> Invokes this tween's evaluation update. This is also called by the evaluator behind the scenes. </summary>
	ITween Evaluate(float deltaTime); 

	/// <summary> Appends the tweens to the current tween and creates a new sequence. </summary>
	SequenceTween Append(params ITween[] components);
	/// <summary> Prepends the tweens to the current tween and creates a new sequence. </summary>
	SequenceTween Prepend(params ITween[] components);

	/// <summary> Adds the passed tweens/creates a new tween collection. </summary>
	CollectionTween Add(params ITween[] components);

	ITween SetLoopType (TweenLoop loopType, int loopCount);
	ITween SetEaseType (Func<float,float> easeType);
	ITween SetUpdateType (TweenUpdate updateType);
	ITween SetPersistant(bool val);
}
}
