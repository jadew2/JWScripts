﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace JW.Tween.Internal
{
/// <summary> Static class responsible for evaluating all Tweens. </summary>
public class TweenEvaluator : MonoBehaviour 
{
	static TweenEvaluator instance;
	public static TweenEvaluator Instance
	{
		get
		{
			if (instance == null)
			{
				GameObject newGo = new GameObject();
				instance = newGo.AddComponent<TweenEvaluator>();
			}
			return instance;
		}
	}
		
	#region Tween Management

	/// <summary> A 2D list of tweens to be evaluated. Sorted like Tweens[UpdateType][Tween]. </summary>
	protected readonly List<ITween>[] Tweens = null; 
	public TweenEvaluator()
	{
		int updateTypes = Enum.GetNames(typeof(TweenUpdate)).Length;
		Tweens = new List<ITween>[updateTypes];
		for (int i = 0; i < updateTypes; i++)
			Tweens[i] = new List<ITween>();
	}

	public virtual void AddTween(ITween tween, TweenUpdate updateType)
	{
		Tweens[(int)updateType].Add(tween);
		UpdateTweenGOName();
	}

	public virtual void RemoveTween(ITween tween, TweenUpdate updateType)
	{	
		if (!Tweens[(int)updateType].Remove(tween))
			Debug.LogWarning(typeof(TweenEvaluator).Name + ": Unable to remove a tween, we may have a leak :(");
		UpdateTweenGOName();
	}

	#endregion

	protected virtual void Awake()
	{
		ClearTweens();
		UpdateTweenGOName();
	}

	protected virtual void OnDestroy()
	{	
		ClearTweens();
	}

	#region Update and Evaluation Methods

	protected void Update()
	{
		var updateTweens = Tweens[(int)TweenUpdate.Update];
		evaluateAll(updateTweens, Time.deltaTime);

		var realtimeTweens = Tweens[(int)TweenUpdate.RealTime];
		evaluateAll(realtimeTweens, Time.unscaledDeltaTime);
	}

	protected void LateUpdate()
	{
		var lateUpdateTweens = Tweens[(int)TweenUpdate.LateUpdate];
		evaluateAll(lateUpdateTweens, Time.deltaTime);
	}

	protected void FixedUpdate()
	{
		var fixedUpdateTweens = Tweens[(int)TweenUpdate.FixedUpdate];
		evaluateAll(fixedUpdateTweens, Time.fixedDeltaTime);
	}
		
	void evaluateAll(List<ITween> tweens, float deltaTime)
	{
		for (int i = 0; i < tweens.Count; i++)
		{
			try
			{
				tweens[i].Evaluate(deltaTime);
			}
			catch (Exception e)
			{
				//If the tween fails, remove it.
				Debug.LogException(e);
				tweens.RemoveAt(i);
				i--;
			}
		}
	}
	#endregion

	/// <summary> Clears all tweens in the Tweens 2D array. </summary>
	protected void ClearTweens()
	{
		for (int i = 0; i < Tweens.Length; i++)
		{
			var updateTypeTweens = Tweens[i];

			for (int j = 0; j < updateTypeTweens.Count; j++) 
				updateTypeTweens[j].Stop();

			updateTypeTweens.Clear();
		}
	}

	/// <summary> Returns the number of tweens in the Tweens 2D array. </summary>
	protected int CountTweens()
	{
		int sum = 0;
		for (int i = 0; i < Tweens.Length; i++)
			sum += Tweens[i].Count;
		return sum;
	}

	protected virtual void UpdateTweenGOName()
	{
		gameObject.name = String.Format("SJTweenEvaluator : {0}", CountTweens());
	}
}
}