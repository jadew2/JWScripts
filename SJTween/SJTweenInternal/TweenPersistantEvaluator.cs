﻿using UnityEngine;
using System.Collections;
using System;

namespace JW.Tween.Internal
{
/// <summary> A version of the evaluator meant to be run across multiple scenes. </summary>
public class TweenPersistantEvaluator : TweenEvaluator {

	static TweenPersistantEvaluator instance;
	public new static TweenPersistantEvaluator Instance
	{
		get
		{
			if (instance == null)
			{
				GameObject newGo = new GameObject();
				instance = newGo.AddComponent<TweenPersistantEvaluator>();
			}
			return instance;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		DontDestroyOnLoad(this);
	}
		

	protected override void UpdateTweenGOName()
	{
		gameObject.name = String.Format("SJTween Persistant Evaluator : {0}", CountTweens());
	}
}
}