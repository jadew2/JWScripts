﻿using UnityEngine;

namespace JW.Tween
{
/// <summary>
/// <para> Contains Easing equations from LeanTween. Can be used in other places. </para>
/// <para> Every method has a overload that takes in just (t). These methods return a value from 0 --> 1, so you can use it as a lerp value.</para>
/// <para> E.g. Mathf.Lerp(20,30,SJTweenEase.clerp(t)); //is the same as SJTweenEase.clerp(20,30,t)</para>
/// </summary>
/// <remarks>
/// By: Jade White 
/// Easing Equations by: Robert Penner   
/// Modifications for LeanTween by: GFX47, Pedro (UnityTween), and rafael.marteleto.
/// <para> 
/// TERMS OF USE - EASING EQUATIONS
/// Open source under the BSD License.
/// Copyright (c)2001 Robert Penner
/// All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
/// Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
/// Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
/// Neither the name of the author nor the names of contributors may be used to toorse or promote products derived from this software without specific prior written permission.
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
/// </para>
/// </remarks>
public static class TweenEase
{
	//-----Easing Methods-----
	public static float nothing(float t) {return nothing(0f,1f,t);}
	public static float nothing(float start, float end, float t){
		return start;
	}

	public static float onCurve(float t, AnimationCurve curve) {return onCurve(0f,1f,t,curve);}
	public static float onCurve(float start, float end, float t, AnimationCurve curve){
		return start + (end - start) * curve.Evaluate(t);
	}

	public static float outQuadOpt(float t) {return outQuadOpt(0f,1f,t);}
	public static float outQuadOpt(float start, float diff, float t){
		return -diff * t * (t - 2) + start;
	}

	public static float inQuadOpt(float t) {return inQuadOpt(0f,1f,t);}
	public static float inQuadOpt(float start, float diff, float t){
		return diff * t * t + start;
	}

	public static float inOutQuadOpt(float t) {return inOutQuadOpt(0f,1f,t);}
	public static float inOutQuadOpt(float start, float diff, float t){
		t *= 2f;
		if (t < 1) return diff * 0.5f * t * t + start;
		t--;
		return -diff * 0.5f * (t * (t - 2) - 1) + start;
	}

	public static float linear(float t) {return linear(0f,1f,t);}
	public static float linear(float start, float end, float t){
		return Mathf.Lerp(start, end, t);
	}

	public static float clerp(float t) {return clerp(0f,1f,t);}
	public static float clerp(float start, float end, float t){
		float min = 0.0f;
		float max = 360.0f;
		float half = Mathf.Abs((max - min) / 2.0f);
		float retval = 0.0f;
		float diff = 0.0f;
		if ((end - start) < -half){
			diff = ((max - start) + end) * t;
			retval = start + diff;
		}else if ((end - start) > half){
			diff = -((max - end) + start) * t;
			retval = start + diff;
		}else retval = start + (end - start) * t;
		return retval;
	}
	
	public static float spring(float t) {return spring(0f,1f,t);}
	public static float spring(float start, float end, float t){
		t = Mathf.Clamp01(t);
		t = (Mathf.Sin(t * Mathf.PI * (0.2f + 2.5f * t * t * t)) * Mathf.Pow(1f - t, 2.2f) + t) * (1f + (1.2f * (1f - t)));
		return start + (end - start) * t;
	}
	
	public static float inQuad(float t) {return inQuad(0f,1f,t);}
	public static float inQuad(float start, float end, float t){
		end -= start;
		return end * t * t + start;
	}
	
	public static float outQuad(float t) {return outQuad(0f,1f,t);}
	public static float outQuad(float start, float end, float t){
		end -= start;
		return -end * t * (t - 2) + start;
	}
	
	public static float inOutQuad(float t) {return inOutQuad(0f,1f,t);}
	public static float inOutQuad(float start, float end, float t){
		t /= .5f;
		end -= start;
		if (t < 1) return end / 2 * t * t + start;
		t--;
		return -end / 2 * (t * (t - 2) - 1) + start;
	}
	
	public static float inCubic(float t) {return inCubic(0f,1f,t);}
	public static float inCubic(float start, float end, float t){
		end -= start;
		return end * t * t * t + start;
	}
	
	public static float outCubic(float t) {return outCubic(0f,1f,t);}
	public static float outCubic(float start, float end, float t){
		t--;
		end -= start;
		return end * (t * t * t + 1) + start;
	}
	
	public static float inOutCubic(float t) {return inOutCubic(0f,1f,t);}
	public static float inOutCubic(float start, float end, float t){
		t /= .5f;
		end -= start;
		if (t < 1) return end / 2 * t * t * t + start;
		t -= 2;
		return end / 2 * (t * t * t + 2) + start;
	}
	
	public static float inQuart(float t) {return inQuart(0f,1f,t);}
	public static float inQuart(float start, float end, float t){
		end -= start;
		return end * t * t * t * t + start;
	}

	public static float outQuart(float t) {return outQuart(0f,1f,t);}
	public static float outQuart(float start, float end, float t){
		t--;
		end -= start;
		return -end * (t * t * t * t - 1) + start;
	}
	
	public static float inOutQuart(float t) {return inOutQuart(0f,1f,t);}
	public static float inOutQuart(float start, float end, float t){
		t /= .5f;
		end -= start;
		if (t < 1) return end / 2 * t * t * t * t + start;
		t -= 2;
		return -end / 2 * (t * t * t * t - 2) + start;
	}
	
	public static float inQuint(float t) {return inQuint(0f,1f,t);}
	public static float inQuint(float start, float end, float t){
		end -= start;
		return end * t * t * t * t * t + start;
	}
	
	public static float outQuint(float t) {return outQuint(0f,1f,t);}
	public static float outQuint(float start, float end, float t){
		t--;
		end -= start;
		return end * (t * t * t * t * t + 1) + start;
	}
	
	public static float inOutQuint(float t) {return inOutQuint(0f,1f,t);}
	public static float inOutQuint(float start, float end, float t){
		t /= .5f;
		end -= start;
		if (t < 1) return end / 2 * t * t * t * t * t + start;
		t -= 2;
		return end / 2 * (t * t * t * t * t + 2) + start;
	}
	
	public static float inSine(float t) {return inSine(0f,1f,t);}
	public static float inSine(float start, float end, float t){
		end -= start;
		return -end * Mathf.Cos(t / 1 * (Mathf.PI / 2)) + end + start;
	}
	
	public static float outSine(float t) {return outSine(0f,1f,t);}
	public static float outSine(float start, float end, float t){
		end -= start;
		return end * Mathf.Sin(t / 1 * (Mathf.PI / 2)) + start;
	}
	
	public static float inOutSine(float t) {return inOutSine(0f,1f,t);}
	public static float inOutSine(float start, float end, float t){
		end -= start;
		return -end / 2 * (Mathf.Cos(Mathf.PI * t / 1) - 1) + start;
	}
	
	public static float inExpo(float t) {return inExpo(0f,1f,t);}
	public static float inExpo(float start, float end, float t){
		end -= start;
		return end * Mathf.Pow(2, 10 * (t / 1 - 1)) + start;
	}
	
	public static float outExpo(float t) {return outExpo(0f,1f,t);}
	public static float outExpo(float start, float end, float t){
		end -= start;
		return end * (-Mathf.Pow(2, -10 * t / 1) + 1) + start;
	}
	
	public static float inOutExpo(float t) {return inOutExpo(0f,1f,t);}
	public static float inOutExpo(float start, float end, float t){
		t /= .5f;
		end -= start;
		if (t < 1) return end / 2 * Mathf.Pow(2, 10 * (t - 1)) + start;
		t--;
		return end / 2 * (-Mathf.Pow(2, -10 * t) + 2) + start;
	}
	
	public static float inCirc(float t) {return inCirc(0f,1f,t);}
	public static float inCirc(float start, float end, float t){
		end -= start;
		return -end * (Mathf.Sqrt(1 - t * t) - 1) + start;
	}
	
	public static float outCirc(float t) {return outCirc(0f,1f,t);}
	public static float outCirc(float start, float end, float t){
		t--;
		end -= start;
		return end * Mathf.Sqrt(1 - t * t) + start;
	}
	
	public static float inOutCirc(float t) {return inOutCirc(0f,1f,t);}
	public static float inOutCirc(float start, float end, float t){
		t /= .5f;
		end -= start;
		if (t < 1) return -end / 2 * (Mathf.Sqrt(1 - t * t) - 1) + start;
		t -= 2;
		return end / 2 * (Mathf.Sqrt(1 - t * t) + 1) + start;
	}
	
	//By: GFX47
	public static float inBounce(float t) {return inBounce(0f,1f,t);}
	public static float inBounce(float start, float end, float t){
		end -= start;
		float d = 1f;
		return end - outBounce(0, end, d-t) + start;
	}
	
	//By: GFX47
	public static float outBounce(float t) {return outBounce(0f,1f,t);}
	public static float outBounce(float start, float end, float t){
		t /= 1f;
		end -= start;
		if (t < (1 / 2.75f)){
			return end * (7.5625f * t * t) + start;
		}else if (t < (2 / 2.75f)){
			t -= (1.5f / 2.75f);
			return end * (7.5625f * (t) * t + .75f) + start;
		}else if (t < (2.5 / 2.75)){
			t -= (2.25f / 2.75f);
			return end * (7.5625f * (t) * t + .9375f) + start;
		}else{
			t -= (2.625f / 2.75f);
			return end * (7.5625f * (t) * t + .984375f) + start;
		}
	}

	//By: GFX47
	public static float inOutBounce(float t) {return inOutBounce(0f,1f,t);}
	public static float inOutBounce(float start, float end, float t){
		end -= start;
		float d= 1f;
		if (t < d/2) return inBounce(0, end, t*2) * 0.5f + start;
		else return outBounce(0, end, t*2-d) * 0.5f + end*0.5f + start;
	}

	public static float inBack(float t) {return inBack(0f,1f,t);}
	public static float inBack(float start, float end, float t){
		end -= start;
		t /= 1;
		float s= 1.70158f;
		return end * (t) * t * ((s + 1) * t - s) + start;
	}
	
	public static float outBack(float t) {return outBack(0f,1f,t);}
	public static float outBack(float start, float end, float t){
		float s= 1.70158f;
		end -= start;
		t = (t / 1) - 1;
		return end * ((t) * t * ((s + 1) * t + s) + 1) + start;
	}
	
	public static float inOutBack(float t) {return inOutBack(0f,1f,t);}
	public static float inOutBack(float start, float end, float t){
		float s= 1.70158f;
		end -= start;
		t /= .5f;
		if ((t) < 1){
			s *= (1.525f);
			return end / 2 * (t * t * (((s) + 1) * t - s)) + start;
		}
		t -= 2;
		s *= (1.525f);
		return end / 2 * ((t) * t * (((s) + 1) * t + s) + 2) + start;
	}
	
	//By: GFX47
	public static float inElastic(float t) {return inElastic(0f,1f,t);}
	public static float inElastic(float start, float end, float t){
		end -= start;
		
		float d = 1f;
		float p = d * .3f;
		float s= 0;
		float a = 0;
		
		if (t == 0) return start;
		t = t/d;
		if (t == 1) return start + end;
		
		if (a == 0f || a < Mathf.Abs(end)){
			a = end;
			s = p / 4;
		}else{
			s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
		}
		t = t-1;
		return -(a * Mathf.Pow(2, 10 * t) * Mathf.Sin((t * d - s) * (2 * Mathf.PI) / p)) + start;
	}		

	//Written By: Pedro (UnityTween), Modified by GFX47, Fixed by rafael.marteleend.
	public static float outElastic(float t) {return outElastic(0f,1f,t);}
	public static float outElastic(float start, float end, float t){
		end -= start;
		
		float d = 1f;
		float p= d * .3f;
		float s= 0;
		float a= 0;
		
		if (t == 0) return start;
		
		t = t / d;
		if (t == 1) return start + end;
		
		if (a == 0f || a < Mathf.Abs(end)){
			a = end;
			s = p / 4;
		}else{
			s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
		}
		
		return (a * Mathf.Pow(2, -10 * t) * Mathf.Sin((t * d - s) * (2 * Mathf.PI) / p) + end + start);
	}		
	
	//By: GFX47
	public static float inOutElastic(float t) {return inOutElastic(0f,1f,t);}
	public static float inOutElastic(float start, float end, float t)
	{
		end -= start;
		
		float d = 1f;
		float p= d * .3f;
		float s= 0;
		float a = 0;
		
		if (t == 0) return start;
		
		t = t / (d/2);
		if (t == 2) return start + end;
		
		if (a == 0f || a < Mathf.Abs(end)){
			a = end;
			s = p / 4;
		}else{
			s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
		}
		
		if (t < 1){
			t = t-1;
			return -0.5f * (a * Mathf.Pow(2, 10 * t) * Mathf.Sin((t * d - s) * (2 * Mathf.PI) / p)) + start;
		}
		t = t-1;
		return a * Mathf.Pow(2, -10 * t) * Mathf.Sin((t * d - s) * (2 * Mathf.PI) / p) * 0.5f + end + start;
	}


}
}
