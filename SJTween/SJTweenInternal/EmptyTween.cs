﻿using UnityEngine;
using System;
using System.Collections;

// Analysis disable once ValueParameterNotUsed

namespace JW.Tween.Internal
{
/// <summary> 
/// A tween that is never evaluated, and does nothing. 
/// It's main use is for a default value. It helps avoid null checking. 
/// </summary>
public class EmptyTween : ITween
{
	#pragma warning disable 67
	public event Action OnPlay = () => {};
	public event Action OnPause = () => {};
	public event Action OnStop = () => {};
	public event Action OnEdge = () => {};
	#pragma warning restore 67

	public ITween Play()
	{
		return this;
	}

	public ITween Pause()
	{
		return this;
	}

	public ITween Stop()
	{
		return this;
	}

	public ITween Rewind()
	{
		return this;
	}

	public ITween Reverse()
	{
		return this;
	}

	public ITween Evaluate(float deltaTime)
	{
		return this;
	}

	public SequenceTween Append(params ITween[] components)
	{
		return new SequenceTween(components);
	}

	public SequenceTween Prepend(params ITween[] components)
	{
		return new SequenceTween(components);
	}

	public CollectionTween Add(params ITween[] components)
	{
		return new CollectionTween(components);
	}

	public ITween SetLoopType(TweenLoop loopType, int loopCount)
	{
		return this;
	}

	public ITween SetEaseType(System.Func<float, float> easeType)
	{
		return this;
	}

	public ITween SetUpdateType(TweenUpdate updateType)
	{
		return this;
	}

	public ITween SetPersistant(bool val)
	{
		return this;
	}

	public TweenLoop LoopType
	{
		get
		{
			return TweenLoop.Repeat;
		}
		set
		{
				
		}
	}

	public int LoopCount
	{
		get
		{
			return 0;
		}
		set
		{
		}
	}

	public Func<float, float> EaseType
	{
		get
		{
			return TweenEase.linear;
		}
		set
		{
				
		}
	}

	public TweenUpdate UpdateType
	{
		get
		{
			return TweenUpdate.Update;
		}
		set
		{
		}
	}

	public bool Persistant
	{
		get
		{
			return false;
		}
		set
		{
		}
	}

	public TweenState State
	{
		get
		{
			return TweenState.Created;
		}
		set
		{
		}
	}

	public bool IsReversed
	{
		get
		{
			return false;
		}
		set
		{
		}
	}

	public float ElapsedTime
	{
		get
		{
			return 0f;
		}
		set
		{
		}
	}

	public float Duration
	{
		get
		{
			return 0f;
		}
		set
		{
		}
	}

	public float LastEasedt
	{
		get
		{
			return 0;
		}
		set
		{
		}
	}
}
}
