﻿using System;
using UnityEngine;

namespace JW.Tween.Internal
{
/// <summary>
/// Helps tweens add themselves/remove themselves from the appropriate evaluator.
/// </summary>
public class TweenEvaluatorHelper
{
	ITween tween = null;

	TweenEvaluator evaluatorInstance = null;
	TweenEvaluator Evaluator 
	{
		// Analysis disable once ConvertConditionalTernaryToNullCoalescing
		get {return (evaluatorInstance == null) ? (evaluatorInstance = TweenEvaluator.Instance) : evaluatorInstance;}
		set {evaluatorInstance = value;}
	}

	bool inEvaluatorFlag = false; //Local flag for optimizaition
	bool InEvaluator 
	{
		get { return inEvaluatorFlag && evaluatorInstance != null; }
		set { inEvaluatorFlag = value; }
	}

	public TweenEvaluatorHelper(ITween tween)
	{
		this.tween = tween;
	}
		
	/// <summary> Adds the tween to the evaluator if it's not already in it. </summary>
	public void AddToEvaluator()
	{
		if (InEvaluator)
			return;

		Evaluator.AddTween(tween,tween.UpdateType);
		InEvaluator = true;
	}

	/// <summary> Removes the tween from the evaluator if it's in it. </summary>
	public void RemoveFromEvaluator()
	{
		if (!InEvaluator)
			return;

		if (evaluatorInstance != null) //If the evaluator instance was destroyed, there is no need to remove the tween.
			Evaluator.RemoveTween(tween,tween.UpdateType);
		InEvaluator = false;
	}

	/// <summary> Changing tween persistance requires a evaluator switch. This method handles that. </summary>
	public void ChangePersistance(bool persistant)
	{
		if (InEvaluator)
			Evaluator.RemoveTween(tween,tween.UpdateType);

		if (persistant)
			Evaluator = TweenPersistantEvaluator.Instance;
		else
			Evaluator = TweenEvaluator.Instance;

		if (InEvaluator)
			Evaluator.AddTween(tween,tween.UpdateType);
	}

	/// <summary> Changes the update method this tween is evaluated in. </summary>
	public void ChangeUpdateType(TweenUpdate oldUpdate, TweenUpdate newUpdate)
	{
		if (InEvaluator)
		{
			Evaluator.RemoveTween(tween, oldUpdate);
			Evaluator.AddTween(tween, newUpdate);
		}
	}

}
}

