using UnityEngine;
using System; //Enum
using System.Collections;
using System.Collections.Generic; //List

/* JWConstraints
 * By: Jade White 2014. Based off original code from Shane Beck.
 * For Project Target Party
 * 
 * A constraint system. Allows you to set up complex relationships between objects where you would previously have to hard code them.
 * Ideal for camera control, rotation limiters etc.
 * 
 * You only need to worry about 3 parts:
 * 		JWConstraintEvaluator: A singleton which evaluates lists of JWAbstractConstraint(s) based on their update type. Orders by priority.
 * 		JWAbstractConstraint: Abstract class that adds itself to the evaluator automatically, and provides a interface for constraints.
 * 		Constraints: LimitPosition,LimitRotation,LookAt,etc. You extend JWAbstractConstraint and write these yourself.
 * 
 * Limitations: (As of time when writing)
 * 		JWAbstractConstraint: UpdateType and Priority can't be changed during runtime. If you wish to change them, call Re-Register.
 * 		A nice "Influence" Parameter like in Blender is impossible, simply because the old position is discarded. 
 * 			However, you can set up your GO's and use LerpPosition or LerpRotation to achieve the same effect.
 * 		Because there is no Influence, Multiple Constraints on one GO is useless.
 */


namespace JW.Constraints
{

public enum ConstraintUpdateType {Update,FixedUpdate,LateUpdate}
public enum ConstraintDepthMode {Automatic,Custom}

/// <summary>
/// Singleton class for evaluating constraints.
/// 
/// The evaluator keeps a list of lists of typed constraints.
/// 		This is so new "update()" methods can be added on as time goes on.
/// 		Update() FixedUpdate() etc. evaluate all the members in their respective list every time they are called.
/// 
/// These lists of typed constraints are ordered by "Priority". Priority ranges from -1 --> infinity. 
/// 		The idea is, the constraints with the lowest depth are evaluated first.
/// 		So the lowest depths are at the front of these lists.
///  	The Abstract contraints are responsible for calculating their own custom priority. 
/// 
/// Important note: We don't check if the depth or update method changes frame to frame. Constraints have the responsibility to re-register themselves to keep these lists current.
/// 		This is for speed and simplicity. But feel free to make it work.
/// </summary>
public class ConstraintEvaluator : MonoBehaviour {

	#region Singleton Declaration
	static ConstraintEvaluator instance;
	static ConstraintEvaluator Instance
	{
		get
		{
			if (!instance)
			{
				GameObject go = new GameObject(typeof(ConstraintEvaluator).Name + " : initialized");
				instance = go.AddComponent<ConstraintEvaluator>();
			}
			return instance;
		}
	}
	public static bool InstanceExists {get{return (instance != null);}} //To prevent instance creation in OnDisable
	#endregion

	//---List of Lists of typed constraints. This is so we can add new Update(),FixedUpdate() etc. methods.
	//e.g. To do so, append something to JWConstraintUpdateType, and write the method down in "Updates"
	List<List<AbstractConstraint>> updateLists;

	int constraintCount = 0;
	int ConstraintCount 
	{
		get {return constraintCount;} 
		set
		{
			Instance.gameObject.name = (typeof(ConstraintEvaluator).Name + " : " + value); 
			constraintCount = value;
		}
	}


	/// <summary> 
	/// Registers the constraint so it is evaluated in it's chosen update method. 
	/// 
	/// Important Note: The update method cannot be changed unless you call ReRegister() 
	/// </summary>
	public static void Register(AbstractConstraint constraint)
	{
		Instance.register(constraint);
	}
	void register(AbstractConstraint constraint)
	{
		ConstraintUpdateType updateType = constraint.UpdateType;
		int depth = constraint.Priority;

		//Add to the appropriate list and order by depth
		bool inserted = false;
		List<AbstractConstraint> wantedList = updateLists[(int)updateType];
		for (int i = 0; i < wantedList.Count; i++)
		{
			if (wantedList[i].Priority >= depth)
			{
				wantedList.Insert(i,constraint); 
				inserted = true; 
				break;
			}
		}
		if (! inserted)
			wantedList.Add(constraint); //Looks like our depth was the largest, Add to the end.

		ConstraintCount++;
	}

	/// <summary> Deregisters the constraint so it is no longer automatically evaluated. </summary>
	public static void DeRegister(AbstractConstraint constraint)
	{
		Instance.deregister(constraint);
	}
	void deregister(AbstractConstraint constraint)
	{
		ConstraintUpdateType updateType = constraint.UpdateType;
		List<AbstractConstraint> wantedList = updateLists[(int)updateType];
		if (! wantedList.Remove(constraint)) //They changed the updateType without telling us. grr...
		{//Search everywhere for it
			for (int i = 0; i < updateLists.Count; i++) 
			{
				if (updateLists[i].Remove(constraint))
				{ConstraintCount--; break;}
			}
		}
		else
			ConstraintCount--;
	}

	/// <summary> Reregisters a constraint. Most useful to change it's update type. </summary>
	public static void ReRegister(AbstractConstraint constraint)
	{
		DeRegister(constraint);
		Register(constraint);
	}

	#region Events
	void Awake()
	{   //Initialize the updateLists
		int updateTypes = Enum.GetValues(typeof(ConstraintUpdateType)).Length;
		updateLists = new List<List<AbstractConstraint>>(updateTypes);
		for (int i = 0; i < updateTypes; i++) 
			updateLists.Add(new List<AbstractConstraint>());
	}

	void FixedUpdate () 
	{
		EvaluateAllOfType(ConstraintUpdateType.FixedUpdate);
	}

	void Update()
	{
		EvaluateAllOfType(ConstraintUpdateType.Update);
	}

	void LateUpdate()
	{
		EvaluateAllOfType(ConstraintUpdateType.LateUpdate);
	}

	/// <summary> Evaluation Method (Called from updates) </summary>
	void EvaluateAllOfType(ConstraintUpdateType updateType) 
	{
		if(updateLists == null)  //This happens when we compile during runtime. Throws errors everywhere.
		{
			Debug.LogError(typeof(ConstraintEvaluator).Name + ": Update lists were wiped! Re-Initialize."); 
			Awake();
		}

		List<AbstractConstraint> wantedList = updateLists[(int)updateType]; 
		for (int i = 0; i < wantedList.Count; i++)
			wantedList[i].Evaluate();
	}
	#endregion
}
}