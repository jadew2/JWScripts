﻿using UnityEngine;

namespace JW.Constraints
{
	[AddComponentMenu("JWConstraints/Smooth Position Lerp")]
	public class SmoothPositionLerp : AbstractConstraint
	{
		public Transform Target;
		public float Damping = 2f;
		
		void Start()
		{}
		
		public override void Evaluate() 
		{
			Vector3 currPos = trans.position;
			Vector3 targetPos = Target.position; 
			
			float usedDeltaTime = (base.UpdateType == ConstraintUpdateType.FixedUpdate) ? Time.fixedDeltaTime : Time.deltaTime;
			trans.position = Vector3.Lerp(currPos, targetPos, Damping*usedDeltaTime);
		}
	}
	
}
