﻿using UnityEngine;
using System.Collections;

//Based off original Code by Shane Beck
namespace JW.Constraints
{
/// <summary>
/// Keeps a GO at a consistent rotation in world space.
/// </summary>
[AddComponentMenu("JWConstraints/Set Rotation")]
public class SetRotation : AbstractConstraint
{
	public Vector3 WorldRotation;

	public override void Evaluate() 
	{
		trans.eulerAngles = WorldRotation;
	}
}
}
