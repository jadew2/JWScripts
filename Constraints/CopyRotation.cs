﻿using UnityEngine;

/* JWConstraint
 * 
 * Good Practice to make use of Influence
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */

//Based off original Code by Shane Beck
namespace JW.Constraints
{
[AddComponentMenu("JWConstraints/Copy Rotation")]
public class CopyRotation : AbstractConstraint
{
	public Transform Target;
	public bool X, Y, Z;

	public Vector3 Offset;

    public override void Evaluate() 
    {
		Vector3 currRot = trans.eulerAngles;
		Vector3 targetRot = Target.eulerAngles;

		Vector3 constrainedResult = new Vector3(
			X ? targetRot.x : currRot.x,
			Y ? targetRot.y : currRot.y,
			Z ? targetRot.z : currRot.z);

		constrainedResult = new Vector3(
			X ? constrainedResult.x + Offset.x : constrainedResult.x,
			Y ? constrainedResult.y + Offset.y : constrainedResult.y,
			Z ? constrainedResult.z + Offset.z : constrainedResult.z);

		//constrainedResult += new Vector3(360f,360f,360f);

		constrainedResult = new Vector3(
			Mathf.Repeat(constrainedResult.x,360f),
			Mathf.Repeat(constrainedResult.y,360f),
			Mathf.Repeat(constrainedResult.z,360f));

		trans.eulerAngles = targetRot; 
    }
}
}