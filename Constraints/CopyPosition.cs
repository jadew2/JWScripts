﻿using UnityEngine;

/* JWConstraint
 * 
 * Good Practice to make use of Influence
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */

//Based off original Code by Shane Beck
namespace JW.Constraints{

[AddComponentMenu("JWConstraints/Copy Position")]
public class CopyPosition : AbstractConstraint
{
	public Transform Target;
    public bool X, Y, Z;
	public Vector3 Offset;

    public override void Evaluate() 
    {
        var currPos = trans.position;
        var targetPos = Target.position;
        
		Vector3 constrainedResult = new Vector3(
			X ? targetPos.x : currPos.x,
			Y ? targetPos.y : currPos.y,
			Z ? targetPos.z : currPos.z);
		
		constrainedResult += Offset;

		trans.position = constrainedResult;
    } 
}
}