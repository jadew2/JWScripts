﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/* MultipleTransformInfluenceConstraint
 * 
 * A unique constraint that averages a transform's properties between other transforms by influence. 
 * 
 * Implementation Details:
 * 
 * As of 2014, we average the world position, rotation, and local scale of the transform this is attached to.
 * TempTransformData is a class for storing these properties temporaily. Only for looks.
 * TransformInfluence is a class for storing a transform and a influence (0-->1)
 */


namespace JW.Constraints{
[AddComponentMenu("JWConstraints/Multiple Transform Influence")]
public class MultipleTransformInfluence : AbstractConstraint 
{
	//Class that holds a Transform component, and a 0 --> 1 Influence value.
	[System.Serializable]
	public class TransformInfluence
	{
		public TransformInfluence(){}
		public TransformInfluence(Transform transform){this.Transform = transform; this.Influence = 0f;}
		public TransformInfluence(Transform transform,float influence){this.Transform = transform; this.Influence = influence;}

		public Transform Transform;
		[SerializeField] [Range(0, 1f)] float influence;
		public float Influence {get{return influence;} set {influence = Mathf.Clamp01(value);}}
	}

	//---Variables
	[SerializeField] public List<TransformInfluence> TransformInfluences;
	public void SetInfluence(Transform thatTransform, float influence) {TransformInfluences.Find(x=> (x.Transform == thatTransform)).Influence = influence;}
	public float GetInfluence(Transform thatTransform) {return TransformInfluences.Find(x=> (x.Transform == thatTransform)).Influence;}
	public void SetAllInfluences(float value){for(int i=0;i<TransformInfluences.Count;i++){TransformInfluences[i].Influence = value;}}

	//---Evaluation
	TempTransformData tempTransformData = new TempTransformData();
	public override void Evaluate() 
	{
		tempTransformData.Clear();

		float influenceSum = 0f;
		for (int i = 0; i < TransformInfluences.Count; i++)
		{
			TransformInfluence ti = TransformInfluences[i];
			if (ExtensionMethods.IsNullOrDestroyed(ti.Transform))
				continue;	
			influenceSum += ti.Influence;
		}

		if (influenceSum == 0f)
			tempTransformData.CopyFrom(trans); //Stay where we are.

		//Apply Transforms according to Influence
		for (int i = 0; i < TransformInfluences.Count; i++) 
		{
			TransformInfluence ti = TransformInfluences[i];
			if (ti.Influence == 0f)
				continue;
			if (ExtensionMethods.IsNullOrDestroyed(ti.Transform))
				continue;	
			
			float proportionalInfluence = ti.Influence/influenceSum; //So the final result will be a average
			applyTransformInfluence(ti.Transform, proportionalInfluence);
		}

		tempTransformData.ApplyTo(trans);
	}

	void applyTransformInfluence(Transform passedTransform, float influence)
	{
		tempTransformData.position += passedTransform.position * influence;

		//Note: Why not rotation *= Quat.Slerp(newRot,Quat.identity,influence); ?
		//Because this actually introduced artifacts similar to gimbal lock. 
		//Using these two directions instead, while more expensive, fixes the problem.
		tempTransformData.rotDirection += passedTransform.rotation * Vector3.forward * influence;
		tempTransformData.rotUp += passedTransform.rotation * Vector3.up * influence;

		tempTransformData.scale += passedTransform.localScale*influence;
	}

	//Class that holds Position,Rotation and Scale values for readability.
	class TempTransformData
	{
		public Vector3 position = Vector3.zero;
		public Vector3 scale = Vector3.zero;

		public Vector3 rotDirection = Vector3.forward;
		public Vector3 rotUp = Vector3.up;

		public void Clear()
		{
			position 	= Vector3.zero;
			scale 		= Vector3.zero;

			rotDirection = Vector3.zero;
			rotUp = Vector3.zero;
		}

		public void CopyFrom(Transform transform)
		{
			this.position = transform.position;
			this.scale = transform.localScale;

			this.rotDirection = transform.rotation * Vector3.forward;
			this.rotUp = transform.rotation * Vector3.up;
		}

		public void ApplyTo(Transform transform)
		{
			transform.position = position;
			if (rotDirection != Vector3.zero && rotUp != Vector3.zero)
				transform.rotation = Quaternion.LookRotation(rotDirection,rotUp);
			transform.localScale = scale;
		}
	}
}
}