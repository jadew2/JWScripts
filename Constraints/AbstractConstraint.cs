﻿using UnityEngine;
using System.Collections;


/* JWAbstractConstraint. (See JWConstraintEvaluator for a rundown of the entire system.)
 * 
 * Abstract contraint class. Meant to be extended for constaint code.
 * 
 * Contains code to register/deregister ourselves to JWConstraintEvaluator (A evaluator singleton) properly 
 * Plus, some protected parameters for optimization.
 * 
 * Priority: A integer for constraint evaluation ordering. The idea is, constraints with lowest depth are evaluated first
 *  -1 = Evaluate first (No transform), 0 = Evaluate second (No parents), 1 = Evaluate 3rd (1 Parent), 2 = Evaluate 4th (2 Parents), etc...
 * 
 * The JWConstraintEvaluator only cares about the UpdateType and the Depth variables. DepthMode and CustomDepth are added for customisability.
 * 
 * Important note: Due to the setup of the system, UpdateType and Depth can't be changed during runtime. (They can be changed, but it won't make a difference)
 * 		If you want to be able to, you need to re-register.
 * 
 */
namespace JW.Constraints
{

public abstract class AbstractConstraint : MonoBehaviour
{
	public ConstraintUpdateType UpdateType = ConstraintUpdateType.FixedUpdate;
	public ConstraintDepthMode PriorityMode = ConstraintDepthMode.Automatic;
	
	/// <summary>
	/// The custom priority, lower numbers run first (e.g. -9999), higher numbers run later (e.g. +9999)
	/// </summary>
	/// <description>
	/// For default ordering, set this value to the number of ancestors the transform has. 
	/// Priority: A integer for constraint evaluation ordering. The idea is, constraints with lowest depth are evaluated first
	/// -1 = Evaluate first (No transform), 0 = Evaluate second (No parents), 1 = Evaluate 3rd (1 Parent), 2 = Evaluate 4th (2 Parents), etc...
	/// </description>
	public int CustomPriority = -1;

	public int Priority
	{
		get
		{
			if (PriorityMode == ConstraintDepthMode.Automatic)
				return calculateDepth();
			else //JWConstraintDepthMode.Custom
					return CustomPriority;
		}
	}

	//---Optimization Params
	protected Transform trans;
	protected Rigidbody rigidb;

	protected virtual void Awake()
	{
		trans = this.transform;
		rigidb = this.GetComponent<Rigidbody>();
	}

	//---Runtime Methods
	protected virtual void OnEnable()
	{
		ConstraintEvaluator.Register(this);
	}

	protected virtual void OnDisable()
	{
		if (ConstraintEvaluator.InstanceExists) //OnDestroy might have already killed it
				ConstraintEvaluator.DeRegister(this);
	}

	public abstract void Evaluate();


	int calculateDepth()
	{
		int depth = -1;
		Transform curr = transform;
		while (curr != null)
		{
			curr = curr.parent;
			depth++;
		}
		return depth;
	}

}
}