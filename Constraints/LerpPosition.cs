﻿using UnityEngine;
using System.Collections;

/* JWConstraint
 * 
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */

//Based on code by Shane Beck
namespace JW.Constraints
{
	[AddComponentMenu("JWConstraints/Lerp Position")]
	public class LerpPosition : AbstractConstraint
	{
		public Transform From;
		public Transform To;

		[Range(0.0f, 1.0F)]
		public float T;
		
		void Start()
		{
			
		}
		
		public override void Evaluate() 
		{
			trans.position = Vector3.Lerp(From.position,To.position,T);
		}
	}
	
}