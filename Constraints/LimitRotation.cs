﻿using System;
using UnityEngine;
using System.Collections;

/* JWConstraint
 * 
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */

//Based off original Code by Shane Beck
namespace JW.Constraints
{
	[AddComponentMenu("JWConstraints/Limit Rotation")]
	public class LimitRotation : AbstractConstraint 
	{
		public enum LimitRotationSpace
		{
			Global,
			Local
		}
		
		[Range(0.0f, 359.0F)]
		public float AngleLimit = 45f;

		public LimitRotationSpace Space = LimitRotationSpace.Global;

		Quaternion originalRot;
		Quaternion originalLocalRot;

		void Start() 
		{
			originalRot = trans.rotation;
			originalLocalRot = trans.localRotation;
		}
		
		public override void Evaluate() 
		{
			if (Space == LimitRotationSpace.Global)
				globalEvaluate();
			else if (Space == LimitRotationSpace.Local)
				localEvaluate();
			else
				throw new NotImplementedException();
		}

		void globalEvaluate()
		{
			Quaternion currentRot = trans.rotation;

			float difference = Quaternion.Angle(originalRot,currentRot);
			if (difference > AngleLimit)
				trans.rotation = Quaternion.RotateTowards(currentRot,originalRot,difference-AngleLimit);
		}

		void localEvaluate()
		{
			Quaternion currentRot = trans.localRotation;

			float difference = Quaternion.Angle(originalLocalRot,currentRot);
			if (difference > AngleLimit)
				trans.localRotation = Quaternion.RotateTowards(currentRot,originalLocalRot,difference-AngleLimit);
		}
	}
}