﻿using UnityEngine;
using System.Collections;
using System;

namespace JW.Constraints
{
/// <summary>
/// Scales a GO depending on how far it is from the target. Has optional maxes and mins.
/// </summary>
[AddComponentMenu("JWConstraints/DistanceScaling")]
public class DistanceScaling : AbstractConstraint 
{
	public Transform Target;
	[Space]

	//The scale for a distance
	public float Distance1 = 0f;
	public Vector3 Scale1 = new Vector3(0.5f,0.5f,0.5f);
	[Space]
	//The scale for another distance
	public float Distance2 = 5f;
	public Vector3 Scale2 = Vector3.one;
	[Space]
	public Vector3 MaxScale = new Vector3(2.5f, 2.5f, 2.5f);
	public Vector3 MinScale = Vector3.zero;

	Vector3 scaleFactor;

	void Start()
	{
		scaleFactor = calculateScaleFactor();
	}

	public override void Evaluate()
	{
		float distance = Vector3.Distance(trans.position, Target.position); //The fastest method, belive it or not.

		Vector3 scale = scaleFactor * (distance - Distance1);

		//Max Clamp
		scale = new Vector3(
			Mathf.Min(scale.x, MaxScale.x),
			Mathf.Min(scale.y, MaxScale.y),
			Mathf.Min(scale.z, MaxScale.z));

		//Min Clamp
		scale = new Vector3(
			Mathf.Max(scale.x, MinScale.x),
			Mathf.Max(scale.y, MinScale.y),
			Mathf.Max(scale.z, MinScale.z));


		trans.localScale = scale;
	}

	/// <summary> Returns the scaling for each unit of distance. Throws errors if it's not possible to compute. </summary>
	Vector3 calculateScaleFactor()
	{
		if (Distance2 < Distance1)
			throw new ArgumentException(typeof(DistanceScaling).Name + ": Distance2 must be >  Distance1!");
		if (Distance1 < 0f || Distance2 < 0f)
			throw new ArgumentException(typeof(DistanceScaling).Name + ": Distances cannot be negative!");
		if (Math.Abs(Distance2 - Distance1) < float.Epsilon)
			throw new ArgumentException(typeof(DistanceScaling).Name + ": Distance1 and Distance2 cannot be equal!");
		
		float diff = Distance2 - Distance1;
		Vector3 scaleDiff = Scale2 - Scale1;
		return scaleDiff * (1f / diff);
	}

}
}
