﻿using UnityEngine;

/* JWConstraint
 * 
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */
namespace JW.Constraints{
	[AddComponentMenu("JWConstraints/Smooth Rotation Lerp")]
	public class SmoothRotationLerp : AbstractConstraint
	{
		public Transform Target;
		public float Damping = 2f;

		void Start()
		{}

		public override void Evaluate() 
	    {
			Quaternion currRot = trans.rotation;
			Quaternion targetRot = Target.rotation; 

			float usedDeltaTime = (base.UpdateType == ConstraintUpdateType.FixedUpdate) ? Time.fixedDeltaTime : Time.deltaTime;
			trans.rotation = Quaternion.Lerp(currRot, targetRot, Damping*usedDeltaTime);
	    } 
	}

}

