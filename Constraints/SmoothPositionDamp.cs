﻿using UnityEngine;

/* JWConstraint
 * 
 * Good Practice to make use of Influence
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */

//Based off original Code by Shane Beck
namespace JW.Constraints{

	[AddComponentMenu("JWConstraints/Smooth Position Damp")]
	public class SmoothPositionDamp : AbstractConstraint
	{
		public Transform Target;

	    Vector3 velocity;
	    public float SmoothTime = 1f;

	    public override void Evaluate() 
	    {
			trans.position = Vector3.SmoothDamp(trans.position, Target.position, ref velocity, SmoothTime);
	    }
	}

}