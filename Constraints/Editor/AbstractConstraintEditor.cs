﻿using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using JW.Utils;

namespace JW.Constraints
{
[CustomEditor (typeof(AbstractConstraint),true)] 
public class AbstractConstraintEditor : Editor
{
	private SerializedObject mainObject;
	private AbstractConstraint abstractConstraintClass;
	string[] fieldsToIgnoreArray;

	public void OnEnable ()
	{
		mainObject = new SerializedObject(target);
		abstractConstraintClass = (AbstractConstraint)target;
		List<string> fieldsToIgnoreList = getPublicFields(typeof(AbstractConstraint)).ToList();
		fieldsToIgnoreList.Add("m_Script"); //The unity script field
		fieldsToIgnoreArray = fieldsToIgnoreList.ToArray();
	}

	string[] getPublicFields(System.Type type)
	{
		return type.GetFields().Where(x => x.IsPublic).Select(x => x.Name).ToArray();
	}

	public override void OnInspectorGUI()
	{
		mainObject.Update(); //Load Changes from the Gameobject into the GUI

		InspectorUtils.ShowScriptField(mainObject);

		abstractConstraintClass.UpdateType 		= (ConstraintUpdateType) 	EditorGUILayout.EnumPopup("Update Type", abstractConstraintClass.UpdateType);
		GUILayout.BeginHorizontal();
		abstractConstraintClass.PriorityMode 	= (ConstraintDepthMode) 	EditorGUILayout.EnumPopup("Priority Mode", abstractConstraintClass.PriorityMode);
		if (abstractConstraintClass.PriorityMode == ConstraintDepthMode.Custom)
			abstractConstraintClass.CustomPriority = EditorGUILayout.IntField(abstractConstraintClass.CustomPriority,GUILayout.MaxWidth(70f));

		GUILayout.EndHorizontal();

		//Draw the properties of the sub type.
		Editor.DrawPropertiesExcluding(mainObject,fieldsToIgnoreArray);

		mainObject.ApplyModifiedProperties(); //Apply any changes
	}


	
}
}