﻿using UnityEngine;

/* JWConstraint
 * 
 * Good Practice to make use of Influence
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */

namespace JW.Constraints{

[AddComponentMenu("JWConstraints/Copy Scale")]
public class CopyScale : AbstractConstraint
{
	public Transform Target;
	public bool X, Y, Z;

	public Vector3 Offset;

    public override void Evaluate() 
    {
		Vector3 currScale = trans.localScale;
		Vector3 targetScale = Target.localScale; //lossy Scale?

		Vector3 constrainedScale = new Vector3(
			X ? targetScale.x : currScale.x,
			Y ? targetScale.y : currScale.y,
			Z ? targetScale.z : currScale.z);

		trans.localScale = constrainedScale;
    } 
}
}