﻿using UnityEngine;

/* JWConstraint
 * 
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */

//Based off original Code by Shane Beck
namespace JW.Constraints
{
	[AddComponentMenu("JWConstraints/ChildOf")]

	public class ChildOf : AbstractConstraint
	{
		public Transform Target;
        public bool UseOffset = true;
	    Vector3 localPos;
	    Quaternion localRot;

	    void Start()
	    {
            if (UseOffset)
			    localPos = Target.InverseTransformPoint(trans.position);
			localRot = Quaternion.Inverse(Target.rotation) * trans.rotation;
		}

	    public override void Evaluate() 
	    {
			trans.position = Target.TransformPoint(localPos);
			trans.rotation = Quaternion.LookRotation(Target.forward, Target.up) * localRot;
	    }
		
	}
}