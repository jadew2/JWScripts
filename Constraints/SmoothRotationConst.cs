﻿using UnityEngine;

/* JWConstraint
 * 
 * Important Note: Due to JWAbstractConstraint, you CANNOT use OnEnable, OnDisable without base() (and frankly shouldn't need to)
 */

//Based on code by Shane Beck
namespace JW.Constraints
{
	[AddComponentMenu("JWConstraints/Smooth Rotation Const")]
	public class SmoothRotationConst : AbstractConstraint
	{
		public Transform Target;

		public float RotateSpeed = 45f;
		public float AngleForRotate = 0.1f;
		
		void Start()
		{

		}

		public override void Evaluate() 
	    {
			Quaternion currRot = trans.rotation;
			Quaternion targetRot = Target.rotation;

			if (Quaternion.Angle(targetRot,currRot) > AngleForRotate)
			{
				float usedDeltaTime = (base.UpdateType == ConstraintUpdateType.FixedUpdate) ? Time.fixedDeltaTime : Time.deltaTime;

				trans.rotation = Quaternion.RotateTowards(trans.rotation,Target.rotation,RotateSpeed* usedDeltaTime);
			}
	    } 
	}

}

