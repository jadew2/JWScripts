﻿using UnityEngine;
using System.Collections;


namespace JW.Constraints
{
/// <summary> 
/// Orients a gameobject to direct +Z towards a target. Uses Quaternion.LookRotation()
/// </summary>
[AddComponentMenu("JWConstraints/LookAt")]
public class LookAt : AbstractConstraint 
{
	public Transform Target;
	public bool Reversed = false;
	public enum UpType {None,Custom,Target}
	public UpType UsedUp;
	public Vector3 CustomUp;
	public Vector3 TargetOffset;
	
	void Start() 
	{

	}
	
	public override void Evaluate() 
	{
		Vector3 directionToTarget  = (Target.position + TargetOffset) - trans.position;
		
		if (Reversed)
			directionToTarget = -directionToTarget;
		Vector3 normal;

		if (UsedUp == UpType.None)
			normal = trans.up;
		else if (UsedUp == UpType.Custom)
			normal = CustomUp;
		else
			normal = Target.up;

		if (directionToTarget != Vector3.zero)
			trans.rotation = Quaternion.LookRotation(directionToTarget,normal);
	}
}
}