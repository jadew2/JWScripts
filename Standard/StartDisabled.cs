﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{

public class StartDisabled : MonoBehaviour
{
	public bool DisableInAwake = true;
	public bool DisableInStart = true;
	public bool DisableFrameAfterStart = true;

	bool wasDisabled = false;
	
	void Awake()
	{
		//if (!Application.isEditor)
		//	Debug.LogError(typeof(StartDisabled).Name + ": Attached to " + gameObject.name + " in " + gameObject.scene.name + " still has this script attached. Remove it!");
		if (DisableInAwake)
		{
			this.gameObject.SetActive(false);
			wasDisabled = true;
		}
	}

	void Start()
	{
		if (DisableInStart && !wasDisabled)
		{
			this.gameObject.SetActive(false);
			wasDisabled = true;
		}

		if (DisableFrameAfterStart && !wasDisabled)
			StartCoroutine(delayedDisable());
	}

	IEnumerator delayedDisable()
	{
		yield return null;
		if (!wasDisabled)
			this.gameObject.SetActive(false);
	}
}
}
