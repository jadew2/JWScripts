﻿using UnityEngine;
using System.Collections;
using JW.Tween;

/// <summary>
/// Animates pausing/resuming the game. 
/// To use, set Animate to true, and call to PauseGame() or UnPauseGame().
/// </summary>
public class AnimatedPauser : Pauser 
{
	public bool Animate = false;
	public float AnimateTime = 1f;
	ITween animationTween = null;

	protected override void DoPauseGame()
	{
		if (Animate)
			handleAnimation(true);
		else
		{
			stopAnimation();
			base.DoPauseGame();
		}
	}

	protected override void DoUnPauseGame()
	{
		if (Animate)
			handleAnimation(false);
		else
		{
			stopAnimation();
			base.DoUnPauseGame();
		}
	}


	void handleAnimation(bool pausing)
	{
		//Create the animation tween
		ITween timeScaleTween = SJTween.Value(
			Time.timeScale,
			(pausing) ? 0f : originalTimeScale,
			AnimateTime,
			Mathf.Lerp,t => Time.timeScale = t);

		ITween fixedDeltaTimeTween = SJTween.Value(
			Time.fixedDeltaTime,
			(pausing) ? 0f : originalFixedDeltaTime,
			AnimateTime,
			Mathf.Lerp,
			t => Time.fixedDeltaTime = originalFixedDeltaTime);

		ITween newTween = SJTween.Collection(timeScaleTween,fixedDeltaTimeTween);

		//When it stops, set the animation tween to null if it matches this new tween.
		newTween.OnStop += () => 
		{
			if (animationTween == newTween)
				animationTween = null;

			//Set the values on stop. This is to make sure if we're animating when a scene loads, nothing screws up too bad.
			if (pausing)
			{
				Time.timeScale = 0f;
				Time.fixedDeltaTime = 0f;
			}
			else
			{
				Time.timeScale = originalTimeScale;
				Time.fixedDeltaTime = originalFixedDeltaTime;
			}
		};
		newTween.EaseType = TweenEase.inOutQuad;
		newTween.SetUpdateType(TweenUpdate.RealTime); //We're changing time, so it's gotta be real.

		//Halt the old tween if it exists
		stopAnimation();
		//Set the new tween
		animationTween = newTween;
	}

	void stopAnimation()
	{
		if (animationTween != null)
			animationTween.Stop();
		animationTween = null;
	}
}
