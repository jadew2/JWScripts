﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

using JW.Tween;

/// <summary> 
/// Class responsible for pausing the game. 
/// Does so by setting Time.timeScale and Time.fixedDeltaTime to 0. 
/// Also calls OnPauseGame() and OnUnPauseGame() methods.
/// </summary>
public class Pauser : MonoBehaviour 
{
	public enum PauseState
	{
		Unpaused,
		Paused,
	}

	/// <summary> Whether the game is paused or unpaused. </summary>
	public PauseState State { get; private set; }

	protected float originalTimeScale = 0f;
	protected float originalFixedDeltaTime = 0f;

	protected virtual void Awake()
	{
		State = PauseState.Unpaused;

		originalTimeScale = Time.timeScale;
		originalFixedDeltaTime = Time.fixedDeltaTime;

		SceneManager.sceneLoaded += onLoadScene;
	}

	protected virtual void OnDestroy()
	{
		SceneManager.sceneLoaded -= onLoadScene;
	}

	void onLoadScene(Scene scene, LoadSceneMode loadMode)
	{
		if (loadMode == LoadSceneMode.Single)
			UnPauseGame();
	}

	/// <summary> Pauses the game, and calls OnPauseGame() in every Monobehaviour. </summary>
	public void PauseGame()
	{
		if (State == PauseState.Paused || !IsSafeToPauseGame())
			return;

		State = PauseState.Paused;
		DoPauseGame();
		sendMessageToAllGameObjects("OnPauseGame");
	}

	/// <summary>
	/// Unpauses the game, and calls OnUnPauseGame() on every Monobehaviour. 
	/// </summary>
	public void UnPauseGame()
	{
		if (State == PauseState.Unpaused || !IsSafeToUnPauseGame())
			return; //Do nothing

		State = PauseState.Unpaused;
		DoUnPauseGame();
		sendMessageToAllGameObjects("OnUnPauseGame");
	}

	/// <summary> The changes to time that pauses the game, use it implement any extra functionality. </summary>
	protected virtual void DoPauseGame()
	{
		Time.timeScale = 0f;
		Time.fixedDeltaTime = 0f;
	}

	/// <summary> The changes to time that unpauses the game,  use it implement any extra functionality. </summary>
	protected virtual void DoUnPauseGame()
	{
		Time.timeScale = originalTimeScale;
		Time.fixedDeltaTime = originalFixedDeltaTime;
	}

	/// <summary> Returns whether it is safe to pause the game. Can be overridden. </summary>
	protected virtual bool IsSafeToPauseGame()
	{
		return true;
	}

	/// <summary> Returns whether it is safe to unpause the game. Can be extended. </summary>
	protected virtual bool IsSafeToUnPauseGame()
	{
		return true;;
	}


	///<summary>>Calls any "void methodName()" on all Monobehaviours.</summary>
	void sendMessageToAllGameObjects(string methodName)
	{
		GameObject[] gameObjects = FindObjectsOfType<GameObject>();
		foreach (GameObject go in gameObjects)
			go.SendMessage (methodName, SendMessageOptions.DontRequireReceiver);
	}


}
