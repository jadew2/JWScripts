﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JW.Standard
{
/// <summary> Simple animation class through colors using cos. Override ApplyValue to do something.  </summary>
public class ColorPulsator : MonoBehaviour 
{
	public List<Color> Colors;

	public float Speed = 1f;
	public bool UseRealTime = false;

	protected void Update()
	{
		float time = (UseRealTime) ? Time.realtimeSinceStartup : Time.time;
		float t = (Mathf.Cos(time * Speed) + 1) * 0.5f; //+1 to get the cos in the range 0 -> 2. Then we divide by 2 to get the range 0 -> 1.
		Color wantedColor = getWantedColor(t);

		ApplyValue(wantedColor);
	}

	/// <summary> Gets the value of the color at T.  </summary>
	Color getWantedColor(float normalizedT)
	{
		if (Colors.Count == 0)
			return Color.black;
		
		int startIndex = (int)(normalizedT * (Colors.Count - 1));

		if (startIndex == Colors.Count - 1)
			return Colors[Colors.Count - 1];

		float segmentT =  Mathf.InverseLerp((float)startIndex,(float)startIndex + 1,normalizedT * (float)(Colors.Count - 1)) ;
		return Color.Lerp(Colors[startIndex],Colors[startIndex+1],segmentT);
	}

	protected virtual void ApplyValue(Color value)
	{

	}
}
}