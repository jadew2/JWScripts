﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace JW.Standard
{

/// <summary> Animates the alpha of UI Text. </summary>
public class TextAlphaPulsator : ValuePulsator {

	[SerializeField] Text uiText = null;

	protected override void ApplyValue(float alphaValue)
	{
		Color oldColor = uiText.color;
		Color newColor = new Color(oldColor.r,oldColor.g,oldColor.b,alphaValue);

		uiText.color = newColor;
	}
}
}
