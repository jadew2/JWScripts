﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> Offsets the child on start. </summary>
public class ChildOffset : MonoBehaviour 
{
	public Vector3 Offset;

	// Use this for initialization
	void Start () 
	{
		transform.localPosition += Offset;
	}
}
}
