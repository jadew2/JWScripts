﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> Sets the application's target frame rate. Because some fool set the default to 30fps on some devices. </summary>
public class ApplicationFrameRate : MonoBehaviour 
{
	[Header("Note: If you change the framerate to something other than 30 or 60, it will disable VSync. VSync forces a higher FPS.")]
	public int FrameRate = 60;
	public int HeadlessFrameRate = 9; //9 is the default rate of network transform send messages.

	void Awake()
	{
		int usedFrameRate = FrameRate;
		if (SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.Null) //If in Headless mode.
			usedFrameRate = HeadlessFrameRate;

		if (usedFrameRate != 60 && usedFrameRate != 30)
			QualitySettings.vSyncCount = 0; //We disable Vsync because it'll force a higher FPS.
		Application.targetFrameRate = FrameRate;
	}
}
}