﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace JW.Standard
{

    public class ImageColorPulsator : ColorPulsator
    {
        [SerializeField] Image uiImage = null;

        public bool IncludeOriginalColor = false;

        protected virtual void Start()
        {
            Colors.Insert(0, uiImage.color);
        }

        protected override void ApplyValue(Color colorValue)
        {
            uiImage.color = colorValue;
        }
    }
}