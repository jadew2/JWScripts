﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace JW.Standard
{
/// <summary> A observable list. (Because Unity uses a old version of .Net) </summary>
/// <remarks>
/// Modified from "WhyDoIDoIt" from http://answers.unity3d.com/questions/677956/getting-notified-when-modifying-a-collection-ala-c.html
/// </remarks>
[Serializable]
public class ObservableList<T> : List<T>
{
	public event Action<ListChangedArgs<T>> OnListChanged = changedArgs => {};

	public new void Add(T item)
	{
		base.Add(item);

		doListChanged(new []{item}, null);
	}

	public new void AddRange(IEnumerable<T> collection)
	{
		base.AddRange(collection);

		doListChanged(collection,null);
	}

	public new void Insert(int index, T item)
	{
		base.Insert(index, item);

		doListChanged(new []{item},null);
	}

	public new void InsertRange(int index, IEnumerable<T> collection)
	{
		base.InsertRange(index, collection);

		doListChanged(collection,null);
	}

	public new bool Remove(T item)
	{
		bool itemRemoved = base.Remove(item);
		if (itemRemoved)
			doListChanged (null,new []{item});
		return itemRemoved;
	}

	public new bool RemoveAt(int index)
	{
		T item = default(T);
		if (index >= 0 && index < base.Count) //If the index is in range, set the item.
			item = base[index];
		else
			return false;
		
		base.RemoveAt(index);
		doListChanged (null,new []{item});
		return true;
	}

	public new void RemoveRange(int index, int count)
	{
		var removedItems = base.GetRange(index, count);
		base.RemoveRange(index, count);

		if (removedItems.Count > 0)
			doListChanged(null,removedItems);
	}

	public new void RemoveAll(Predicate<T> match)
	{
		var removedItems = base.FindAll(match);
		base.RemoveAll(match);

		if (removedItems.Count > 0)
			doListChanged(null,removedItems);
	}

	public new void Clear()
	{
		var removedItems = new List<T>(this);
		base.Clear();

		//If we called .Clear() on a empty list, do nothing. Otherwise do the callback.
		if (removedItems.Count > 0)
			doListChanged(null,removedItems);
	}

	public new T this[int index]
	{
		get
		{
			return base[index];
		}
		set
		{
			var added = new []{value};
			var removed = new []{base[index]};

			base[index] = value;

			doListChanged(added,removed);
		}
	}
	
	void doListChanged(IEnumerable<T> addedItems, IEnumerable<T> removedItems)
	{
		OnListChanged(new ListChangedArgs<T>(addedItems,removedItems,(Count == 0)));
	}

}
}