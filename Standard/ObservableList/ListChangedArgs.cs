﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JW.Standard
{
/// <summary> Class to help pass list changed events. </summary>
public class ListChangedArgs<T>
{
	public ListChangedArgs(IEnumerable<T> addedItems, IEnumerable<T> removedItems, bool listEmptied = false)
	{
		this.ListEmptied = listEmptied;

		if (addedItems != null)
			AddedItems = addedItems;
		if (removedItems != null)
			RemovedItems = removedItems;
	}

	public readonly bool ListEmptied = false;
	public readonly IEnumerable<T> AddedItems = new List<T>();
	public readonly IEnumerable<T> RemovedItems = new List<T>();
}
}
