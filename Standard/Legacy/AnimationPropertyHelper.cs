﻿using UnityEngine;
using System.Collections;

namespace JW.Standard.Legacy
{
/// <summary>
/// Class that exposes properties on the legacy animation class that should have been exposed all along. Applies them on start.
/// </summary>
[System.Obsolete("Use Animators wherever possible. Legacy animation will be removed.")]
[RequireComponent (typeof(Animation))]
public class AnimationPropertyHelper : MonoBehaviour {
//Disable warning that "new" isn't required.
#pragma warning disable 109
    new Animation animation;
#pragma warning restore 109

    public WrapMode WrapMode = WrapMode.Default;
	public float ClipSpeed = 1f;

	void Awake()
	{
		animation = GetComponent<Animation>();
	}

	void Start () 
	{
		setProperties();
	}
		
	//Sets all the properties specified if they aren't null.
	void setProperties()
	{
		//Set the clip speed
		string clipName = animation.clip.name;
		animation[clipName].speed = ClipSpeed;

		//Set the wrap mode
		animation.wrapMode = WrapMode;
	}
}
}
