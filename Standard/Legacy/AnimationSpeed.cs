﻿using UnityEngine;
using System.Collections;
namespace JW.Standard.Legacy
{
/// <summary> Sets the speed of a legacy animation component on Start(). Pathetic. I know. </summary>
[System.Obsolete("Use Animation Property Helper instead",true)]
[RequireComponent (typeof(Animation))]
public class AnimationSpeed : MonoBehaviour
{

//Disable warning that "new" isn't required.
#pragma warning disable 109
    new Animation animation;
#pragma warning restore 109

        public float Speed = 1f;

	void Awake()
	{
		animation = GetComponent<Animation>();
	}

	void Start () 
	{
		string clipName = animation.clip.name;
		animation[clipName].speed = Speed;
	}

}
}
