﻿using UnityEngine;
using System.Collections;

namespace JW.Standard.Legacy
{
/// <summary> Plays a Animation OnEnable. </summary>
[System.Obsolete("Use Animators wherever possible. Legacy animation will be removed.")]
[RequireComponent (typeof(Animation))]
public class AnimateGOEnable : MonoBehaviour {
	
	public string AnimationName = "nothing";

	void OnEnable()
	{
		gameObject.GetComponent<Animation>().Play(AnimationName);
	}
}
}
