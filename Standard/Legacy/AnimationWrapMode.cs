﻿using UnityEngine;
using System.Collections;

namespace JW.Standard.Legacy
{
///<summary> Allows you to set the animation wrap mode of a legacy animation. Seriously. What genius coded this. </summary>
[System.Obsolete("Use Animation Property Helper instead",true)]
[RequireComponent (typeof(Animation))]
public class AnimationWrapMode : MonoBehaviour {
	public WrapMode wrapMode;
	
	void OnEnable () 
	{
		GetComponent<Animation>().wrapMode = wrapMode;
	}
}
}
