﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> A class moves the GO it's attached to at this velocity indefinitley. </summary>
public class BasicTranslate : MonoBehaviour 
{
	public Vector3 Velocity = new Vector3(0,0,1);
	
	// Update is called once per frame
	void Update () 
	{
		transform.position += Velocity*Time.deltaTime;
	}
}
}
