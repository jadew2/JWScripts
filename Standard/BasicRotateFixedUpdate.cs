﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> Rotates a object around a defined axis. Useful for adding quick effects like windmill propellors. </summary>
/// <remarks> By Jade White February 18, 2014 - Project Sky </remarks>
public class BasicRotateFixedUpdate : MonoBehaviour {
	
	public Vector3 Axis = Vector3.forward;
	public float Speed = 0f;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate() 
	{
		transform.Rotate(Axis,Speed*Time.deltaTime);
	}
}
}