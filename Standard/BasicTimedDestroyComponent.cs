﻿using UnityEngine;
using System.Collections;
using System.Reflection;

namespace JW.Standard
{
/// <summary> A class which destroys the Monobehaviour it's set to after X seconds. Uses Invoke. </summary>
public class BasicTimedDestroyComponent<T> : MonoBehaviour
{
	public MonoBehaviour Component;
	public float TimeUntilDestruction = 10f;

	bool invoked = false;
	// Use this for initialization
	void OnEnable () 
	{
		if (invoked == false)
		{Invoke("destroy",TimeUntilDestruction); invoked = true;}
	}
	
	void destroy()
	{
		Object.Destroy(Component);
		Object.Destroy(this);
	}

	void OnDestroy()
	{
		CancelInvoke();
	}
}
}
