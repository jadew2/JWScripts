﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> A class which destroys the GameObject it's attached to after X seconds. Uses Invoke in OnEnable(). </summary>
public class BasicTimedDestroyGO : MonoBehaviour {

	public float TimeUntilDestruction = 10f;

	bool invoked = false;
	// Use this for initialization
	void OnEnable () 
	{
		if (invoked == false)
		{Invoke("destroy",TimeUntilDestruction); invoked = true;}
	}
	
	void destroy()
	{
		Object.Destroy(this.gameObject);
		//Object.Destroy(this); //(Not reached)
	}

	void OnDestroy()
	{
		CancelInvoke();
	}

}
}
