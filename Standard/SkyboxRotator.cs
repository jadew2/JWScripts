﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> Rotates a global skybox by setting the _Rotation float in it's material from 0f --> 360f. </summary>
public class SkyboxRotator : MonoBehaviour {
	
	public float Speed = 1f;

	// Update is called once per frame
	void Update () 
	{
		RenderSettings.skybox.SetFloat("_Rotation",Mathf.Repeat(Time.timeSinceLevelLoad * Speed,360f));
	}
}
}
