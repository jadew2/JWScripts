﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace JW.Standard
{
/// <summary>
/// Slides a UI RawImage.
/// </summary>
/// <remarks>
/// To slide a texture in UnityEngine.UI, it's not as simple as .SetOffset().
/// 
/// 1. Use UI.RawImage instead of UI.Image
/// 2. Be sure the texture you set inside RawImage is not a sprite (Use a regular texture, with Wrapping = Repeat)
/// 3. Change the UV rect in RawImage for the offset
/// </remarks>
[RequireComponent(typeof(RawImage))]
public class TextureSliderUI : MonoBehaviour {

	Vector2 initialOffset = Vector2.zero;
	public Vector2 OffsetSpeed;
	public bool UseRealTime = false;

	RawImage rawImage;

	void Awake()
	{
		rawImage = GetComponent<RawImage>();
	}

	void Start()
	{
		initialOffset = rawImage.uvRect.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		float time = (UseRealTime) ? Time.realtimeSinceStartup : Time.time;
		Vector2 offset = OffsetSpeed * time;

		Rect newRect = rawImage.uvRect;
		newRect.position = offset + initialOffset;

		rawImage.uvRect = newRect;
	}
}
}
