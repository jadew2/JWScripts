﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Standard
{

/// <summary>
/// Unparents itself and becomes a root Gameobject on startup.
/// </summary>
public class RootGameobject : MonoBehaviour 
{
	public enum UnparentTime
	{
		Awake,
		OnEnable,
		Start
	}

	public UnparentTime UnparentOn;

	void Awake()
	{
		if (UnparentOn == UnparentTime.Awake)
			transform.parent = null;
	}

	void OnEnable()
	{
		if (UnparentOn == UnparentTime.OnEnable)
			transform.parent = null;
	}

	// Use this for initialization
	void Start () {
		if (UnparentOn == UnparentTime.Start)
			transform.parent = null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
}
