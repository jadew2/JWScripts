﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> Enables particle emission after a delay, with a fade in of a duration. </summary>
/// <remarks> Written by Jade White for project DreamFlyer. Can be replaced by SJTween. </remarks>
[RequireComponent (typeof(ParticleSystem))]
public class AnimateParticleEnabledAlpha : MonoBehaviour {

	public float StartDelay = 0f;
	public float Duration = 10f;
	public AnimationCurve AlphaOverDuration;
	public string MaterialColorPropertyName = "_TintColor";

	ParticleSystem particleSys = null;

	void Awake()
	{
		particleSys = GetComponent<ParticleSystem>();
	}

	bool started = false;
	void OnEnable() 
	{
		if(!started)
		{
			StartCoroutine(handleEmissionEnable());
			started = true;
		}
	}
	
	void OnDisable()
	{
		StopAllCoroutines();
		started = false;
	}
	
	IEnumerator handleEmissionEnable()
	{
		setTheMaterialAlpha(0f);
		float SafeDuration = Mathf.Max(0.001f,Duration);

		yield return new WaitForSeconds(StartDelay);

		float elaspedTime = 0f;
		while (elaspedTime < Duration)
		{
			float alpha = AlphaOverDuration.Evaluate(elaspedTime/SafeDuration);
			setTheMaterialAlpha(alpha);

			elaspedTime += Time.deltaTime;
			yield return null;
		}
	}

	void setTheMaterialAlpha(float alpha)
	{
		Material particleMaterial = particleSys.GetComponent<Renderer>().material;
		Color oldColor = particleMaterial.GetColor(MaterialColorPropertyName);
		Color newColor = new Color(oldColor.r,oldColor.g,oldColor.b,alpha);
		
		particleMaterial.SetColor(MaterialColorPropertyName, newColor);
	}

	void OnDestroy()
	{
		StopAllCoroutines();
	}
}
}
