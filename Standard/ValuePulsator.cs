﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{

/// <summary> Simple animation class using cos. Override ApplyValue to do something.  </summary>
public class ValuePulsator : MonoBehaviour 
{
	public float MaxValue = 1f;
	public float MinValue = 0f;
	public float Speed = 1f;

	public bool UseRealTime = false;

	protected void Update()
	{
		float time = (UseRealTime) ? Time.realtimeSinceStartup : Time.time;
		float t = (Mathf.Cos(time * Speed) + 1) * 0.5f; //+1 to get the cos in the range 0 -> 2. Then we divide by 2 to get the range 0 -> 1.

		float wantedValue = Mathf.Lerp(MinValue,MaxValue,t);

		ApplyValue(wantedValue);
	}

	protected virtual void ApplyValue(float value)
	{

	}

}
}