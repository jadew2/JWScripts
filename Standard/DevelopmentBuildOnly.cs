﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevelopmentBuildOnly : MonoBehaviour 
{
	void Awake()
	{
		if (!Debug.isDebugBuild)
			this.gameObject.SetActive(false);
	}

}
