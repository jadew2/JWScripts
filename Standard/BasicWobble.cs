﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> A class that makes it's Gameobject wobble. Doesn't stack well. </summary>
public class BasicWobble : MonoBehaviour {

	public Vector3 Axis = Vector3.forward;
	public float Speed = 5f;
	public float Angle = 20f;

	// Use this for initialization
	Quaternion inital_rotation = Quaternion.identity;
	void Start () 
	{
		inital_rotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () 
	{
		float angle = Mathf.Cos(Time.timeSinceLevelLoad*Speed)*Angle;
		transform.rotation = inital_rotation * Quaternion.AngleAxis(angle,Axis);
	}
}
}
