﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
public class BasicJiggle : MonoBehaviour {

	Vector3 scaleDifference = Vector3.zero;

	public float speed;
	public Vector3 addedScale;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		//Undo our scale change from last time.
		transform.localScale = transform.localScale - scaleDifference;

		//Calculate the new scale changes
		float multiplier = (Mathf.Cos(Time.time*speed * 6f) + 1f);
		scaleDifference = addedScale * multiplier;

		//Add it
		transform.localScale = transform.localScale + scaleDifference;
	}
}
}