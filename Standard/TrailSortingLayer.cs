﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{

[RequireComponent(typeof(TrailRenderer))]
public class TrailSortingLayer : MonoBehaviour 
{
	TrailRenderer trailRenderer;

	void Awake()
	{
		trailRenderer = GetComponent<TrailRenderer>();
	}

	// Use this for initialization
	void Start () 
	{
		trailRenderer.sortingLayerName = "Foreground";
		//trailRenderer.material.
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
}
