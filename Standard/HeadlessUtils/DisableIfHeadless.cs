﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace JW.Standard.HeadlessUtils
{
	public class DisableIfHeadless : MonoBehaviour
	{
		void Awake()
		{
			if (ExtensionMethods.IsHeadless())
			{
				//Disable particle systems (if they exist)
				ParticleSystem ps = GetComponent<ParticleSystem>();
				if (ps != null)
				{
					ps.Clear(true);
					ps.Stop(true);
				}

				//Disable the GO
				gameObject.SetActive(false);
			}
		}
	}
}