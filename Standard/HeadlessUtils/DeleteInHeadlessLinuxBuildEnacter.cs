﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.SceneManagement;
#endif

namespace JW.Standard.HeadlessUtils
{
	/// <summary>
	/// Deletes any Gameobject with DeleteInHeadlessLinuxBuild attached
	/// Only if we're building to Linux64 with Headless Mode Enabled.
	/// 
	/// This is to solve a nasty bug with LevelMusicPlayer, where it causes the server to freeze up for some reason.
	/// </summary>
	public class DeleteInHeadlessLinuxBuildEnacter : MonoBehaviour
	{
#if UNITY_EDITOR
		[PostProcessScene]
		public static void DeleteGO()
		{
			if (BuildPipeline.isBuildingPlayer)
			{
				BuildTarget buildTarget = EditorUserBuildSettings.activeBuildTarget;

				bool linuxBuildTarget = buildTarget == BuildTarget.StandaloneLinux64;
				bool headlessModeEnabled = EditorUserBuildSettings.enableHeadlessMode;

				if (linuxBuildTarget && headlessModeEnabled)
				{
					DeleteInHeadlessLinuxBuild[] markedObjects = FindObjectsOfType<DeleteInHeadlessLinuxBuild>();

					for (int i = 0; i < markedObjects.Length; i++)
					{
						GameObject currGameObject = markedObjects[i].gameObject;

						Debug.Log(typeof(DeleteInHeadlessLinuxBuild).Name + ": Destroying: " + currGameObject.name + " in scene " + EditorSceneManager.GetActiveScene().name);

						DestroyImmediate(currGameObject);
					}
				}
			}
		}
#endif
	}
}
