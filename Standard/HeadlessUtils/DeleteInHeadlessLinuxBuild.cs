﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Standard.HeadlessUtils
{
	public class DeleteInHeadlessLinuxBuild : MonoBehaviour
	{
		void Awake()
		{
			//Show a error message if this makes it into a headless linux app.
			if (Application.platform == RuntimePlatform.LinuxPlayer && SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.Null)
				Debug.LogError(typeof(DeleteInHeadlessLinuxBuild).Name + ": Gameobject " + gameObject + " was not deleted! The game might have unexpected behaviour.");
		}
	}
}
