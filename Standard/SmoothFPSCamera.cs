﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Standard
{

public class SmoothFPSCamera : MonoBehaviour 
{
	public float SpeedMultiplier = 25.0f;
	public float FastSpeedMultiplier = 100.0f;

	public float RotationMultiplier = 10.0f;

	public float PositionDamping = 2.0f;
	public float RotationDamping = 2.0f;

	public float minimumY = -89f;
	public float maximumY = 89f;

	public bool OnlyRotateOnLeftMouse = true;

	float rotationX = 0.0f;
	float rotationY = 0.0f;

	Vector3 wantedPositon = Vector3.zero;

	bool fastSpeedToggle = false;
	
	void Start()
	{
		wantedPositon = transform.position;
	}

	void Update()
	{
		//Movement
		Vector3 directedMovement = transform.rotation * new Vector3(Input.GetAxisRaw("Horizontal"),0.0f,Input.GetAxisRaw("Vertical")) ;

		if (Input.GetKey(KeyCode.Space))
			directedMovement += new Vector3(0,1.0f,0);
		if (Input.GetKey(KeyCode.LeftShift))
			directedMovement += new Vector3(0,-1.0f,0);

		if (Input.GetKeyDown(KeyCode.V))
			fastSpeedToggle = !fastSpeedToggle;

		float usedSpeedMultiplier = (fastSpeedToggle) ? FastSpeedMultiplier : SpeedMultiplier;
		
		wantedPositon += directedMovement * usedSpeedMultiplier * Time.deltaTime;

		transform.position = Vector3.Lerp(transform.position,wantedPositon,PositionDamping * Time.deltaTime);

		//Rotation

		//Rotate. Unless OnlyRotateOnLeftMouse is checked and LeftMouse isn't down. 
		bool canRotate = !(OnlyRotateOnLeftMouse && !Input.GetMouseButton(0));

		if (canRotate)
		{
			//We use Input.GetAxis() because it's a delta value that changes even if the mouse doesn't move on screen.
			rotationX += Input.GetAxis("Mouse X") * RotationMultiplier; 
			rotationY += Input.GetAxis("Mouse Y") * RotationMultiplier;
			rotationY = Mathf.Clamp(rotationY,minimumY,maximumY);
		}

		//We lerp, because chances are we'll use this camera for the promotional video.
		transform.localRotation = Quaternion.Lerp(transform.localRotation,Quaternion.Euler(new Vector3(-rotationY, rotationX, 0)), RotationDamping * Time.deltaTime);
	}
}
}

