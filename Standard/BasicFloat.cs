﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Standard
{
	public class BasicFloat : MonoBehaviour
	{

		Vector3 translationDifference = Vector3.zero;

		public float speed = 1.0f;
		public Vector3 addedTranslation = Vector3.up;

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

			//Undo our scale change from last time.
			transform.localPosition = transform.localPosition - translationDifference;

			//Calculate the new scale changes
			float multiplier = (Mathf.Cos(Time.time * speed) + 1f);
			translationDifference = addedTranslation * multiplier;

			//Add it
			transform.localPosition = transform.localPosition + translationDifference;
		}
	}
}