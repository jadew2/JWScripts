﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary>
/// Calls DontDestroyOnLoad() in Start().
/// </summary>
public class DontDestroyOnLoad : MonoBehaviour 
{
	/// <summary> Deparents the GO so we are a root gameObject (So DontDestroyOnLoadWorks) </summary>
	public bool UnparentOnStart = false;

	// Use this for initialization
	void Start () 
	{
		if (UnparentOnStart)
			this.transform.parent = null;
		
		DontDestroyOnLoad(this);
	}

}
}