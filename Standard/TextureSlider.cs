using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> Adds a UV panning effect to a texture. Works with both renderers and UIImages. </summary>
/// <remarks> 
/// By Jade White February 18, 2014 For Project Sky
/// Modified by Jade White, October 4, 2015 For Project Target Party. 
/// </remarks>
public class TextureSlider : MonoBehaviour 
{
	public string ShaderTextureName = "_MainTex";
	public Vector2 OffsetSpeed;

	Material material;

	void Awake()
	{
		Renderer rend = GetComponent<Renderer>();
		material = rend.material;
	}
	// Update is called once per frame
	void Update () 
	{ 	
		Vector2 offset = OffsetSpeed * Time.time;
		material.SetTextureOffset(ShaderTextureName, offset);
	}

}
}
