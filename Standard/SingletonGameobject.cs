﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> Prevents the gameobject it is attached to from being doubled in the scene. Works by comparing it's Unique Label. If the Unique label is not set, uses it's Gameobject Name.</summary>
/// <remarks> To work properly, give it a early execution order. -1000 is nice. </remarks>
public class SingletonGameobject : MonoBehaviour 
{
	/// <summary> The unique label for this class. If we find another instance that has this label in Awake, we will destroy ourselves. </summary>
	public string UniqueLabel = "None";

	void Awake()
	{
		if (UniqueLabel == "None")
			UniqueLabel = gameObject.name;
		
		//If any other thing exists in this scene with our label, kill ourselves.
		SingletonGameobject[] otherInstances = FindObjectsOfType<SingletonGameobject>();
		foreach (var instance in otherInstances)
		{
			if (instance != this && instance.UniqueLabel == this.UniqueLabel)
			{
				gameObject.SetActive(false);
				DestroyImmediate(gameObject); //This is needed to prevent any other Awake()'s from being called.
				break;
			}
		}

		//Otherwise, continue.
	}

}
}