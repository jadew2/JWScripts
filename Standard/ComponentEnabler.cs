﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary>
/// Simply enables all the components in the list on startup.
/// Useful for hiding behaviours in the editor.
/// </summary>
public class ComponentEnabler : MonoBehaviour {

	public MonoBehaviour[] Components;

	void Awake()
	{
		foreach (var component in Components)
			component.enabled = true;
	}
}
}
