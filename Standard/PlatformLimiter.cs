﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Disables the GO it's attached to if we aren't running on one of the checked platforms.
/// </summary>
public class PlatformLimiter : MonoBehaviour 
{
	[Space]
	public Platforms AllowedPlatforms;
	[System.Serializable]
	public class Platforms
	{
		public bool Android = true;
		public bool IOS = true;
		public bool WindowsStore = true;
	}

	public SystemTypes AllowedSystemTypes;

	[System.Serializable]
	public class SystemTypes
	{
		public bool Editor = true;
		public bool Mobile = true;
		public bool Console = true;
		public bool Desktop = true;
	}
		
	void Awake()
	{
		if (Application.isEditor)
		{
			if (!AllowedSystemTypes.Editor)
				doDisable();
		} 
		else if (Application.isMobilePlatform)
		{
			if (!AllowedSystemTypes.Mobile)
				doDisable();
		}
		else if (Application.isConsolePlatform)
		{
			if (!AllowedSystemTypes.Console)
				doDisable();
		}
		else if (!AllowedSystemTypes.Desktop)
			doDisable();

		//Platform limitations
		if (Application.platform == RuntimePlatform.Android)
		{
			if (!AllowedPlatforms.Android)
				doDisable();
		} 
		else if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			if (!AllowedPlatforms.IOS)
				doDisable();
		} 
		else if (Application.platform == RuntimePlatform.WSAPlayerARM ||
		         Application.platform == RuntimePlatform.WSAPlayerX64 ||
		         Application.platform == RuntimePlatform.WSAPlayerX86)
		{
			if (!AllowedPlatforms.WindowsStore)
				doDisable();
		}
		else if (Application.platform == RuntimePlatform.OSXPlayer ||
		         Application.platform == RuntimePlatform.WindowsPlayer ||
		         Application.platform == RuntimePlatform.LinuxPlayer)
		{
			if (!AllowedSystemTypes.Desktop)
				doDisable();
		}
		
	}


	protected virtual void doDisable()
	{
		gameObject.SetActive(false);
	}

}
