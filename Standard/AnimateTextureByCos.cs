﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace JW.Standard
{
/// <summary>
/// Adds a circular UV panning effect to a texture. Rotates around the initial offset.
/// </summary>
/// <remarks>  By Jade White February 18, 2014, For Project Sky </remarks>
public class AnimateTextureByCos : MonoBehaviour {

	[FormerlySerializedAs("Shader_Texture_Name")]
	public string ShaderTextureName = "_MainTex";
	[FormerlySerializedAs("speed")]
	public float Speed = 1;
	[FormerlySerializedAs("scale")]
	public float Scale = 1;

//Disable warning that "new" isn't required.
#pragma warning disable 109
    new Renderer renderer;
#pragma warning restore 109

    Vector2 initial_offset = Vector2.zero;
	void Start() 
	{
		renderer = GetComponent<Renderer>();
		initial_offset = renderer.material.GetTextureOffset(ShaderTextureName);

	}

	void Update ()
    { 	
		Vector2 output_VEC = initial_offset + new Vector2(Mathf.Cos(Time.timeSinceLevelLoad *Speed)*Scale ,Mathf.Sin(Time.timeSinceLevelLoad *Speed)*Scale);		
		renderer.material.SetTextureOffset(ShaderTextureName, output_VEC);
	}
}
}
