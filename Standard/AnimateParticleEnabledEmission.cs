﻿using UnityEngine;
using System.Collections;

namespace JW.Standard
{
/// <summary> Enables Particle emission after a delay, and disables emission after the duration. </summary>
[RequireComponent (typeof(ParticleSystem))]
public class AnimateParticleEnabledEmission : MonoBehaviour {

	public float EmissionStartDelay = 0f;
	public float EmissionDuration = 10f;

	ParticleSystem particleSys = null;

	void Awake()
	{
		particleSys = GetComponent<ParticleSystem>();
	}

	bool started = false;
	void OnEnable() 
	{
		if(!started)
		{
			StartCoroutine(handleEmissionEnable());
			started = true;
		}
	}

	void OnDisable()
	{
		StopAllCoroutines();
		started = false;
	}

	IEnumerator handleEmissionEnable()
	{
		var emissionModule = particleSys.emission;
		emissionModule.enabled = false;
		yield return new WaitForSeconds(EmissionStartDelay);
		emissionModule.enabled = true;
		yield return new WaitForSeconds(EmissionDuration);
		emissionModule.enabled = false;
	}

	void OnDestroy()
	{
		StopAllCoroutines();
	}

}
}
